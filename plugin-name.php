<?php
//error_reporting(0);
/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * Dashboard. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              http://example.com
 * @since             1.0.0
 * @package           Plugin_Name
 *
 * @wordpress-plugin
 * Plugin Name:       Cycling Plugin
 * Plugin URI:        http://example.com/plugin-name-uri/
 * Description:       This is a short description of what the plugin does. It's displayed in the WordPress dashboard.
 * Version:           1.0.0(new)
 * Author:            Lotto Soudal
 * Author URI:        http://example.com/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       plugin-name
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

//require plugin_dir_path( __FILE__ ) . 'includes/widgets/events_widget.php';

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-plugin-name-activator.php
 */
function activate_plugin_name() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-plugin-name-activator.php';
	Plugin_Name_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-plugin-name-deactivator.php
 */
function deactivate_plugin_name() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-plugin-name-deactivator.php';
	Plugin_Name_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_plugin_name' );
register_deactivation_hook( __FILE__, 'deactivate_plugin_name' );

/**
 * The core plugin class that is used to define internationalization,
 * dashboard-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-plugin-name.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_plugin_name() {

	$plugin = new Plugin_Name();
	$plugin->run();

}
run_plugin_name();



define('ADV_PLUGIN_FS_PATH1', plugin_dir_path(__FILE__) );
define('ADV_PLUGIN_WS_PATH1', plugin_dir_url(__FILE__) );

/*
 * Include Custom Feild Files
 */

//Declares Custom Feilds for Riders
require plugin_dir_path( __FILE__ ) . 'includes/class-rider-custom-feilds.php';

//Declares Custom Feilds for Staff
require plugin_dir_path( __FILE__ ) . 'includes/staff/class-staff-custom-feilds.php';

//Declares Custom Feilds for Events
require plugin_dir_path( __FILE__ ) . 'includes/events/class-events-custom-feilds.php';

//Declares Custom Feilds for result
require plugin_dir_path( __FILE__ ) . 'includes/results/class-results-custom-feilds.php';

//Declares Custom Feilds for Palmars
require plugin_dir_path( __FILE__ ) . 'includes/palmares/class-palmares-custom-feilds.php';

//Declares Custom Feilds for Gallery
require plugin_dir_path( __FILE__ ) . 'includes/gallery/class-gallery-custom-feilds.php';

//Declares Events Types Template
require plugin_dir_path( __FILE__ ) . 'includes/events/class-events-types-custom-feilds-templates.php';

//Declares Calender Events Types Template
require plugin_dir_path( __FILE__ ) . 'includes/templates/class-templates.php';

require plugin_dir_path( __FILE__ ) . 'includes/widgets/responsive_calender.php';

require plugin_dir_path( __FILE__ ) . 'includes/widgets/events_widget.php';


//Declares Settings Template
//require plugin_dir_path( __FILE__ ) . 'includes/settings/class-settings.php';


//Declares Common Fucntion File 
require plugin_dir_path( __FILE__ ) . 'includes/common/fucntions.php';

/* Filter the single_template with our custom function*/
add_filter('single_template', 'my_custom_template');

function my_custom_template($single) {
    global $wp_query, $post;

/* Checks for rider template by post type */
if ($post->post_type == "riders"){
   
    if(file_exists(plugin_dir_path( __FILE__ ) . '/includes/rider/class-rider-custom-template.php'))
        return plugin_dir_path( __FILE__ ) .  '/includes/rider/class-rider-custom-template.php';
}

/* Checks for staff template by post type */
if ($post->post_type == "suddo_staff"){
   
    if(file_exists(plugin_dir_path( __FILE__ ) . '/includes/staff/class-staff-custom-template.php'))
        return plugin_dir_path( __FILE__ ) .  '/includes/staff/class-staff-custom-template.php';
}

/* Checks for events template by post type */
if ($post->post_type == "events"){
   
    if(file_exists(plugin_dir_path( __FILE__ ) . '/includes/events/class-events-custom-template.php'))
        return plugin_dir_path( __FILE__ ) .  '/includes/events/class-events-custom-template.php';
}
    
   return $single;    
}


function call_taxonomy_template_from_directory(){
    global $post;
    $taxonomy_slug = get_query_var('teams');
    //load_template(get_template_directory() . "/templates-taxonomy/taxonomy-$taxonomy_slug.php");
    load_template( plugin_dir_path( __FILE__ ) .  "/includes/events/taxonomy-events.php");
}
add_filter('taxonomy_template', 'call_taxonomy_template_from_directory');

$op_size = get_option('rider_list_thumb');
$op_size = explode(',', $op_size);


if($op_size[0] != '' && $op_size[1] != ''){
    
}elseif($op_size[0] != '' && $op_size[1] == ''){
    $op_size[1] == '';
}
elseif($op_size[0] == '' && $op_size[1] != ''){
    $op_size[0] == '';
}

add_image_size( 'rider-thumb', $op_size[0], $op_size[1], true ); // (cropped)
add_image_size( 'rider_list_thumb', $op_size[0], $op_size[1], true ); // (cropped)

//add_image_size( 'rider-event-thumb', 156, 200, true ); // (cropped)


$op_size1 = get_option('rider-event-thumb');    
add_image_size( 'rider-page-thumb', $op_size1 ); // 300 pixels wide (and unlimited height)



add_image_size( 'cycling_gallery-thumb', 148, 95, true ); // 300 pixels wide (and unlimited height)


//Declares Calender Events Types Template
require plugin_dir_path( __FILE__ ) . 'includes/countries-array.php';


add_action('admin_init', array(&$this, 'createDefaultTables'));

function createDefaultTables() {
    global $wpdb;
    include plugin_dir_path( __FILE__ ) . 'includes/countries-array.php';
    foreach ($countries as $ckey=>$cvals){
        $ckeydd[] = $ckey;
    }
   
    $sercountries = serialize($ckeydd);
    $cout_query = "INSERT INTO wp_options (option_name, option_value) VALUES ( 'countries_array', '".serialize($countries)."')";
    $wpdb->query( $wpdb->prepare($cout_query));
    
    $cout_query1 = "INSERT INTO wp_options (option_name, option_value) VALUES ( 'selected_countries_array', '".$sercountries."')";
    $wpdb->query( $wpdb->prepare($cout_query1));
    
    add_option( 'rider-event-thumb', 300, 'yes' );
    add_option( 'rider_list_thumb', '172, 226', 'yes' );
    
    
}
register_activation_hook( __FILE__, 'createDefaultTables' );






// Start Cron Job

add_action( 'wp', 'prefix_setup_schedule' );
/**
 * On an early action hook, check if the hook is scheduled - if not, schedule it.
 */
function prefix_setup_schedule() {
	if ( ! wp_next_scheduled( 'prefix_hourly_event' ) ) {
		wp_schedule_event( time(), 'daily', 'prefix_hourly_event');
	}
}


add_action( 'prefix_hourly_event', 'prefix_do_this_hourly' );
/**
 * On the scheduled action hook, run a function.
 */
function prefix_do_this_hourly() {
    global $wpdb;


$current_date = date("m/d/Y");

// Add 15 Days to current date
$i = 1;
$date = date("m/d/Y", strtotime(date('m/d/Y') . " +" . $i . " years"));
$update = strtotime($date);

$currentdate = strtotime($current_date);


// Get All Riders Posts
$args = array(
    'posts_per_page' => 200,
    'offset' => 0,
    'category' => '',
    'category_name' => '',
    'orderby' => 'post_date',
    'order' => 'DESC',
    'include' => '',
    'exclude' => '',
    'meta_key' => '',
    'meta_value' => '',
    'post_type' => array( 'riders', 'suddo_staff' ),
    'post_mime_type' => '',
    'post_parent' => '',
    'post_status' => 'publish',
    'suppress_filters' => true
);
$get_all_riders_left_posts = get_posts($args);




foreach ($get_all_riders_left_posts as $post) {

    /* do not know */
    $post_categories = get_the_terms($post->ID, 'teams');
    foreach ($post_categories as $term) {
        $team_link = $term->name;
        $team_slug_nm = $term->slug;
        $team_id = $term->term_id;
    }

   $team_id_fr = icl_object_id($team_id, 'teams', true, 'fr' );
   $team_id_nl = icl_object_id($team_id, 'teams', true, 'nl' );
   $team_id_en = icl_object_id($team_id, 'teams', true, 'en' );
   $get_post_type = get_post_type($post->ID);

    // Get Rider Date And Month
    $rider_brth = get_post_meta($post->ID, 'rddatepicker', true);
    
    /* convert birth day to current year  */
    $time=strtotime($rider_brth);
    $riderdate = date("m/d",$time);
    $finalriderdate = $riderdate.'/'.date('Y');   // current year processing
    $serriderdate = strtotime($finalriderdate);
    
    /* get next year & current year */
    $nextyear1 = date("Y", strtotime(date('Y') . " +1 years"));
    $finaleventdatess = $riderdate.'/'.date("Y");
    $finaleventdate_next = $riderdate.'/'.$nextyear1;

    
    // Check if rider birthday matched with after 365 Days
    if (($serriderdate >= $currentdate) && ($serriderdate <= $update)) {    
                
        
        $checkquery = "SELECT count(post_title) FROM `".$wpdb->prefix."posts` as ps INNER JOIN `".$wpdb->prefix."postmeta` as pm ON pm.post_id = ps.ID WHERE post_title like '".$post->post_title."' AND `post_status` = 'publish' AND `post_type` = 'events' AND `meta_value` = '".$finaleventdatess."'";
        $post_if = $wpdb->get_var($checkquery);
        
        
        if($post_if < 1){                            
        // Get Birthday Category ID by slug 'birthday'
        $category = get_term_by('slug', 'birthday-nl', 'suddo_event_type');
        $term_id = $category->term_id;    
                    
        $nextyear = date("Y", strtotime(date('Y') . " +1 years"));
    
        //Generate Event Date
        $finaleventdate = $riderdate.'/'.date("Y");
        
        // Array for wp_insert_post
        $my_posts = array(
            'post_title' => $post->post_title,
            'post_content' => 'Birthday Event ' . $post->post_title,
            'post_type' => 'events',
            'tax_input' => array(
                                'suddo_event_type' => array($term_id)
                            ),
            'post_status' => 'publish',            
        );

        
		
		$postid_nl = icl_object_id($post->ID, $get_post_type , true, 'nl' );
		$riderpostid = array($postid_nl);
        $serriderpostid_nl = serialize($riderpostid);
        echo $serriderpostid_nl;
        
        $post_id = wp_insert_post($my_posts);    
        //Custom Feilds of created posts
        update_post_meta($post_id, 'eventDateofEvent', $finaleventdate);
        update_post_meta($post_id, 'key_event_team_name', $team_id_nl);
        update_post_meta($post_id, 'riderskey', $serriderpostid_nl);
        
        
        $myrows = $wpdb->get_results( "SELECT * FROM ".$wpdb->prefix."icl_translations WHERE element_type = 'post_events' AND element_id = '$post_id'" );
    
        $trid = $myrows[0]->trid;
        
        
    
        // Array for wp_insert_post for nl
        // Get Birthday Category ID by slug 'birthday'
        $category_nl = get_term_by('slug', 'birthday-en', 'suddo_event_type');
        $term_id_nl = $category_nl->term_id;  
        
        $my_posts_nl = array(
            'post_title' => $post->post_title,
            'post_content' => 'Birthday Event ' . $post->post_title,
            'post_type' => 'events',
            'tax_input' => array(
                                'suddo_event_type' => array($term_id_nl)
                            ),
            'post_status' => 'publish',            
        );
        $post_id_nl = wp_insert_post($my_posts_nl);
		
		$postid_en = icl_object_id($post->ID, $get_post_type, true, 'en' );
		$riderpostid_en = array($postid_en);
        $serriderpostid_en = serialize($riderpostid_en);
		
        //Custom Feilds of created posts
        update_post_meta($post_id_nl, 'eventDateofEvent', $finaleventdate);
        update_post_meta($post_id_nl, 'key_event_team_name', $team_id_en);
        update_post_meta($post_id_nl, 'riderskey', $serriderpostid_en);
        $wpdb->query(
	"
            UPDATE " .$wpdb->prefix."icl_translations
            SET trid = $trid,
            language_code = 'en',    
            source_language_code = 'nl'
            WHERE element_id = $post_id_nl 
	"
        );
        
        // Array for wp_insert_post for fr
        $category_fr = get_term_by('slug', 'birthday-fr', 'suddo_event_type');
        $term_id_fr = $category_fr->term_id;  
        
        $my_posts_fr = array(
            'post_title' => $post->post_title,
            'post_content' => 'Birthday Event ' . $post->post_title,
            'post_type' => 'events',
            'tax_input' => array(
                                'suddo_event_type' => array($term_id_fr)
                            ),
            'post_status' => 'publish',            
        );
        $post_id_fr = wp_insert_post($my_posts_fr);
		
		$postid_fr = icl_object_id($post->ID, $get_post_type, true, 'fr' );
		$riderpostid_fr = array($postid_fr);
        $serriderpostid_fr = serialize($riderpostid_fr);
		
        update_post_meta($post_id_fr, 'eventDateofEvent', $finaleventdate);
        update_post_meta($post_id_fr, 'key_event_team_name', $team_id_fr);
        update_post_meta($post_id_fr, 'riderskey', $serriderpostid_fr);
        $wpdb->query(
	"
            UPDATE " .$wpdb->prefix."icl_translations
            SET trid = $trid,
            language_code = 'fr',    
            source_language_code = 'nl'
            WHERE element_id = $post_id_fr 
	"
        );
        
      
        
        // Error Handing
        if (is_wp_error($post_id)) {
            $errors = $post_id->get_error_messages();
            foreach ($errors as $error) {
                echo $error; 
                exit;
            }
        }
        }
        
        
       
        
        $checkquerynext = "SELECT count(post_title) FROM `".$wpdb->prefix."posts` as ps INNER JOIN `".$wpdb->prefix."postmeta` as pm ON pm.post_id = ps.ID WHERE post_title like '".$post->post_title."' AND `post_status` = 'publish' AND `post_type` = 'events' AND `meta_value` = '".$finaleventdate_next."'";
        $post_if_next = $wpdb->get_var($checkquerynext);
        
        if($post_if_next < 1){
                        // Get Birthday Category ID by slug 'birthday'
                        $category1 = get_term_by('slug', 'birthday-nl', 'suddo_event_type');
                        $term_id1 = $category1->term_id;    

                        $nextyear_n = date("Y", strtotime(date('Y') . " +1 years"));

                        //Generate Event Date
                        $finaleventdate1 = $riderdate.'/'.$nextyear_n;

                        // Array for wp_insert_post
                        $my_posts1 = array(
                            'post_title' => $post->post_title,
                            'post_content' => 'Birthday Event ' . $post->post_title,
                            'post_type' => 'events',
                            'tax_input' => array(
                                                'suddo_event_type' => array($term_id1)
                                            ),
                            'post_status' => 'publish',            
                        );

                        $riderpostid1 = array($post->ID);
                        $serriderpostid1 = serialize($riderpostid1);

                        $post_id1 = wp_insert_post($my_posts1); 
						
						$postid_nl1 = icl_object_id($post->ID, $get_post_type, true, 'nl' );
						$riderpostid1 = array($postid_nl1);
						$serriderpostid_nl1 = serialize($riderpostid1);
						   
                        //Custom Feilds of created posts
                        update_post_meta($post_id1, 'eventDateofEvent', $finaleventdate1);
                        update_post_meta($post_id1, 'key_event_team_name', $team_id_nl);
                        update_post_meta($post_id1, 'riderskey', $serriderpostid_nl1);


                        $myrows1 = $wpdb->get_results( "SELECT * FROM ".$wpdb->prefix."icl_translations WHERE element_type = 'post_events' AND element_id = '$post_id1'" );

                        $trid1 = $myrows1[0]->trid;

                        // Array for wp_insert_post for nl
                        // Get Birthday Category ID by slug 'birthday'
                        $category_nl1 = get_term_by('slug', 'birthday-en', 'suddo_event_type');
                        $term_id_nl1 = $category_nl1->term_id;  

                        $my_posts_nl1 = array(
                            'post_title' => $post->post_title,
                            'post_content' => 'Birthday Event ' . $post->post_title,
                            'post_type' => 'events',
                            'tax_input' => array(
                                                'suddo_event_type' => array($term_id_nl1)
                                            ),
                            'post_status' => 'publish',            
                        );
                        $post_id_nl1 = wp_insert_post($my_posts_nl1);
						
						$postid_en1 = icl_object_id($post->ID, $get_post_type, true, 'en' );
						$riderpostid_en1 = array($postid_en1);
						$serriderpostid_en1 = serialize($riderpostid_en1);
						
                        //Custom Feilds of created posts
                        update_post_meta($post_id_nl1, 'eventDateofEvent', $finaleventdate1);
                        update_post_meta($post_id_nl1, 'key_event_team_name', $team_id_en);
                        update_post_meta($post_id_nl1, 'riderskey', $serriderpostid_en1);
                        $wpdb->query(
                        "
                            UPDATE " .$wpdb->prefix."icl_translations
                            SET trid = $trid1,
                            language_code = 'en',    
                            source_language_code = 'nl'
                            WHERE element_id = $post_id_nl1 
                        "
                        );

                        // Array for wp_insert_post for fr
                        $category_fr1 = get_term_by('slug', 'birthday-fr', 'suddo_event_type');
                        $term_id_fr1 = $category_fr1->term_id;  

                        $my_posts_fr1 = array(
                            'post_title' => $post->post_title,
                            'post_content' => 'Birthday Event ' . $post->post_title,
                            'post_type' => 'events',
                            'tax_input' => array(
                                                'suddo_event_type' => array($term_id_fr1)
                                            ),
                            'post_status' => 'publish',            
                        );
                        $post_id_fr1 = wp_insert_post($my_posts_fr1);
						
						$postid_fr1 = icl_object_id($post->ID, $get_post_type, true, 'fr' );
						$riderpostid_fr1 = array($postid_fr1);
						$serriderpostid_fr1 = serialize($riderpostid_fr1);
						
                        update_post_meta($post_id_fr1, 'eventDateofEvent', $finaleventdate1);
                        update_post_meta($post_id_fr1, 'key_event_team_name', $team_id_fr);
                        update_post_meta($post_id_fr1, 'riderskey', $serriderpostid_fr1);
                        $wpdb->query(
                        "
                            UPDATE " .$wpdb->prefix."icl_translations
                            SET trid = $trid1,
                            language_code = 'fr',    
                            source_language_code = 'nl'
                            WHERE element_id = $post_id_fr1 
                        "
                        );

                        //Custom Feilds of created posts
                        /*update_post_meta($post_id, 'eventDateofEvent', $finaleventdate);
                        update_post_meta($post_id, 'key_event_team_name', $team_id);
                        update_post_meta($post_id, 'riderskey', $serriderpostid1);
                        */
                        // Error Handing
                        if (is_wp_error($post_id)) {
                            $errors = $post_id->get_error_messages();
                            foreach ($errors as $error) {
                                echo $error; 
                                exit;
                            }
                        }
        }

       
        
    }
}
    
    
$to = "rocky.developer@gmail.com";
$subject = "Script Runned Cycle";
$txt = "Script Runned Cycle";
$headers = "From: rocky.developer@gmail.com";

mail($to,$subject,$txt,$headers);
echo 'Events updated'; 
}

add_filter('get_post_metadata', function($metadata, $object_id, $meta_key) {
    $fieldtitle = "pyre_page_title_custom_text";
    if (isset($meta_key) && $meta_key == $fieldtitle && is_archive()) {
        return FALSE;
    }
}, 10, 3);
function pr($e){echo "<pre>";print_r($e);echo "</pre>";}
add_action('after_fusion_checklist_list_items', function ($_args) {
    $args = array();
    $teams = get_terms('teams', $args);
    foreach ($teams as $term) {
        $checked = in_array($term->term_id, $_SESSION['team_filter']) ? ' checked' : '';
        echo "<li class=\"$_args[li_class]\" style=\"$_args[li_style]\">";
        echo "<label><input type=\"checkbox\" class=\"$_args[input_class]\" name=\"team_filter[]\" value=\"{$term->term_id}\"{$checked} /> ".$term->name."</label>";
        echo "</li>";
    }
});
add_action('before_hidenfilter_submit', function () {
    if (!isset($_SESSION['team_filter']))
        $_SESSION['team_filter'] = null;
    session_destroy();
});
add_action('after_hidenfilter_submit', function () {
    $_SESSION['team_filter'] = isset($_POST['team_filter']) ? $_POST['team_filter'] : null;
});
add_action('before_events_get', function(){
    if(!$_SESSION['team_filter'])
        return;
    global $get_all_events;//meta => key_event_team_name
    if(!isset($get_all_events['meta_query']))
        $get_all_events['meta_query'] = array();
    $get_all_events['meta_query'][] = array(
        'key' => 'key_event_team_name',
        'value' => $_SESSION['team_filter'],
        'compare' => 'IN'
    );
});