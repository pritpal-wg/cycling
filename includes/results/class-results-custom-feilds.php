<?php
error_reporting(0);
/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the dashboard.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Plugin_Name
 * @subpackage Plugin_Name/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, dashboard-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    Plugin_Name
 * @subpackage Plugin_Name/includes
 * @author     Your Name <email@example.com>
 */
/**
 * Calls the class on the post edit screen.
 */
function call_resultsCustomFeilds() {
    new resultsCustomFeilds();
}

if ( is_admin() ) {
    add_action( 'load-post.php', 'call_resultsCustomFeilds' );
    add_action( 'load-post-new.php', 'call_resultsCustomFeilds' );
}


$prefix = 'result';
$c_results_meta_boxes[] =  array(                          // list of meta fields

                    array(
                        'name' => __('Result Type','wp-cycle-manager'),                 // field name
                        'desc' => __('Result Type','wp-cyle-manager'), // field description, optional
                        'id' => $prefix.'ResultType',              // field id, i.e. the meta key
                        'type' => 'selected',                       // text box
                        'std' => ''                    // default value, optional
                    ),
                   /* array(
                        'name' => __('Date','wp-team-manager'),                  // field name
                        'desc' => __('Date','wp-team-manager'), // field description, optional
                        'id' => $prefix . 'datepicker',              // field id, i.e. the meta key
                        'type' => 'text',                       // text box
                        'std' => ''                    // default value, optional
                    ),
                    array(
                        'name' => __('Location','wp-team-manager'),                  // field name
                        'desc' => __('Location','wp-team-manager'), // field description, optional
                        'id' => $prefix . 'Location',              // field id, i.e. the meta key
                        'type' => 'text',                       // text box
                        'std' => ''                    // default value, optional
                    ),                  
                    array(
                        'name' => __('Select Event','wp-team-manager'),                  // field name
                        'desc' => __('Select Event','wp-team-manager'), // field description, optional
                        'id' => $prefix . 'SelectEvent',              // field id, i.e. the meta key
                        'type' => 'text',                       // text box
                        'std' => ''                    // default value, optional
                    ),        
                    */ 
                    
);

$cweb_events_args = array(
                                    'posts_per_page'   => 1000,
                                    'offset'           => 0,
                                    'category'         => '',
                                    'category_name'    => '',
                                    'orderby'          => 'post_date',
                                    'order'            => 'DESC',
                                    'include'          => '',
                                    'exclude'          => '',
                                    'meta_key'         => '',
                                    'meta_value'       => '',
                                    'post_type'        => 'events',
                                    'post_mime_type'   => '',
                                    'post_parent'      => '',
                                    'post_status'      => 'publish',
                                    'suppress_filters' => 0 );



/** 
 * The Class.
 */
class resultsCustomFeilds {
        
	/**
	 * Hook into the appropriate actions when the class is constructed.
	 */
	public function __construct() {
            
		add_action( 'add_meta_boxes', array( $this, 'add_meta_box' ) );
		add_action( 'save_post', array( $this, 'save' ) );
                add_action( 'post_edit_form_tag' , array( $this,'post_edit_form_tag') );
                
	}

	/**
	 * Adds the meta box container.
	 */
	public function add_meta_box( $post_type ) {
            $post_types = array('event_results');     //limit meta box to certain post types
            if ( in_array( $post_type, $post_types )) {
		add_meta_box(
			'some_meta_box_name'
			,__( 'Details', 'myplugin_textdomain' )
			,array( $this, 'render_meta_box_content' )
			,$post_type
			,'advanced'
			,'high'
		);
            }
	}
        
        function select(){
            echo "The select function is called.";
        }
	/**
	 * Save the meta when the post is saved.
	 *
	 * @param int $post_id The ID of the post being saved.
	 */
	public function save( $post_id ) {
                global $c_results_meta_boxes;
                global $wpdb;
		/*
		 * We need to verify this came from the our screen and with proper authorization,
		 * because save_post can be triggered at other times.
		 */

		// Check if our nonce is set.
		if ( ! isset( $_POST['myplugin_inner_custom_box_nonce'] ) )
			return $post_id;

		$nonce = $_POST['myplugin_inner_custom_box_nonce'];

		// Verify that the nonce is valid.
		if ( ! wp_verify_nonce( $nonce, 'myplugin_inner_custom_box' ) )
			return $post_id;

		// If this is an autosave, our form has not been submitted,
                //     so we don't want to do anything.
		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) 
			return $post_id;

		// Check the user's permissions.
		if ( 'page' == $_POST['post_type'] ) {

			if ( ! current_user_can( 'edit_page', $post_id ) )
				return $post_id;
	
		} else {

			if ( ! current_user_can( 'edit_post', $post_id ) )
				return $post_id;
		}

		/* OK, its safe for us to save the data now. */
                
              
		// Sanitize the user input.
		$mydata = sanitize_text_field( $_POST['rider_gender'] );
                
                
		// Update the meta field.
		update_post_meta( $post_id, 'key1', $mydata );
                
                $rider_res_ico= serialize( $_POST['rider_res_ico'] );
                update_post_meta( $post_id, 'key_rider_res_ico', $rider_res_ico );
                
                $rider_marks = serialize( $_POST['rider_marks'] );
                update_post_meta( $post_id, 'key_rider_marks', $rider_marks );
                
                
                $rider_award = serialize( $_POST['rider_award'] );
                update_post_meta( $post_id, 'key_rider_award', $rider_award );
                
                $rider_id = serialize( $_POST['rider_id'] );
                update_post_meta( $post_id, 'key_rider_id', $rider_id );
                
                foreach ($c_results_meta_boxes['0'] as $field) {
                    update_post_meta( $post_id, $field['id'], $_POST[$field['id']] );
                }
                                            
                update_post_meta( $post_id, 'key_events_result_name', $_POST['events_result_name'] );
                
                //echo '<pre>';
                //print_r($_FILES);
                //echo '<br/><br/>';
                if(!empty($_FILES['rider_award']['name'])){
                    foreach($_FILES['rider_award']['name'] as $key=>$val):
                           
                            $_FILES['rider_award_icon']=array();
                            $_FILES['rider_award_icon']=array('name'=>$_FILES['rider_award']['name'][$key],'type'=>$_FILES['rider_award']['type'][$key],'tmp_name'=>$_FILES['rider_award']['tmp_name'][$key],'error'=>$_FILES['file']['error'][$key],'size'=>$_FILES['rider_award']['size'][$key]);
                            
                            $upload = wp_upload_bits($_FILES['rider_award_icon']['name'], null, file_get_contents($_FILES['rider_award_icon']['tmp_name']));
                            
                            if(isset($upload['error']) && $upload['error'] != 0) {
                                wp_die('There was an error uploading your file. The error is: ' . $upload['error']);
                            } else {
                                add_post_meta($post_id, 'key_rider_award_icon'.$key, $upload);
                                update_post_meta($post_id, 'key_rider_award_icon'.$key, $upload);     
                            } // end if/else
                            //echo '<pre>'; print_r($_FILES['rider_award_icon']);
                    endforeach;
                }
                
                      
                
                
               if(isset($_POST['deleteimgs'])){
                   delete_post_meta($post_id, $_POST['deleteimgs']); 
               }
                
                
	}


	/**
	 * Render Meta Box content.
	 *
	 * @param WP_Post $post The post object.
	 */
	public function render_meta_box_content( $post ) {
	
		// Add an nonce field so we can check for it later.
		wp_nonce_field( 'myplugin_inner_custom_box', 'myplugin_inner_custom_box_nonce' );
		
                global $c_results_meta_boxes;
                global $cweb_events_args;
                                                         
                
		echo '<table class="form-table">';

		foreach ($c_results_meta_boxes['0'] as $field) {
			$meta = get_post_meta($post->ID, $field['id'], true);
			//$meta = !empty($meta) ? $meta : $field['std'];
                        
			echo '<tr>';
			// call separated methods for displaying each type of field
			//call_user_func(array(&$this, 'show_field_' . $field['type']), $field, $meta);
                        echo '<th><label for="myplugin_new_field">';
                            _e( ''.$field['name'].'', 'myplugin_textdomain' );
                        echo '</label></th><td> ';
                        if($field['type'] == 'selected'){
                            echo '<select id="'.$field['id'].'" name="'.$field['id'].'" style="width:95%">';
                            ?>
                            <option value="Temporary" <?php if($meta == 'Temporary' ){ echo 'selected=selected'; }?>>Temporary</option>
                            <option value="Final" <?php if($meta == 'Final' ){ echo 'selected=selected'; }?>>Final</option>
                            <?php
                            echo '</select>';
                        }else{
                        echo '<input type="'.$field['type'].'" id="'.$field['id'].'" name="'.$field['id'].'"';
                        echo ' value="' . esc_attr( $meta ) . '" style="width:95%" />';
                            echo '</td></tr>';                                                      
                            }
                        }
                        
                //print_r($cweb_events_args);exit;
                $get_events_args = get_posts( $cweb_events_args );
                //echo '<pre>';
                //print_r($get_events_args);exit;
                echo '<tr><th><label for="myplugin_new_field">';
                            _e( 'Select Event', 'myplugin_textdomain' );
                echo '</label></th><td>';
                echo '<select style="width:95%" name="events_result_name" id="main_team_list" onchange="showUser(this.value)">'
                . '<option value="">Select Event</option>';
                foreach ($get_events_args as $get_events_arg) {                 
                    $get_events_val = get_post_meta($post->ID, 'key_events_result_name', true);
                    $tobject=wp_get_post_terms($get_events_arg->ID, 'suddo_event_type');
                    if($tobject[0]->name!='Birthday'){
                    ?>
                    <option value="<?php echo $get_events_arg->ID ?>" <?php if($get_events_arg->ID ==  $get_events_val){ echo 'selected=selected';} ?>><?php echo $get_events_arg->post_title;?></option>
                    <?php
}
                }
                echo '</select>';
              
                echo '</td></tr>'; 
                
                echo '<tr><th></th><td>';
                    
                    $get_events_val1 = get_post_meta($post->ID, 'key_events_result_name', true);
                    if($get_events_val1 == '' || !isset($get_events_val1) || $get_events_val1 =='0'){
                        
                        echo '<p>Please select event first to get list of event riders</p>';
                        
                       ;
                    }else{
                    
                    $get_rider_meta_name = get_post_meta($post->ID, 'key_rider_marks', true);
                    if( ! empty( $get_rider_meta_name ) ) {
                        $uns_rider_meta_name = unserialize($get_rider_meta_name);
                        //print_r($uns_rider_meta_name);
                    } 
                    
                    $get_rider_meta_name1 = get_post_meta($post->ID, 'key_rider_award', true);
                    if( ! empty( $get_rider_meta_name1 ) ) {
                        $uns_rider_meta_name1 = unserialize($get_rider_meta_name1);
                        //print_r($uns_rider_meta_name);
                    }
                    
                    $key_rider_res_ico = get_post_meta($post->ID, 'key_rider_res_ico', true);
                    if( ! empty( $key_rider_res_ico ) ) {
                        $key_rider_res_ico = unserialize($key_rider_res_ico);
                        //print_r($uns_rider_meta_name);
                    }
                    
                    //Check if events exists
                    if ( FALSE === get_post_status( $get_events_val1 ) ) {
                                // The post does not exist                    
                    } else { 
                    $get_event_rider = get_post_meta($get_events_val1, 'riderskey', true);
                    if( ! empty( $get_event_rider ) ) {
                        $uns_event_rider = unserialize($get_event_rider);
                        //print_r($getunserary);
                        if(isset($uns_event_rider) || $uns_event_rider !=''){
                        echo '<table class="form-table"><tr><th>Name</th><th style="text-align: center;">Ranking</th><th style="text-align: center;">Prize</th><th style="text-align: center;">Image</th><th style="text-align: center;">Preview</th></tr>';
                        $i = 0;                           
                        foreach($uns_event_rider as $key=>$value){ 
                            $key_rider_award_icon = get_post_meta($post->ID, 'key_rider_award_icon'.$i, true);                            
                            //echo '<pre>';
                            //print_r($key_rider_award_icon);
                            //
                            //Check if rider exists
                            if ( FALSE === get_post_status( $value ) ) {
                                // The post does not exist                    
                            } else {                              
                            echo '<tr><th style="width: 25%;vertical-align: middle;">'; 
                            echo get_the_title( $value );
                            echo '<input type="hidden" style="width:95%" name="rider_id[]" value="'.$value.'"  />';                            
                            echo '</th><td>'; 
                            echo '<input type="text" style="width:95%" name="rider_marks[]" value="'.$uns_rider_meta_name[$i].'"  />';                            
                            echo '</td><td><input type="text" style="width:95%" name="rider_award[]" value="'.$uns_rider_meta_name1[$i].'"  /></td>';                            
                            echo '<td>'; 
                            
                            echo '<div align="center"><input type="text" style="width:95%" name="rider_res_ico[]" value="'.$key_rider_res_ico[$i].'"  /></div>';                            
                            echo '</td>';
                            
                            echo '<td>';
                            if($key_rider_res_ico[$i] != ''){
                                echo '<div align="center"><img src="'.$key_rider_res_ico[$i].'" style="max-width:95px" /></div>';
                            }
                            echo '</td>';
                            echo '</tr>';
                            
                            }
                            $i++;
                        }
                        echo '</table>';
                       
                        
                        
                        }
                        
                    } 
                    }
                echo '</td></tr>';
                    }
                    
                    echo '<tr><td></td><td><p><input name="save" type="submit" class="button button-primary button-large" id="publish" accesskey="p" value="Update"></p></td></tr>';
		echo '</table>';            
                
                echo '<br/><br/><br/>';
                echo '<h3 class="hndle ui-sortable-handle"><span>Upload Image Here</span></h3>';
                echo '<div><iframe style="width:100%; height:500px" src="'.get_site_url().'/wp-admin/media-upload.php?post_id=2&flash=0&type=image&tab=type"></iframe></div>';
	}
        
        function post_edit_form_tag( ) {
            echo ' enctype="multipart/form-data"';
        }
        
        
        
}


;