<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the dashboard.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Plugin_Name
 * @subpackage Plugin_Name/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, dashboard-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    Plugin_Name
 * @subpackage Plugin_Name/includes
 * @author     Your Name <email@example.com>
 */
/**
 * Calls the class on the post edit screen.
 */
function call_staffCustomFeilds() {
    new staffCustomFeilds();
}

if ( is_admin() ) {
    add_action( 'load-post.php', 'call_staffCustomFeilds' );
    add_action( 'load-post-new.php', 'call_staffCustomFeilds' );
}


$prefix = 'rd';
$staff_meta_boxes[] =  array(                          // list of meta fields
                    array(
                        'name' => __('First Name','wp-team-manager'),                 // field name
                        'desc' => __('First Name','wp-team-manager'), // field description, optional
                        'id' => $prefix . 'FirstName',              // field id, i.e. the meta key
                        'type' => 'text',                       // text box
                        'std' => ''                    // default value, optional
                    ),
                    array(
                        'name' => __('Last Name','wp-team-manager'),                 // field name
                        'desc' => __('Last Name','wp-team-manager'), // field description, optional
                        'id' => $prefix . 'LastName',              // field id, i.e. the meta key
                        'type' => 'text',                       // text box
                        'std' => ''                    // default value, optional
                    ),
                    array(
                        'name' => __('Gender','wp-team-manager'),                 // field name
                        'desc' => __('Birthday','wp-team-manager'), // field description, optional
                        'id' => $prefix . 'Gender',              // field id, i.e. the meta key
                        'type' => 'selected',                       // text box
                        'std' => ''                    // default value, optional
                    ),
                    array(
                        'name' => __('Birthday','wp-team-manager'),                  // field name
                        'desc' => __('Birthday','wp-team-manager'), // field description, optional
                        'id' => $prefix . 'datepicker',              // field id, i.e. the meta key
                        'type' => 'text',                       // text box
                        'std' => ''                    // default value, optional
                    ),
                    array(
                        'name' => __('Nationality','wp-team-manager'),                  // field name
                        'desc' => __('Nationality','wp-team-manager'), // field description, optional
                        'id' => $prefix . 'Nationality',              // field id, i.e. the meta key
                        'type' => 'natselected',                       // text box
                        'std' => ''                    // default value, optional
                    ),                   
                    array(
                        'name' => __('Weight','wp-team-manager'),                  // field name
                        'desc' => __('Weight','wp-team-manager'), // field description, optional
                        'id' => $prefix . 'Weight',              // field id, i.e. the meta key
                        'type' => 'text',                       // text box
                        'std' => ''                    // default value, optional
                    ),        
                    array(
                        'name' => __('Height','wp-team-manager'),                 // field name
                        'desc' => __('Height','wp-team-manager'), // field description, optional
                        'id' => $prefix . 'Height',              // field id, i.e. the meta key
                        'type' => 'text',                       // text box
                        'std' => ''                    // default value, optional
                    ),
                    array(
                        'name' => __('Professional Since','wp-team-manager'),                  // field name
                        'desc' => __('Professional Since','wp-team-manager'), // field description, optional
                        'id' => $prefix . 'Professional',              // field id, i.e. the meta key
                        'type' => 'text',                       // text box
                        'std' => ''                    // default value, optional
                    ),
                    array(
                        'name' => __('Personal Twitter Handle','wp-team-manager'),                  // field name
                        'desc' => __('Personal Twitter Handle','wp-team-manager'), // field description, optional
                        'id' => $prefix . 'Twitter',              // field id, i.e. the meta key
                        'type' => 'text',                       // text box
                        'std' => ''                    // default value, optional
                    ),
                    array(
                        'name' => __('Twitter Data Widget ID','wp-team-manager'), //field name
                        'desc' => __('Twitter Data Widget ID','wp-team-manager'), //description, optional
                        'id' => $prefix . 'TwitterWidgetID', //meta key
                        'type' => 'text', //text box
                        'std' => '' //default value, optional
                    ),
                    array(
                        'name' => __('Personal Website','wp-team-manager'),                  // field name
                        'desc' => __('Personal Website','wp-team-manager'), // field description, optional
                        'id' => $prefix . 'Website',              // field id, i.e. the meta key
                        'type' => 'text',                       // text box
                        'std' => ''                    // default value, optional
                    ),
                    
                    array(
                        'name' => __('Description / Abstract','wp-team-manager'),                  // field name
                        'desc' => __('Description / Abstract','wp-team-manager'), // field description, optional
                        'id' => $prefix . 'Description',              // field id, i.e. the meta key
                        'type' => 'textarea',                       // text box
                        'std' => ''                    // default value, optional
                    ),
                   
);


$cweb_palmars_args = array(
                                    'posts_per_page'   => 200,
                                    'offset'           => 0,
                                    'category'         => '',
                                    'category_name'    => '',
                                    'orderby'          => 'post_date',
                                    'order'            => 'DESC',
                                    'include'          => '',
                                    'exclude'          => '',
                                    'meta_key'         => '',
                                    'meta_value'       => '',
                                    'post_type'        => 'cwebc_palmares',
                                    'post_mime_type'   => '',
                                    'post_parent'      => '',
                                    'post_status'      => 'publish',
                                    'suppress_filters' => 0 );

/** 
 * The Class.
 */
class staffCustomFeilds {
        
	/**
	 * Hook into the appropriate actions when the class is constructed.
	 */
	public function __construct() {
            
		add_action( 'add_meta_boxes', array( $this, 'add_meta_box' ) );
		add_action( 'save_post', array( $this, 'save' ) );
                
              
                
	}

	/**
	 * Adds the meta box container.
	 */
	public function add_meta_box( $post_type ) {
            $post_types = array('suddo_staff',);     //limit meta box to certain post types
            if ( in_array( $post_type, $post_types )) {
		add_meta_box(
			'some_meta_box_name'
			,__( 'Staff More Details', 'myplugin_textdomain' )
			,array( $this, 'render_meta_box_content' )
			,$post_type
			,'advanced'
			,'high'
		);
            }
	}

	/**
	 * Save the meta when the post is saved.
	 *
	 * @param int $post_id The ID of the post being saved.
	 */
	public function save( $post_id ) {
                global $staff_meta_boxes;
		/*
		 * We need to verify this came from the our screen and with proper authorization,
		 * because save_post can be triggered at other times.
		 */

		// Check if our nonce is set.
		if ( ! isset( $_POST['myplugin_inner_custom_box_nonce'] ) )
			return $post_id;

		$nonce = $_POST['myplugin_inner_custom_box_nonce'];

		// Verify that the nonce is valid.
		if ( ! wp_verify_nonce( $nonce, 'myplugin_inner_custom_box' ) )
			return $post_id;

		// If this is an autosave, our form has not been submitted,
                //     so we don't want to do anything.
		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) 
			return $post_id;

		// Check the user's permissions.
		if ( 'page' == $_POST['post_type'] ) {

			if ( ! current_user_can( 'edit_page', $post_id ) )
				return $post_id;
	
		} else {

			if ( ! current_user_can( 'edit_post', $post_id ) )
				return $post_id;
		}

		/* OK, its safe for us to save the data now. */
                
              
		// Sanitize the user input.
		$mydata = sanitize_text_field( $_POST['rider_gender'] );
                $mydata1 = sanitize_text_field( $_POST['rider_birthday'] );
                
		// Update the meta field.
		update_post_meta( $post_id, 'key1', $mydata );
                
                update_post_meta( $post_id, 'key2', $mydata1 );
                foreach ($staff_meta_boxes['0'] as $field) {
                    update_post_meta( $post_id, $field['id'], $_POST[$field['id']] );
                }
                
                //Save Riders
                $pl_name = serialize( $_POST['pl_name'] );  
                update_post_meta( $post_id, 'key_pl_name', $pl_name );
	}


	/**
	 * Render Meta Box content.
	 *
	 * @param WP_Post $post The post object.
	 */
	public function render_meta_box_content( $post ) {
	
		// Add an nonce field so we can check for it later.
		wp_nonce_field( 'myplugin_inner_custom_box', 'myplugin_inner_custom_box_nonce' );
		
                global $staff_meta_boxes;
                global $cweb_palmars_args;
                global $countries;
                global $s_countries;
                
                
                
                echo '<link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">

  <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>';

                
		echo '<table class="form-table">';

		foreach ($staff_meta_boxes['0'] as $field) {
			$meta = get_post_meta($post->ID, $field['id'], true);
			//$meta = !empty($meta) ? $meta : $field['std'];

			echo '<tr>';
			// call separated methods for displaying each type of field
			//call_user_func(array(&$this, 'show_field_' . $field['type']), $field, $meta);
                        echo '<th><label for="myplugin_new_field">';
                            _e( ''.$field['name'].'', 'myplugin_textdomain' );
                        echo '</label></th><td> ';
                        
                         if($field['type'] == 'selected'){
                            echo '<select id="'.$field['id'].'" name="'.$field['id'].'" style="width:95%">';
                            ?>
                            <option value="Male" <?php if($meta == 'Male' ){ echo 'selected=selected'; }?>>Male</option>
                            <option value="Female" <?php if($meta == 'Female' ){ echo 'selected=selected'; }?>>Female</option>
                            <?php
                            echo '</select>';
                        }elseif($field['type'] == 'natselected'){                            
                            
                           
                            echo '<select id="'.$field['id'].'" name="'.$field['id'].'" style="width:95%">';
                            foreach ($s_countries as $countid=>$contval){
                            ?>
                            <option value="<?php echo $contval; ?>" <?php if($meta == $contval ){ echo 'selected=selected'; }?>><?php echo __ ($countries[$contval]); ?></option>                           
                            <?php                            
                            }
                            echo '</select>';
                        }elseif($field['type'] == 'textarea'){
                            echo '<textarea style="width:95%" id="'.$field['id'].'" name="'.$field['id'].'">' . esc_attr( $meta ) . '</textarea>';
                        }else{
                        echo '<input type="'.$field['type'].'" id="'.$field['id'].'" name="'.$field['id'].'"';
                        echo ' value="' . esc_attr( $meta ) . '" style="width:95%" />';
                            echo '</td></tr>';    
                            wp_reset_postdata();
                        }
                        }
                          
                
		echo '</table>';                                              
	}                                       
}





