<?php
class cycling_events extends WP_Widget {
    
function __construct() {
parent::__construct(
// Base ID of your widget
'cycling_events', 

// Widget name will appear in UI
__('Recent Events Display', 'wpb_widget_domain'), 

// Widget description
array( 'description' => __( 'Display Recent Cycling Events', 'wpb_widget_domain' ), ) 
);
}

// Creating widget front-end
// This is where the action happens
public function widget( $args, $instance ) {
global $smof_data;
$title = apply_filters( 'widget_title', $instance['title'] ); ?>

<?php
$no_of_events =  $instance['no_of_events'];
 $events_type =  $instance['events_type'];

//$icl_id_of_post = icl_object_id($events_type, 'suddo_event_type', FALSE, ICL_LANGUAGE_CODE);
//echo $icl_id_of_post; exit;

// before and after widget arguments are defined by themes

 //echo $translated= get_post_meta(icl_object_id($posts->ID,'events',false,ICL_LANGUAGE_CODE), '_sample_key', true);


$current_date=date("m/d/Y");

echo $args['before_widget'];

 global $wpdb;



                     $defaults = array(
                      'post_type' => 'events',
          
		   //'taxonomy' => 'suddo_event_type',
                      'post_status' => 'publish',
		    // 'term' => $events_type,
                     'showposts' => $no_of_events,
                     'suppress_filters'=> '0'
             
                      
                     
                    
                   );

//$query = new WP_Query( $defaults );
 $queryRec = get_posts($defaults);
//print_r($queryRec); //exit;
 if(!empty($queryRec)):
     foreach($queryRec as $key=>$val):

      //echo  $events_date= $val->post_date;

        $events_date= get_post_meta($val->ID, 'eventDateofEvent', 'true');
        if(strtotime($events_date)<strtotime(date('Y-m-d'))):			
            unset($queryRec[$key]);
        else:
		$val->evtDate=$events_date;
		$queryRec[$key]=$val;
        endif;       
     endforeach;     
 endif;

 
//echo '<pre>'; print_r($queryRec); exit;
function sortByOrder($a, $b) {
    return strtotime($a->evtDate) - strtotime($b->evtDate);
}

usort($queryRec, 'sortByOrder');

  //echo '<pre>';
  //print_r($query);
//exit;
?>
<style type="text/css">
    .widget_cycling_events h3 {

background: #F6F6F6;
padding: 10px !important;
color: #000 !important;
text-align: center;
}

.events_sidebar {
padding: 1px 0;
background: #F6F6F6;
 
}
.main_events {

border-bottom:1px solid #c3c3c3;
}
    div.main_events h3 {color:<?php echo $smof_data['primary_color']; ?> !important; text-align: center; cursor:pointer;
padding: 0px !important;
}

div.main_events h3 a {color:<?php echo $smof_data['primary_color']; ?> !important; text-align: center; cursor:pointer; }
    
.main_events:last-child {
border-bottom:none;
}
</style>

<h3> <?php echo $title; ?></h3>


<div class="events_sidebar">

<?php

foreach ($queryRec as $posts){

$translated = icl_object_id($posts->ID,'events',ICL_LANGUAGE_CODE);

 //$events = get_post(icl_object_id($posts->ID, 'events', false));.



$events_date= get_post_meta($posts->ID, 'eventDateofEvent', 'true');

  ?>  
<div class="main_events">

<p style="text-align:center;"><?php echo $events_date; ?></p>
<h3 style="<?php echo $smof_data['primary_color']; ?> text-transform:capitalize;"><a href="<?php echo get_permalink($posts->ID); ?>" style="<?php echo $smof_data['primary_color']; ?>"><?php echo $posts->post_title; ?></a></h3>
<!--<p style="text-transform:capitalize;"><?php echo $posts->post_content; ?>  </p>-->   


</div> 
  <?php 
 
}
?></div>
</div>
<?php }

// Widget Backend 
public function form( $instance ) {
 
$title = $instance[ 'title' ];
$events_type=$instance[ 'events_type' ];
$no_of_events = $instance[ '$no_of_events' ];

 
 
// Widget admin form
?>
<?php
$teams_args = array(
                        'type'                     => 'events',              
                        'orderby'                  => 'name',
                        'order'                    => 'ASC',                       
                        'taxonomy'                 => 'suddo_event_type',                      
                         
); 
                $teams_categories = get_categories( $teams_args );
               //echo '<pre>';
                //print_r($teams_categories); ?>
                
<p>
<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label> 
<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
</p>

<!--<p>
<label for="<?php echo $this->get_field_id( 'Select_team' ); ?>"><?php _e( 'Select_team:' ); ?></label> 
<select name="<?php echo $this->get_field_name( 'events_type' ); ?>" style="width: 100%;">
<?php 
foreach ($teams_categories as $print){ ?>
    <option value="<?php echo $print->slug; ?>" <?php echo ($instance['events_type']==$print->slug)?'selected':''; ?> ><?php echo $print->cat_name; ?></option>
<?php  } ?>
</select>
 
</p>
-->
<p>
<label for="<?php echo $this->get_field_id( 'NO of Events Display' ); ?>"><?php _e( 'NO of Events Display' ); ?></label> 
<input class="widefat" id="<?php echo $this->get_field_id( 'no_of_events' ); ?>" name="<?php echo $this->get_field_name( 'no_of_events' ); ?>" type="text" value="<?php echo $instance['no_of_events'];; ?>" />
</p>

<?php 
}
	
// Updating widget replacing old instances with new
public function update( $new_instance, $old_instance ) {
echo $instance = array();
print_r($instance);
$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
$instance['no_of_events'] = $new_instance['no_of_events'];
$instance['events_type'] = $new_instance['events_type'];
return $instance;
}
} // Class wpb_widget ends here

// Register and load the widget
function cycling_load_widget() {
	register_widget( 'cycling_events' );
}
add_action( 'widgets_init', 'cycling_load_widget' );
