<?php
class wpb_widget extends WP_Widget {

    function __construct() {
        parent::__construct(
// Base ID of your widget
                'wpb_widget',
// Widget name will appear in UI
                __('Responsive Calender Widget', 'wpb_widget_domain'),
// Widget description
                array('description' => __('Responsive Calender Widget Based On Event Calender', 'wpb_widget_domain'),)
        );
    }

// Creating widget front-end
// This is where the action happens
    public function widget($args, $instance) {
        global $smof_data;
        
        $title = apply_filters('widget_title', $instance['title']);
// before and after widget arguments are defined by themes
        echo $args['before_widget'];
        if (!empty($title))
            echo $args['before_title'] . __($title) . $args['after_title'];

// This is where you run the code and display the output
//echo __( 'Hello, World!', 'wpb_widget_domain' );
        echo $args['after_widget'];
        ?>
        
        <!-- Style For Avada Theme -->
        <style type="text/css">
            .responsive-calendar .btn-primary{
                background-color: <?php echo $smof_data['primary_color']; ?>;
                border-color: <?php echo $smof_data['primary_color']; ?>;
            }
            .responsive-calendar .btn-primary:hover, .responsive-calendar .btn-primary:focus, .responsive-calendar .btn-primary:active, .responsive-calendar .btn-primary.active, .responsive-calendar .open .dropdown-toggle.btn-primary{
                background-color: <?php echo $smof_data['primary_color']; ?>;
                border-color: <?php echo $smof_data['primary_color']; ?>;
                opacity: 0.7;
                -ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=70)"; // IE8
                filter: alpha(opacity=70); // IE 5-7
            }
            .responsive-calendar .day.active a{
                background-color: <?php echo $smof_data['primary_color']; ?> !important;
                background: <?php echo $smof_data['primary_color']; ?> !important;
            }
            .responsive-calendar .day.active a:hover{
                background-color: <?php echo $smof_data['primary_color']; ?> !important;
                background: <?php echo $smof_data['primary_color']; ?> !important;
                opacity: 0.7;
                -ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=70)"; // IE8
                filter: alpha(opacity=70); // IE 5-7
            }
            .responsive-calendar .badge{
                display: none;
            }
        </style>
        <div class="responsive-calendar">
<div class="controls">
                <a class="pull-left" data-go="prev"><div class="btn btn-primary"><?php _e('Prev'); ?></div></a>
                <h4> <span data-head-month></span> <span data-head-year></span></h4>
                <a class="pull-right" data-go="next"><div class="btn btn-primary"><?php _e('Next'); ?></div></a>
            </div><hr/>
            <div class="day-headers">
                <div class="day header"><?php _e('Mon');?></div>
                <div class="day header"><?php _e('Tue');?></div>
                <div class="day header"><?php _e('Wed');?></div>
                <div class="day header"><?php _e('Thu');?></div>
                <div class="day header"><?php _e('Fri');?></div>
                <div class="day header"><?php _e('Sat');?></div>
                <div class="day header"><?php _e('Sun');?></div>
            </div>
            <div class="days" data-group="days">

            </div>
        </div><?php
        $get_all_events_for_cal = array(
            'posts_per_page' => 200,
            'offset' => 0,
            'category' => '',
            'category_name' => '',
            'orderby' => 'post_date',
            'order' => 'ASC',
            'include' => '',
            'exclude' => '',
            'post_type' => 'events',
            'post_mime_type' => '',
            'post_parent' => '',
            'post_status' => 'publish',
            'suppress_filters' => 0);

        if (isset($_GET['team'])) {
            //Convert term slug into id
            $catinfo = get_term_by('slug', $_GET['team'], 'teams');

            $get_all_events_for_cal['meta_key'] = 'key_event_team_name';
            $get_all_events_for_cal['meta_value'] = $catinfo->term_id;
        }

        $fetch_all_events_for_cal = get_posts($get_all_events_for_cal);
//print_r($fetch_all_events_for_cal);

        foreach ($fetch_all_events_for_cal as $fetch_all_events_for_cals) {
            $get_eventDateofEventForCal = get_post_meta($fetch_all_events_for_cals->ID, 'eventDateofEvent', true);
            $newDate = date("Y-m-d", strtotime($get_eventDateofEventForCal));
        }
        ?>
        <?php echo responsive_calender_files(); ?>
        <script type="text/javascript">
            jQuery(document).ready(function () {
                //alert('sdsdsadadsadd');
                jQuery(".responsive-calendar").responsiveCalendar({
                time: '<?php echo date('Y-m') ?>',
                        events: {

                        //"2015-02-15": {"number": 5, "url": "http://w3widgets.com/responsive-slider"} }

        <?php
        if (!empty($fetch_all_events_for_cal)):

            foreach ($fetch_all_events_for_cal as $key => $val):
                $term_list = wp_get_post_terms($val->ID, 'suddo_event_type', array("fields" => "ids"));
                $tax_idss = $term_list[0];
                $term_meta = get_option("taxonomy_$tax_idss");
                $geteventtypecolor = esc_attr($term_meta['custom_term_meta']);
                $hetdbsasd = get_post_meta($val->ID, 'eventDateofEvent', true);
                $get_eventDateofEventForCal = get_post_meta($val->ID, 'eventDateofEvent', true);
                $newDate = date("Y-m-d", strtotime($get_eventDateofEventForCal));
                if ($hetdbsasd != '') {
                    ?>

                    <?php echo ' "' . $newDate . '" : {"number": 1, "url": "' . get_permalink($val->ID) . '"},' ?>

                    <?php
                    //echo '{  
                    //"'.get_post_meta($val->ID, 'eventDateofEvent', true).'": {'
                    // . '"number": 1, "url": "'.htmlspecialchars_decode($val->guid).'" 
                    //},';     
                    //echo  '},'; 
                    ?>
                                    //"'.get_post_meta($val->ID, 'eventDateofEvent', true).'": {"number": 1, "url": "'.htmlspecialchars_decode($val->guid).'", "title": "'.$val->post_title.'",';} echo ';

                                    // "2013-05-03":{"number": 1}, 

                <?php
                }

            endforeach;
        endif;
        ?>
                        }
            });
            });
        </script>
        <?php
    }

// Widget Backend 
    public function form($instance) {
        if (isset($instance['title'])) {
            $title = $instance['title'];
        } else {
            $title = __('New title', 'wpb_widget_domain');
        }
// Widget admin form
        ?>
        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?></label> 
            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" />
        </p>

        <?php
    }

// Updating widget replacing old instances with new
    public function update($new_instance, $old_instance) {
        $instance = array();
        $instance['title'] = (!empty($new_instance['title']) ) ? strip_tags($new_instance['title']) : '';
        return $instance;
    }

}

// Class wpb_widget ends here
// Register and load the widget
function wpb_load_widget() {
    register_widget('wpb_widget');
}

add_action('widgets_init', 'wpb_load_widget');
?>
