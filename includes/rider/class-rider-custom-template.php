<?php
/**
 * The file that defines the rider
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the dashboard.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Plugin_Name
 * @subpackage Plugin_Name/includes
 */
/**
 * The core plugin class.
 *
 * This is used to define internationalization, dashboard-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    Plugin_Name
 * @subpackage Plugin_Name/includes
 * @author     Your Name <email@example.com>
 */
/**
 * Calls the class on the post edit screen.
 */
get_header();


global $post;
global $smof_data;


$cweb_events_args_temp = array(
    'posts_per_page' => 200,
    'offset' => 0,
    'category' => '',
    'category_name' => '',
    'meta_key' => 'eventDateofEvent',
    'orderby' => 'meta_value',
    'order' => 'DESC',
    'include' => '',
    'exclude' => '',    
    'meta_value' => '',
    'post_type' => 'events',
    'post_mime_type' => '',
    'post_parent' => '',
    'post_status' => 'publish',
    'suppress_filters' => 0);
?>

<div id="content" style="float: left;width:71% !important; margin:0 !important">

    <div class="fusion-one-third one_third fusion-layout-column fusion-column spacing-yes visible-desktop" style="background: #F6F6F6; padding: 10px;
         ">
        <div class="fusion-title title">
            <h1 class="title-heading-left" style="font-weight: normal;"><?php $post_categories = get_the_terms($post->ID, 'teams');
foreach ($post_categories as $term) {
    echo $draught_links = $term->name;
    $team_slug_nm = $term->slug;
} ?></h1>
            <div class="title-sep-container"><!--<div class="title-sep sep-single"></div>--></div>

        </div>
<?php
$get_all_riders_left = array(
    'posts_per_page' => 200,
    'offset' => 0,
    'category' => '',
    'category_name' => '',
    'orderby' => 'meta_value',
    'order' => 'ASC',
    'include' => '',
    'exclude' => '',
    'meta_key' => 'rdLastName',
    'meta_value' => '',
    'tax_query' => array(
        array(
            'taxonomy' => 'teams',
            'key' => 'rdLastName',
            'field' => 'slug',
            'terms' => $team_slug_nm)),
    'post_type' => 'riders',
    'post_mime_type' => '',
    'post_parent' => '',
    'post_status' => 'publish',
    'suppress_filters' => 0);
?>
        <div>
            <ul class="fusion-checklist">
<?php
$get_all_riders_left_posts = get_posts($get_all_riders_left);
foreach ($get_all_riders_left_posts as $get_all_riders_left_post):
    ?>
                    <li class="fusion-li-item size-small " style="text-transform: capitalize;width: 100%; ">                    
                        <a href="<?php echo get_the_permalink($get_all_riders_left_post->ID); ?>" style="height: auto;line-height: 2em !important; <?php if ($get_all_riders_left_post->ID == $post->ID) {
                    echo 'color:' . $smof_data['primary_color'] . ' ;font-weight:bold';
                } else {
                    echo 'color: #747474';
                } ?>" ><b style="font-size: 18px; font-weight: normal;">&GT;</b>  
                    <?php echo get_post_meta($get_all_riders_left_post->ID, 'rdFirstName', true); ?> <?php echo get_post_meta($get_all_riders_left_post->ID, 'rdLastName', true); ?> </a>
                    </li>
                    <?php
                endforeach;
                ?>    
            </ul>
        </div>
    </div>
    <div class="fusion-two-third two_third fusion-layout-column fusion-column last spacing-yes ">
     
        <div class="fusion-two-third two_third fusion-layout-column fusion-column  spacing-yes">
<?php $image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full'); ?>
            <img class="person-img img-responsive" width="333px" src="<?php echo $image[0]; ?>" alt="">
        </div>
        <div class="fusion-one-third one_third fusion-layout-column fusion-column last spacing-yes">
            <ul class="fusion-checklist">
                <li class="fusion-li-item size-small">                    
                    <b> <?php echo __('Nationality'); ?>:</b> <br/>
                    <span style="color: #000">
                        <?php
                        $getnaid = get_post_meta($post->ID, 'rdNationality', true);
                        // print_r($countries);

                        echo __($countries[$getnaid]);
                        ?>                        
                    </span>
                    <span ><img src="http://upload.wikimedia.org/wikipedia/commons/d/d5/Blank_-_Spacer.png" class="flag flag-<?php echo strtolower($getnaid); ?>" alt="<?php echo __($countries[$getnaid]); ?>" style="margin-top: 4%;" /></span>
                </li>
                <li class="fusion-li-item size-small">                    
                    <b><?php echo __('Birthday') ?>: </b><br/>
                    <span style="color: #000">
                        <?php
                        $rider_birth = get_post_meta($post->ID, 'rddatepicker', true);
                        echo __(date("d/m/Y", strtotime($rider_birth)))
                        ?>
                    </span>
                </li>
                <li class="fusion-li-item size-small">                    
                    <b><?php echo __('Weight'); ?>: </b><br/>
                    <span style="color: #000"><?php echo get_post_meta($post->ID, 'rdWeight', true); ?></span>
                </li>
                <li class="fusion-li-item size-small">                    
                    <b><?php echo __('Length'); ?>: </b><br/>
                    <span style="color: #000"><?php echo get_post_meta($post->ID, 'rdHeight', true); ?></span>
                </li>                
                <li class="fusion-li-item size-small"> 
                    <b><?php echo __('Professional since (start date)'); ?>:  </b><br/>
                    <span style="color: #000"><?php echo get_post_meta($post->ID, 'rdProfessional', true); ?></span>
                </li>               
                <?php
                $test = get_post_meta($post->ID, 'rdWebsite', true);
                if ($test == '') {
                    echo '';
                } else {
                    ?>
                    <li class="fusion-li-item size-small">
                        <b><?php echo __('Visit Website'); ?>: </b><br/>
                        <span style="color: #ee2e24"><a style="color: #ee2e24" href="<?php echo get_post_meta($post->ID, 'rdWebsite', true); ?>" target="_blank" ><?php echo __('Link'); ?></a></span>
                    </li>

                <?php } ?>
                <?php
                $text = get_post_meta($post->ID, 'rdTwitter', true);
                if ($text == '') {
                    echo '';
                } else {
                    ?>
                    <li class="fusion-li-item size-small">
                        <b><?php echo __('Visit Twitter Profile'); ?>:</b><br/>
                        <span style="color: #ee2e24"><a  style="color: #ee2e24" href="<?php echo get_post_meta($post->ID, 'rdTwitter', true); ?>" target="_blank" ><?php echo __('Link'); ?></a></span>
                    </li>
<?php } ?>

            </ul> 
        </div>
        <div class="clear"></div>

        <div><?php echo get_post_meta($post->ID, 'rdDescription', true); ?></div>
        
         <?php
        //Get Rider Events
        $show='0';
        $get_cweb_events_temp = get_posts($cweb_events_args_temp);
        //print_r($get_cweb_events_temp);
        foreach ($get_cweb_events_temp as $get_cweb_events_temps) {
            //echo $get_cweb_events_temps->ID;
            $getserevents = get_post_meta($get_cweb_events_temps->ID, 'riderskey', true);
            if (!empty($getserevents)) {
                $ungetserevents = unserialize($getserevents);
            }
            if (in_array($post->ID, $ungetserevents)) {
                //echo $get_cweb_events_temps->ID. ', ';
                $get_results_ofevent_args = array(
                    'posts_per_page' => 200,
                    'offset' => 0,
                    'category' => '',
                    'category_name' => '',
                    'orderby' => 'post_date',
                    'order' => 'DESC',
                    'include' => '',
                    'exclude' => '',
                    'meta_key' => 'key_events_result_name',
                    'meta_value' => $get_cweb_events_temps->ID,
                    'post_type' => 'event_results',
                    'post_mime_type' => '',
                    'post_parent' => '',
                    'post_status' => 'publish',
                    'suppress_filters' => 0);

                //Get Result Of Events
                $get_results_ofevents = get_posts($get_results_ofevent_args);
                //echo '<pre>';
                //print_r($get_results_ofevents);
		

                if (count($get_results_ofevents)) {
                     $show ='1';
                }
                
               
            }
        }
?>
        
        
       <?php if($show == '1'): ?> 
        
        <div style="background: #F6F6F6;padding: 2%; margin-top: 4%">
                        <h3 style="padding: 0;margin: 0;"><?php echo __('Results Rider'); ?></h3><hr style="height:1px;border:0px;background-color:#c3c3c3;"/>  
                        <table style="width:100%">

                        
        <?php
        //Get Rider Events
        $get_cweb_events_temp = get_posts($cweb_events_args_temp);
        //print_r($get_cweb_events_temp);
        foreach ($get_cweb_events_temp as $get_cweb_events_temps) {
            //echo $get_cweb_events_temps->ID;
            $getserevents = get_post_meta($get_cweb_events_temps->ID, 'riderskey', true);
            if (!empty($getserevents)) {
                $ungetserevents = unserialize($getserevents);
            }


            //echo '<pre>';
            //print_r($ungetserevents);
            //echo $post->ID;
            if (in_array($post->ID, $ungetserevents)) {
                ?>

                

                <?php
                $geteventdate = get_post_meta($get_cweb_events_temps->ID, 'eventDateofEvent', true);
                '<div>' . $geteventdate . ' <a href="' . get_permalink($get_cweb_events_temps->ID) . '">' . $get_cweb_events_temps->post_title . '</a></div>';


                //echo $get_cweb_events_temps->ID. ', ';
                $get_results_ofevent_args = array(
                    'posts_per_page' => 200,
                    'offset' => 0,
                    'category' => '',
                    'category_name' => '',
                    'orderby' => 'post_date',
                    'order' => 'DESC',
                    'include' => '',
                    'exclude' => '',
                    'meta_key' => 'key_events_result_name',
                    'meta_value' => $get_cweb_events_temps->ID,
                    'post_type' => 'event_results',
                    'post_mime_type' => '',
                    'post_parent' => '',
                    'post_status' => 'publish',
                    'suppress_filters' => 0);

                //Get Result Of Events
                $get_results_ofevents = get_posts($get_results_ofevent_args);
                //echo '<pre>';
                //print_r($get_results_ofevents);
		

                if (count($get_results_ofevents)) {
                    ?>

                    
                        <?php
                        }

                        foreach ($get_results_ofevents as $get_results_ofevents_g) {


                            //Get Palmers Of Riders from Result
                            $rider_metasjgjg = get_post_meta($get_results_ofevents_g->ID, 'key_rider_id', true);
                            //echo $rider_metasjgjg;
                            if (!empty($rider_metasjgjg)) {
                                $getunserarydfsfd = unserialize($rider_metasjgjg);
                                //print_r($getunserarydfsfd);
                                $get_key = array_search($post->ID, $getunserarydfsfd);
                                //echo $get_key.',';
                            }
                            // echo print_r($getunserarydfsfd);
                            $get_rider_meta_name1 = get_post_meta($get_results_ofevents_g->ID, 'key_rider_marks', true);
                            
                            // Get Result Icon Image
                            $rider_result_img = get_post_meta($get_results_ofevents_g->ID, 'key_rider_res_ico', true);
                            $rider_result_img = unserialize($rider_result_img);
				                            
			    $eventID=get_post_meta($get_results_ofevents_g->ID, 'key_events_result_name');
			    
                            $permalink = get_permalink($eventID[0]);

                            if (!empty($get_rider_meta_name1)) {

                                $uns_rider_meta_name1 = unserialize($get_rider_meta_name1);
                                //print_r($uns_rider_meta_name1);
                                echo '<tr>';
                                echo '<td style="width: 20%;">' . __(date("d/m/Y", strtotime($geteventdate))) . '</td>';
                                echo '<td><div>'; if($rider_result_img[0] != ''){ echo '<img src="'.$rider_result_img[0].'" style="width:16px;height:16px" />'; } echo'&nbsp;&nbsp;<a style="color:' . $smof_data['primary_color'] . ' ;text-decoration: underline;" href="' . $permalink . '">' . $get_results_ofevents_g->post_title . '</a> </td>';
                                echo '<td style="text-align:center"> ' . $uns_rider_meta_name1[$get_key] . '</div></td>';
                                echo '</tr>';
                            }
                        }
                        ?>
        <?php if (count($get_results_ofevents)) { ?>

                      
                    
                <?php } ?>
                <?php
            }
        }
        ?>
                      </table>
        </div>
        <?php endif; ?>
        <div class="clear"></div>

        <!-- == Fusion Builder Code == Start-->
        <?php
        if (get_the_content() != ''):
            ?>
            <div style="background: #F6F6F6;padding: 2%; margin-top: 4%">
                <h3 style="padding: 0;margin: 0;"><?php echo __('Palmares'); ?></h3>
                <table class="palmers-tbl" ><tr><td><?php echo the_content(); ?></td></tr></table>
                <div class="clear"></div>
            </div>
            <?php
        endif;
        ?>
        <!-- == Fusion Builder Code == Ends-->


        <!-- ===== Old Palmares Code ===== Start-->

        <?php /*  Commest Start
          $gettotalwins = get_post_meta( $post->ID, 'add-more-pal', true );

          if($gettotalwins == '' || $gettotalwins == '0'){

          }else{
          ?>
          <div style="background: #F6F6F6;padding: 2%; margin-top: 4%">


          <h3 style="padding: 0;margin: 0;"><?php echo __('Palmares');?></h3>
          <?php


          $addhorline = '';
          $record=array();

          for($no = 1; $no <= $gettotalwins; $no++){
          $getpamersquery = "SELECT
          t.post_id,t.meta_id as year_meta_id,t.meta_key as year_meta_key,t.meta_value as year_meta_value,
          t1.post_id,t1.meta_id as name_meta_id,t1.meta_key as name_meta_key,t1.meta_value as name_meta_value,
          t2.post_id,t2.meta_id as detail_meta_id,t2.meta_key as detail_meta_key,t2.meta_value as detail_meta_value
          FROM `wp_postmeta` as t
          LEFT JOIN (SELECT *  FROM `wp_postmeta` WHERE `meta_key` = 'PalmersName".$no."' AND `post_id` = $post->ID ) t1 on t1.post_id=t.post_id
          LEFT JOIN (SELECT *  FROM `wp_postmeta` WHERE `meta_key` = 'PalmersDetails".$no."' AND `post_id` = $post->ID ) t2 on t2.post_id=t.post_id
          WHERE t.meta_key= 'PalmersYear".$no."' AND t.post_id = $post->ID
          ";
          $result=$wpdb->get_results($getpamersquery, OBJECT);
          if(!empty($result)):
          foreach($result as $key=>$val):
          $record[$val->year_meta_value][]=$val;
          endforeach;
          endif;
          }
          krsort($record);

          echo "<table class='palmers-tbl'>";
          if(!empty($record)):
          foreach($record as $key=>$val):
          echo "<tr><td style='width: 20%;'>".$record[$key][0]->name_meta_value."</td><td style='color:".$smof_data['primary_color']." ;width: 20%;'>".$key."</td>
          <td ><b style='color: #c3c3c3;'>".count($record[$key])." Victories</b><br/>";
          if(!empty($record[$key])):
          foreach($record[$key] as $subkey=>$subVal):
          echo $subVal->detail_meta_value."<br/>";
          endforeach;
          endif;
          echo "</td></tr>";
          endforeach;
          endif;
          echo "</table>";


          ?>
          <div class="clear"></div>
          </div>
          <?php } */ ?>
        <!-- ===== Old Palmares Code ===== Ends-->

    </div>
</div>


<div id="sidebar" class="sidebar">
    <a style="color:#ee2e24;" class="twitter-timeline" href="https://twitter.com/<?php echo get_post_meta($post->ID, 'rdTwitter', true); ?>" data-widget-id="<?php echo get_post_meta($post->ID, 'rdTwitterWidgetID', true); ?>">Tweets by @<?php echo get_post_meta($post->ID, 'rdTwitter', true); ?></a>
    <script>!function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0], p = /^http:/.test(d.location) ? 'http' : 'https';
            if (!d.getElementById(id)) {
                js = d.createElement(s);
                js.id = id;
                js.src = p + "://platform.twitter.com/widgets.js";
                fjs.parentNode.insertBefore(js, fjs);
            }
        }(document, "script", "twitter-wjs");</script>
    <br/><br/>
<?php
dynamic_sidebar('Rider Sidebar');
?>       
</div>            


<div class="clear"></div>

<?php get_footer(); ?>

