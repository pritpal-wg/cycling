<?php
global $wpdb;


// no default values. using these as examples 
  $get_teams = get_terms( 'teams' );
  
  

?>
<div class="wrap">
<h2><?php echo __('Lotto Soudal Team Dashboard');?>​</h2>

<div id="welcome-panel" class="welcome-panel">
    <div class="welcome-panel-content">
	<h3><?php echo __('Welcome to  Lotto Soudal Cycling Team. Belgium');?>!</h3>
	<p class="about-description"></p>
	<div class="welcome-panel-column-container">
            <div class="welcome-panel-column" >
                <h4><?php echo __('Manage Teams');?></h4>
		<ul>			                    
                    <li><a href="<?php echo admin_url().'edit-tags.php?taxonomy=teams&post_type=riders'; ?>" class="welcome-icon welcome-add-page"><?php echo __('Add Teams');?></a></li>
                    <li><a href="<?php echo admin_url().'edit.php?post_type=riders'; ?>" class="welcome-icon dashicons-groups"><?php echo __('Manage Riders');?></a></li>
                    <li><a href="<?php echo admin_url().'edit.php?post_type=suddo_staff'; ?>" class="welcome-icon dashicons-businessman"><?php echo __('Manage Staff');?></a></li>
		</ul>	
            </div>
	<div class="welcome-panel-column">
		<h4><?php echo __('Next Steps');?></h4>
		<ul>
			<li><a href="<?php echo admin_url().'edit-tags.php?taxonomy=suddo_event_type&post_type=events'; ?>" class="welcome-icon dashicons-calendar"><?php echo __('Manage Events Types');?></a></li>
			<li><a href="<?php echo admin_url().'edit.php?post_type=events'; ?>" class="welcome-icon dashicons-calendar-alt"><?php echo __('Manage Events');?></a></li>
			<li><a href="<?php echo admin_url().'edit.php?post_type=event_results'; ?>" class="welcome-icon dashicons-pressthis"><?php echo __('Manage Results');?></a></li>
                        <li><a href="<?php echo admin_url().'edit.php?post_type=suddo_gallery'; ?>" class="welcome-icon dashicons-format-gallery"><?php echo __('Manage Gallery');?></a></li>
		</ul>
	</div>
	<div class="welcome-panel-column welcome-panel-last">
		<h4><?php echo __('Settings');?></h4>
		<ul>			                    
                    <li><a href="<?php echo admin_url().'admin.php?page=settings'; ?>" class="welcome-icon dashicons-list-view"><?php echo __('Countries');?></a></li>
                    <li><a href="<?php echo admin_url().'admin.php?page=settings'; ?>" class="welcome-icon dashicons-format-image"><?php echo __('Media');?></a></li>
		</ul>
	</div>
	</div>
    </div>
</div>
<style>
    #dashboard_right_now li a:before, #dashboard_right_now li span:before{ content: initial}
</style>
<div class="clear"></div>
<div id="dashboard-widgets-wrap">
    <div id="dashboard-widgets" class="metabox-holder">       
        <?php foreach($get_teams as $get_team):                 
            $get_all_riders_dashboard = array(
                                    'posts_per_page'   => 200,
                                    'offset'           => 0,
                                    'category'         => '',
                                    'category_name'    => '',
                                    'orderby'          => 'post_date',
                                    'order'            => 'DESC',
                                    'include'          => '',
                                    'exclude'          => '',
                                    'meta_key'         => '',
                                    'meta_value'       => '',
                                    'post_type'        => 'riders', 
                                    'tax_query'        => array(
                                                            array(
                                                            'taxonomy' => 'teams',
                                                            'field' => 'slug',
                                                            'terms' => $get_team->slug)),
                                    
                                    'post_mime_type'   => '',
                                    'post_parent'      => '',
                                    'post_status'      => 'publish',
                                    'suppress_filters' => true );
         $getr_all_riders_dashboardssss = get_posts( $get_all_riders_dashboard );
         
        // echo '<pre>';
        // print_r($getr_all_riders_dashboardssss);
         
        
        ?>                  
        <div id="postbox-container-1" class="postbox-container">
            <div id="normal-sortables" class="meta-box-sortables ui-sortable">
                <div id="dashboard_right_now" class="postbox " style="display: block;">
                <div class="handlediv" title="Click to toggle"><br></div><h3 class="hndle ui-sortable-handle"><span><?php echo $get_team->name; ?></span></h3>
                    <div class="inside">                        
                        <div id="activity-widget">                                                        
                            <div id="latest-comments" class="activity-block">
                                <h3 class="hndle ui-sortable-handle"><span>Riders</span></h3>
                                <div id="the-comment-list" data-wp-lists="list:comment">
                                    
                                    <ul class="riders-list">
                                            <?php
                                    if(!empty($getr_all_riders_dashboardssss)){
                                    
                                        foreach($getr_all_riders_dashboardssss as $getr_all_riders_dashboardsf):                                                                                      
                                    ?>
                                        <li style="margin: 0;padding-bottom: 5px;">
                                            <div id="comment-1" class="comment even thread-even depth-1 comment-item approved" style="padding-bottom: 0;">
                                        <?php $get_cimage = wp_get_attachment_image_src( get_post_thumbnail_id( $getr_all_riders_dashboardsf->ID ), array('50','50')); ?>
                                        <img alt="" src="<?php echo $get_cimage[0]; ?>" class="avatar avatar-50 photo avatar-default" style="height: 25px" >
                                        <div class="dashboard-comment-wrap" style="padding-left: 30px;">
                                            <h4 class="comment-meta" style="line-height: 13px;">                                                 
                                                <cite class="comment-author">                                                    
                                                        <?php //echo $getr_all_riders_dashboardsf->post_title; ?>
                                                        <?php echo get_post_meta( $getr_all_riders_dashboardsf->ID, 'rdFirstName', true );  ?> <?php echo get_post_meta( $getr_all_riders_dashboardsf->ID, 'rdLastName', true );  ?>                                                    
                                                </cite> 
                                        </h4>                                        
                                            <p class="row-actions" style="margin: 0;line-height: 9px;padding: 0;">
                                            <span class="reply hide-if-no-js"> 
                                                <a href="<?php echo $getr_all_riders_dashboardsf->guid; ?>" class="vim-r hide-if-no-js" title="Reply to this comment" target="_blank" style="font-size: 10px;" >View</a>
                                            </span>
                                            <span class="edit"> | 
                                                <a href="<?php echo get_edit_post_link( $getr_all_riders_dashboardsf->ID ); ?>" title="Edit comment" style="font-size: 10px;">Edit</a>
                                            </span>
                                            <span class="spam"> </span>
                                        </p>
                                        </div>
                                    </div>
                                            </li>
                                <?php endforeach;  
                                    }else{
                                        echo '<div style="padding: 8px 12px;">No Riders</div>';
                                    }
                                ?>                                                                                        
                                        </ul>
                                   
                                        
                                </div>
                            </div>
                            <br/>
                            <h3 class="hndle ui-sortable-handle"><span><?php echo __('Upcoming Events');?></span></h3>
                            <!--=====Upcoming Events=====-->
                            <?php $cwebco_post_dash_events = cwebco_get_posts('events', 'key_event_team_name', $get_team->term_id, '',''); 
                            if(!empty($cwebco_post_dash_events) ){
                            ?>
                            <table style="width: 100%;text-align: left;margin: 0 0 8px 12px;">
                                    <tr>
                                        <th>Name</th>
                                        <th>Team</th>
                                        <th>Date</th>
                                    </tr>
                                    <?php 
                                    
                                    //Get Events
                                    
                                    foreach ($cwebco_post_dash_events as $cwebco_post_dash_event): ?>
                                    <?php 
                                        $event_date = get_post_meta( $cwebco_post_dash_event->ID, 'eventDateofEvent', true ); 
                                        if(strtotime($event_date) >= strtotime(date("Y/m/d")) ){
                                    ?>
                                    <tr>
                                        <td><a href='<?php echo $cwebco_post_dash_event->guid; ?>'><?php echo $cwebco_post_dash_event->post_title; ?></a></td>
                                        <td>
                                            <?php 
                                                $geteventkey = get_post_meta( $cwebco_post_dash_event->ID, 'key_event_team_name', true );  
                                                $term = get_term( $geteventkey, 'teams' );
                                                echo $term->name;
                                            ?>
                                        </td>
                                        <td>
                                            <?php echo $event_date; ?>
                                        </td>
                                    </tr>
                                        <?php } 
                                    endforeach; ?>
                            </table>
                            <?php } else{ echo '<div style="padding: 8px 12px;">No Events</div>'; } ?>
                        </div>                                                
                    </div>
                </div>
            </div>
        </div>
        <?php endforeach; ?>
        
        
        
        
        
        
        
    </div>
</div>






</div>
