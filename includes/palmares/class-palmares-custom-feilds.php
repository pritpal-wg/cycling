<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the dashboard.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Plugin_Name
 * @subpackage Plugin_Name/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, dashboard-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    Plugin_Name
 * @subpackage Plugin_Name/includes
 * @author     Your Name <email@example.com>
 */
/**
 * Calls the class on the post edit screen.
 */
function call_palmaresCustomFeilds() {
    new palmaresCustomFeilds();
}

if ( is_admin() ) {
    add_action( 'load-post.php', 'call_palmaresCustomFeilds' );
    add_action( 'load-post-new.php', 'call_palmaresCustomFeilds' );
}


$prefix = 'pal';
$c_palmares_meta_boxes[] =  array(                          // list of meta fields

                    array(
                        'name' => __('Victories','wp-cycle-manager'),                 // field name
                        'desc' => __('Victories','wp-cyle-manager'), // field description, optional
                        'id' => $prefix.'Victories',              // field id, i.e. the meta key
                        'type' => 'text',                       // text box
                        'std' => ''                    // default value, optional
                    ),
                    array(
                        'name' => __('Year','wp-team-manager'),                  // field name
                        'desc' => __('Year','wp-team-manager'), // field description, optional
                        'id' => $prefix . 'Year',              // field id, i.e. the meta key
                        'type' => 'text',                       // text box
                        'std' => ''                    // default value, optional
                    ),
                  
                    
);




/** 
 * The Class.
 */
class palmaresCustomFeilds {
        
	/**
	 * Hook into the appropriate actions when the class is constructed.
	 */
	public function __construct() {
            
		add_action( 'add_meta_boxes', array( $this, 'add_meta_box' ) );
		add_action( 'save_post', array( $this, 'save' ) );
	}

	/**
	 * Adds the meta box container.
	 */
	public function add_meta_box( $post_type ) {
            $post_types = array('cwebc_palmares');     //limit meta box to certain post types
            if ( in_array( $post_type, $post_types )) {
		add_meta_box(
			'some_meta_box_name'
			,__( 'Details', 'myplugin_textdomain' )
			,array( $this, 'render_meta_box_content' )
			,$post_type
			,'advanced'
			,'high'
		);
            }
	}

	/**
	 * Save the meta when the post is saved.
	 *
	 * @param int $post_id The ID of the post being saved.
	 */
	public function save( $post_id ) {
                global $c_palmares_meta_boxes;
		/*
		 * We need to verify this came from the our screen and with proper authorization,
		 * because save_post can be triggered at other times.
		 */

		// Check if our nonce is set.
		if ( ! isset( $_POST['myplugin_inner_custom_box_nonce'] ) )
			return $post_id;

		$nonce = $_POST['myplugin_inner_custom_box_nonce'];

		// Verify that the nonce is valid.
		if ( ! wp_verify_nonce( $nonce, 'myplugin_inner_custom_box' ) )
			return $post_id;

		// If this is an autosave, our form has not been submitted,
                //     so we don't want to do anything.
		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) 
			return $post_id;

		// Check the user's permissions.
		if ( 'page' == $_POST['post_type'] ) {

			if ( ! current_user_can( 'edit_page', $post_id ) )
				return $post_id;
	
		} else {

			if ( ! current_user_can( 'edit_post', $post_id ) )
				return $post_id;
		}

		/* OK, its safe for us to save the data now. */
                
              
		// Sanitize the user input.
		
                
              
                
                foreach ($c_palmares_meta_boxes['0'] as $field) {
                    update_post_meta( $post_id, $field['id'], $_POST[$field['id']] );
                }
                
             
               
                
                
	}


	/**
	 * Render Meta Box content.
	 *
	 * @param WP_Post $post The post object.
	 */
	public function render_meta_box_content( $post ) {
	
		// Add an nonce field so we can check for it later.
		wp_nonce_field( 'myplugin_inner_custom_box', 'myplugin_inner_custom_box_nonce' );
		
                global $c_palmares_meta_boxes;
             
       
		echo '<table class="form-table">';

		foreach ($c_palmares_meta_boxes['0'] as $field) {
			$meta = get_post_meta($post->ID, $field['id'], true);
			//$meta = !empty($meta) ? $meta : $field['std'];
                        
			echo '<tr>';
			// call separated methods for displaying each type of field
			//call_user_func(array(&$this, 'show_field_' . $field['type']), $field, $meta);
                        echo '<th><label for="myplugin_new_field">';
                            _e( ''.$field['name'].'', 'myplugin_textdomain' );
                        echo '</label></th><td> ';
                      
                        echo '<input type="'.$field['type'].'" id="'.$field['id'].'" name="'.$field['id'].'"';
                        echo ' value="' . esc_attr( $meta ) . '" style="width:95%" />';
                            echo '</td></tr>';                                                      
                      
                        }
                        
               
                    
		echo '</table>';
                
                

                
                              
	}
}



