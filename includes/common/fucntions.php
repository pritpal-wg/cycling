<?php
function cwebco_calender_files(){
    ?>    
    <link href='<?php echo ADV_PLUGIN_WS_PATH1 ?>/public/calender/fullcalendar.css' rel='stylesheet' />
    <link href='<?php echo ADV_PLUGIN_WS_PATH1 ?>/public/calender/fullcalendar.print.css' rel='stylesheet' media='print' />
    <script src='<?php echo ADV_PLUGIN_WS_PATH1 ?>/public/calender/lib/moment.min.js'></script>
    <script src='<?php echo ADV_PLUGIN_WS_PATH1 ?>/public/calender/lib/jquery.min.js'></script>
    <script src='<?php echo ADV_PLUGIN_WS_PATH1 ?>/public/calender/fullcalendar.min.js'></script>
    <?php
}
function responsive_calender_files(){ ?>
    
  <link href='<?php echo ADV_PLUGIN_WS_PATH1 ?>/includes/widgets/css/bootstrap.min.css' rel='stylesheet' media='all' />
    <link href='<?php echo ADV_PLUGIN_WS_PATH1 ?>/includes/widgets/css/responsive-calendar.css' rel='stylesheet' media='all' />
    <script src='<?php echo ADV_PLUGIN_WS_PATH1 ?>/includes/widgets/js/jquery.js'></script>
    <script src='<?php echo ADV_PLUGIN_WS_PATH1 ?>/includes/widgets/js/responsive-calendar.js'></script>
    <script src='<?php echo ADV_PLUGIN_WS_PATH1 ?>/includes/widgets/js/bootstrap.min.js'></script>   
    
    
<?php }

function cwebco_get_posts($post_type, $meta_key='', $meta_value='', $taxonomy='', $term=''){
      //Get Events
$cwebco_array = array(
                                    'posts_per_page'   => 200,
                                    'offset'           => 0,
                                    'category'         => '',
                                    'category_name'    => '',
                                    'orderby'          => 'post_date',
                                    'order'            => 'DESC',
                                    'include'          => '',
                                    'exclude'          => '',
                                    'meta_key'         => $meta_key,
                                    'meta_value'       => $meta_value,
                                    'post_type'        => $post_type,                                                                        
                                    'post_mime_type'   => '',
                                    'post_parent'      => '',
                                    'post_status'      => 'publish',
                                    'suppress_filters' => true );


$cwebco_get_posts = get_posts($cwebco_array);

return $cwebco_get_posts;
}


function cwebco_get_posts_term($post_type, $meta_key='', $meta_value='', $taxonomy='', $feild='', $term='',  $operator=''){
      //Get Events
$cwebco_array = array(
                                    'posts_per_page'   => 200,
                                    'offset'           => 0,
                                    'category'         => '',
                                    'category_name'    => '',
                                    'orderby'          => 'post_date',
                                    'order'            => 'DESC',
                                    'include'          => '',
                                    'exclude'          => '',
                                    'meta_key'         => $meta_key,
                                    'meta_value'       => $meta_value,
                                    'post_type'        => $post_type,                                                                        
                                    'post_mime_type'   => '',
                                    'post_parent'      => '',
                                    'post_status'      => 'publish',
                                    'suppress_filters' => 0,                                                                        
                    );
if($taxonomy != ''):
    $cwebco_array['tax_query']    = array(
                                        array(
                                            'taxonomy' => $taxonomy,
                                            'field' => $feild,
                                            'terms' => $term,
                                            'operator' => $operator,
                                        ),    
                                    );
endif;

$cwebco_get_posts = get_posts($cwebco_array);

return $cwebco_get_posts;
}
function themefusion_breadcrumb() {
		global $smof_data,$post;
		echo '<ul class="breadcrumbs">';

		 if ( !is_front_page() ) {
		echo '<li>'.$smof_data['breacrumb_prefix'].' <a href="';
		echo home_url();
		echo '">'.__('Home', 'Avada');
		echo "</a></li>";
		}

		$params['link_none'] = '';
		$separator = '';

		if (is_category() && !is_singular('avada_portfolio')) {
			$category = get_the_category();
			$ID = $category[0]->cat_ID;
			echo is_wp_error( $cat_parents = get_category_parents($ID, TRUE, '', FALSE ) ) ? '' : '<li>'.$cat_parents.'</li>';
		}

		if(is_singular('avada_portfolio')) {
			echo get_the_term_list($post->ID, 'portfolio_category', '<li>', '&nbsp;/&nbsp;&nbsp;', '</li>');
			echo '<li>'.get_the_title().'</li>';
		}

		if(is_singular('event')) {
			$terms = get_the_term_list($post->ID, 'event-categories', '<li>', '&nbsp;/&nbsp;&nbsp;', '</li>');
			if( ! is_wp_error( $terms ) ) {
				echo get_the_term_list($post->ID, 'event-categories', '<li>', '&nbsp;/&nbsp;&nbsp;', '</li>');
			}
			echo '<li>'.get_the_title().'</li>';
		}

		if (is_tax()) {
			$term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
			$link = get_term_link( $term );
			
			if ( is_wp_error( $link ) ) {
				echo sprintf('<li>%s</li>', $term->name );
			} else {
				echo sprintf('<li><a href="%s" title="%s">%s</a></li>', $link, $term->name, $term->name );
			}
		}

		if(is_home()) { echo '<li>'.$smof_data['blog_title'].'</li>'; }
		if(is_page() && !is_front_page()) {
			$parents = array();
			$parent_id = $post->post_parent;
			while ( $parent_id ) :
				$page = get_page( $parent_id );
				if ( $params["link_none"] )
					$parents[]  = get_the_title( $page->ID );
				else
					$parents[]  = '<li><a href="' . get_permalink( $page->ID ) . '" title="' . get_the_title( $page->ID ) . '">' . get_the_title( $page->ID ) . '</a></li>' . $separator;
				$parent_id  = $page->post_parent;
			endwhile;
			$parents = array_reverse( $parents );
			echo join( '', $parents );
			echo '<li>'.get_the_title().'</li>';
		}
		
		if(is_single() && !is_singular('avada_portfolio')  && ! is_singular('tribe_events') && ! is_singular('event') && ! is_singular('wpfc_sermon')) {
			$categories_1 = get_the_category($post->ID);
			if($categories_1):
				foreach($categories_1 as $cat_1):
					$cat_1_ids[] = $cat_1->term_id;
				endforeach;
				$cat_1_line = implode(',', $cat_1_ids);
			endif;
			if( isset( $cat_1_line ) && $cat_1_line ) {
				$categories = get_categories(array(
					'include' => $cat_1_line,
					'orderby' => 'id'
				));
				if ( $categories ) :
					foreach ( $categories as $cat ) :
						$cats[] = '<li><a href="' . get_category_link( $cat->term_id ) . '" title="' . $cat->name . '">' . $cat->name . '</a></li>';
					endforeach;
					echo join( '', $cats );
				endif;
			}
                        global $post;
                        if ($post->post_type == 'riders') {
                            $teams = wp_get_post_terms($post->ID, 'teams');
                            if(!is_wp_error($teams)){
                                $term = $teams[0];
				echo sprintf('<li><a href="%s" title="%s">%s</a></li>', get_term_link($term), $term->name, $term->name );
                            }
                        }
			echo '<li>'.get_the_title().'</li>';
		}
		if( is_tag() ){ echo '<li>'."Tag: ".single_tag_title('',FALSE).'</li>'; }
		if( is_search() ){ echo '<li>'.__("Search", 'Avada').'</li>'; }
		if( is_year() ){ echo '<li>'.get_the_time('Y').'</li>'; }
		
		if( is_404() ) { 
			if( class_exists( 'TribeEvents' ) && 
				tribe_is_event() || is_events_archive()
			) { 
				echo '<li>'. tribe_get_events_title() .'</li>';
			} else {		
				echo '<li>'.__("404 - Page not Found", 'Avada').'</li>'; 
			}
		}		
		
  		if( class_exists( 'TribeEvents' ) &&
			tribe_is_event() && 
  			! is_404() 
  		) {
  			if( is_singular('tribe_events') ) {
				echo sprintf( '<li><a href="%s" title="%s">%s</a></li>', tribe_get_events_link(), tribe_get_events_title(), tribe_get_events_title() );
  			  			
  				echo '<li>'. get_the_title() .'</li>';
  			} else {  		
	  			echo '<li>'. tribe_get_events_title() .'</li>';
	  		}
		}		

		if( is_archive() && is_post_type_archive() ) {				
			$title = post_type_archive_title( '', false );
			
			$sermon_settings = get_option('wpfc_options');
			if( is_array( $sermon_settings ) ) {
				$title = $sermon_settings['archive_title'];
			}
			echo '<li>'. $title .'</li>';
		}

		echo "</ul>";
}
?>
