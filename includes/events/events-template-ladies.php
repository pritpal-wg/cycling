<?php
    get_header();
    session_start();
    global $post;
    global $smof_data;
    //unset($_SESSION);
    
    //Get Filetr Data
    if(isset($_POST['event_filter']) && isset($_POST['hidenfilter'])){
        $gefilterdata = $_POST['event_filter'];
        $_SESSION['cart_items'] = $gefilterdata ;
        $sessgefilterdata = $_SESSION['cart_items'];
    }elseif(!isset($_POST['event_filter']) && isset($_POST['hidenfilter'])){
        echo $_SESSION['cart_items'] = '';
    }
    
    
    $thispage_args=array('name__like'=>'ladies');
    $terms=get_terms('teams', $thispage_args);
    
    //Get Results
    $get_all_events = array(
                            'posts_per_page'   => 200,
                            'offset'           => 0,
                            'category'         => '',
                            'category_name'    => '',
                            'meta_key'         => 'eventDateofEvent',
                            'orderby'          => 'meta_value',
                            'order'            => 'ASC',
                            'include'          => '',
                            'exclude'          => '',
                            'post_type'        => 'events',
                            'post_mime_type'   => '',
                            'post_parent'      => '',
                            'post_status'      => 'publish',
                            'suppress_filters' => 0,
                            'meta_query' => array(
                                                  array(
                                                        'key' => 'key_event_team_name',
                                                        'value' => $terms['0']->term_id
                                                        )),
                            );
    
    
    /* =====Get Events Without Birthday===== Start*/
    $getevent_nobirthday = cwebco_get_posts_term('events', '', '', 'suddo_event_type', 'slug', 'birthday', 'NOT IN');
    foreach ($getevent_nobirthday as $genb){
        $get_post_m = get_post_meta($genb->ID, 'eventDateofEvent', true);
        $get_event_date[] = date('Y', strtotime($get_post_m));
    }
    /* =====Get Events Without Birthday===== Ends*/
    
    if($_SESSION['cart_items'] != ''){
        $get_all_events['tax_query']        =
        array(
              array(
                    'taxonomy' => 'suddo_event_type',
                    'field' => 'term_taxonomy_id',
                    'terms' => $_SESSION['cart_items']
                    ));
    }
    
    
    
    
    //print_r($get_all_events);exit;
    
    
    $months = array(
                    1 => 'January',
                    2 => 'February',
                    3 => 'March',
                    4 => 'April',
                    5 => 'May',
                    6 => 'June',
                    7 => 'July',
                    8 => 'August',
                    9 => 'September',
                    10 => 'October',
                    11 => 'November',
                    12 => 'December');
    
    
    if($_GET['month'] == ''){
        $getmonths = date("m", strtotime("now"));
        $getyears = date("Y", strtotime("now"));
    }elseif(isset($_GET['month']) && isset ($_GET['get_year'])){
        $getmonths = $_GET['month'];
        $getyears  = $_GET['get_year'];
    }else{
        $getmonths = $_GET['month'];
        $getyears = date("Y", strtotime("now"));
        
    }
    
    $prevmonth =  $getmonths - 1;
    $nextmonth =  $getmonths + 1;
    
    
    
    ?>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>


<script type="text/javascript">
$(document).ready(function () {
                  $(".checkBoxClass").click(function () {
                                            this.form.submit()
                                            });
                  });
</script>
<style type="text/css">
.table-1 {padding-bottom: 10px }
.table-1 tr td{
    padding-top: 0;
    border-color: #fff !important;
    padding-bottom: 0;
}
</style>
<div id="content" style="float: left; margin:0 !important">
<div class="fusion-one-third one_third fusion-layout-column fusion-column spacing-yes">
<ul class="fusion-checklist">


<?php
    $getcurrentyears = date("Y", strtotime("now"));
    
    if(isset($_GET['get_year']) && $_GET['get_year'] != $getcurrentyears){
        $current_url1 = $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
        
        if (strpos($current_url1, '?get_year='.$_GET['get_year'].'&month='.$_GET['month']) !== false ) {
            //echo 'adasd';
            
            $current_url1 = str_replace('?get_year='.$_GET['get_year'].'&month='.$_GET['month'], '', $current_url1);
            
        }
        
        if (strpos($current_url1, '&get_year='.$_GET['get_year'].'&month='.$_GET['month']) !== false ) {
            //echo 'adasd';
            
            $current_url1 = str_replace('&get_year='.$_GET['get_year'].'&month='.$_GET['month'], '', $current_url1);
            
        }
        
        if (strpos($current_url1, '&get_year=') !== false ) {
            //echo 'adasd';
            
            $current_url1 = str_replace('&get_year='.$_GET['get_year'], '', $current_url1);
            
        }
        elseif (strpos($current_url1, '?get_year=') !== false) {
            //echo 'adasd';
            
            $current_url1 = str_replace('?get_year='.$_GET['get_year'], '', $current_url1);
            
        }
        $get_year = $_GET['get_year']+1;
        for ($dsst= $getcurrentyears;$dsst>=$get_year;$dsst--){
            if(in_array($dsst, $get_event_date)){
                ?>
<li class="fusion-li-item size-small" style="background: #EEEEEE; padding: 5px 2px 5px 12px;">
<span class="icon-wrapper circle-yes" style="visibility: hidden; display:none;"><i class="fusion-li-icon fa fa-star-o" style="color:#ffffff;"></i></span>
<a href="//<?php echo $current_url1; ?><?php if (strpos($current_url1, '?') !== false) {
echo '&';
}else{
    echo '?';
} ?>get_year=<?php echo $dsst; ?>&month=<?php echo date('m'); ?>" style="font-weight: bold;"><?php echo $dsst ;?></a>
<b style="float: right; font-size: 18px; color: #747474;font-weight: normal;">&GT;</b></li>
<?php
    }
    }
    }
    ?>
<li class="fusion-li-item size-small" style="background: #EEEEEE; padding: 3px;">
<span  class="icon-wrapper circle-yes" style="visibility: hidden; display:none;"><i class="fusion-li-icon fa fa-star-o" style="color:#ffffff;"></i></span>

<b style="font-weight: bold; padding-left: 9px;font-size: 13px;color: #333333;">
<?php
    if(isset($_GET['get_year'])){
        echo $getyear =  $_GET['get_year'];
    }else{
        echo $getyear = date("Y", strtotime("now"));
    }
    ?>
</b>
<ul style="padding: 2px 5px 5px 10px;">

<?php
    $current_url = $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
    
    if (strpos($current_url, '?month='.$_GET['month'].'&get_year='.$_GET['get_year']) !== false) {
        //echo 'adasd';
        
        $current_url = str_replace('?month='.$_GET['month'].'&get_year='.$_GET['get_year'], '', $current_url);
        
    }elseif (strpos($current_url, '?month=') !== false) {
        //echo 'adasd';
        
        $current_url = str_replace('?month='.$_GET['month'], '', $current_url);
        
    }elseif (strpos($current_url, '&month=') !== false) {
        //echo 'adasd';
        
        $current_url = str_replace('&month='.$_GET['month'], '', $current_url);
        
    }
    
    
    
    for ($i = 1; $i <= 12; $i++)
    {
        echo '<li style="list-style-type: none;"><a';
        if($getmonths == $i){echo ' style="font-weight:bold; color:'.$smof_data['primary_color'].' "';}
        //echo ' href="'.get_permalink( $post->ID );
        echo ' href="//'.$current_url;
        if (strpos($current_url, '?') !== false) {
            echo '&month=';
        }else{
            echo '?month=';
        }
        echo $i.'"> <b style="font-size: 18px;color: #747474;font-weight: normal;">&GT;</b>  '. __($months[$i]).' </a></li>';
    }
    ?>

</ul>
</li>
<?php
    
    $current_url2 = $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
    
    if (strpos($current_url2, '?month='.$_GET['month'].'&get_year='.$_GET['get_year']) !== false) {
        //echo 'adasd';
        
        $current_url2 = str_replace('?month='.$_GET['month'].'&get_year='.$_GET['get_year'], '?', $current_url2);
        
    }
    if (strpos($current_url2, '&month='.$_GET['month'].'&get_year='.$_GET['get_year']) !== false) {
        //echo 'adasd';
        
        $current_url2 = str_replace('&month='.$_GET['month'].'&get_year='.$_GET['get_year'], '&', $current_url2);
        
    }
    
    if (strpos($current_url2, '?get_year='.$_GET['get_year'].'&month='.$_GET['month']) !== false ) {
        //echo 'adasd';
        
        $current_url2 = str_replace('?get_year='.$_GET['get_year'].'&month='.$_GET['month'], '', $current_url2);
        
    }
    
    
    
    if (strpos($current_url2, '&get_year=') !== false ) {
        //echo 'adasd';
        
        $current_url2 = str_replace('&get_year='.$_GET['get_year'], '', $current_url2);
        
    }
    elseif (strpos($current_url2, '?get_year=') !== false) {
        //echo 'adasd';
        
        $current_url2 = str_replace('?get_year='.$_GET['get_year'], '', $current_url2);
        
    }
    
    if (strpos($current_url2, '?month=') !== false ) {
        //echo 'adasd';
        
        $current_url2 = str_replace('?month='.$_GET['month'], '', $current_url2);
        
    }
    if (strpos($current_url2, '&get_year='.$_GET['get_year'].'&month='.$_GET['month']) !== false ) {
        //echo 'adasd';
        
        $current_url2 = str_replace('&get_year='.$_GET['get_year'].'&month='.$_GET['month'], '', $current_url2);
        
    }
    
    ?>

<?php
    if($_GET['get_year']){
        for ($dt=1;$dt<=5;$dt++){  // dt Loop Start
            $newEndingDate =  $_GET['get_year'] - $dt;
            if(in_array($newEndingDate, $get_event_date)){ // Check Array Start
                ?>
<li class="fusion-li-item size-small" style="background: #EEEEEE; padding: 5px 2px 5px 12px;">
<span  class="icon-wrapper circle-yes" style="visibility: hidden; display:none;"><i class="fusion-li-icon fa fa-star-o" style="color:#ffffff;"></i></span>
<?php //$newEndingDate = date("Y", strtotime(date("Y", strtotime($_GET['get_year'])) . " $dt years")); ?>
<a href="//<?php echo $current_url2; ?><?php if (strpos($current_url2, '?') !== false) {
echo '&';
}else{
    echo '?';
} ?>get_year=<?php echo $newEndingDate; ?>&month=<?php echo date('m'); ?>" style="font-weight: bold;"><?php echo $newEndingDate ;?></a>
<b style="float: right; font-size: 18px; color: #747474;font-weight: normal;">&GT;</b>&nbsp;&nbsp;</li>
<?php
    } // Check Array Ends
    } // dt Loop Ends
    }else{
        
        for ($dt=-1;$dt>=-5;$dt--){    // dt Loop Start
            $getstrttimeate = date('Y', strtotime('+'.$dt.' years'));
            if(in_array($getstrttimeate, $get_event_date)){ // Check Array Start
                ?>
<li class="fusion-li-item size-small" style="background: #EEEEEE; padding: 5px 2px 5px 12px;">
<span class="icon-wrapper circle-yes" style="visibility: hidden; display:none;"><i class="fusion-li-icon fa fa-star-o" style="color:#ffffff;"></i></span>
<?php $newEndingDate = date("Y", strtotime(date("Y", strtotime($_GET['get_year'])) . " + $dt years")); ?>
<a href="//<?php echo $current_url2; ?><?php if (strpos($current_url2, '?') !== false) {
echo '&';
}else{
    echo '?';
} ?>get_year=<?php echo $getstrttimeate; ?>&month=<?php echo date('m'); ?>" style="font-weight: bold;"><?php echo $getstrttimeate ;?></a>
<b style="float: right; font-size: 18px;color: #747474;font-weight: normal;">&GT;&nbsp;&nbsp;</b></li>
<?php
    } // Check Array Ends
    } // dt Loop Ends
    } ?>
</ul>
</div>
<div class="fusion-two-third two_third fusion-layout-column fusion-column last spacing-yes">
<h3 style="margin-bottom: 10px; margin-left:0px; font-weight: normal;"><?php echo __('Select calendar & event type');?>:</h3>
<form method="POST" id="filterform">
<input type="hidden" value="1" name="hidenfilter" />
<?php
    $terms = get_terms( 'suddo_event_type' );
    // print_r($terms);
    
    if ( ! empty( $terms ) && ! is_wp_error( $terms ) ){
        echo '<ul class="fusion-checklist" style="margin:0;background: #EEEEEE;display: block;float: left;width: 100%;padding: 5px 0px 5px 0px;">';
        foreach ( $terms as $term ) {
            echo '<li class="fusion-li-item size-small" style="float:left;margin-left:2%; margin-bottom: 0px;">';
            echo '<input type="checkbox" id="checkbox" class="checkBoxClass" name="event_filter[]" value="'.$term->term_id.'"';
            if(in_array($term->term_id, $_SESSION['cart_items'])){echo 'checked=checked';};
            echo '/>'; echo _e($term->name);
            echo '</li>';
            
        }
        echo '</ul>';
    }
    ?>
</form>
<div class="clear"></div>
<br/>

<hr/>
<?php $get_current_date = date("d", strtotime("now"));
    //$get_current_dates = date("m/d/Y", strtotime("now"));
    ?>
<div class="table-1">


<table style="width: 100%">
<?php
    //print_r($get_all_events);exit;
    $fetch_get_all_events = get_posts($get_all_events);
    
    //echo '<pre>';
    //print_r($fetch_get_all_events);
    
    //echo '<pre>';
    //print_r($fetch_get_all_events); exit;
    foreach($fetch_get_all_events as  $fetch_get_all_event){
        //print_r(get_post_meta ($fetch_get_all_event->ID));
        $get_eventDateofEvent = get_post_meta($fetch_get_all_event->ID, 'eventDateofEvent', true);
        $get_eventCountryofEvent = get_post_meta($fetch_get_all_event->ID, 'eventCountry', true);
        $getmonthforfilter = date("m", strtotime($get_eventDateofEvent));
        $getyearforfilter = date("Y", strtotime($get_eventDateofEvent));
        $getdateforfilter = date("d", strtotime($get_eventDateofEvent)).',';
        if($getmonthforfilter == $getmonths && $getyears == $getyearforfilter ){
            if(strtotime(date('m/d/Y')) <= strtotime($get_eventDateofEvent)){
                echo '<tr>';
                echo '<td width="15%">';
                echo $get_current_dates;
                echo $getmdYforfilter;
                
                echo '<span style="color:#000">' .__(date("d/m/Y", strtotime($get_eventDateofEvent))).'</span>';
                
                echo '</td>';
                
                $get_terms_name11 = get_the_terms( $fetch_get_all_event->ID, 'suddo_event_type' );
                foreach ( $get_terms_name11 as $get_terms_names11 ) {
                    $get_terms_names11->name;
                }
                
                // Check if event type is birthday
                if($get_terms_names11->slug == 'birthday-nl' || $get_terms_names11->slug == 'birthday-fr' || $get_terms_names11->slug == 'birthday-en'){
                    $get_riderskey = get_post_meta($fetch_get_all_event->ID, 'riderskey', true);
                    $un_get_riderskey = unserialize($get_riderskey);
                    $getposttypes = get_post_type( $un_get_riderskey[0]);
                    if($getposttypes == 'riders'){
                        $icl_id_of_post = icl_object_id($un_get_riderskey[0], 'riders', FALSE, ICL_LANGUAGE_CODE);
                    }elseif($getposttypes == 'suddo_staff'){
                        $icl_id_of_post = icl_object_id($un_get_riderskey[0], 'suddo_staff', FALSE, ICL_LANGUAGE_CODE);
                    }
                    $get_eventCountryofEvent=get_post_meta($icl_id_of_post, 'rdNationality', true);
                }else{ // Else send default id
                    $icl_id_of_post = $fetch_get_all_event->ID;
                }
                
                //$get_eventCountryofEvent1=get_post_meta($icl_id_of_post, 'rdNationality', true);
                
                echo '<td width="20%">';
                
    ?>
<img src="http://upload.wikimedia.org/wikipedia/commons/d/d5/Blank_-_Spacer.png" class="flag flag-<?php echo strtolower($get_eventCountryofEvent); ?>" alt="<?php echo $countries[$get_eventCountryofEvent];  ?>" title="<?php echo $countries[$get_eventCountryofEvent];  ?>" style="margin-top: 5px;margin-left: 1%;" />

<?php
    echo $get_terms_names11->name;
    echo '</td>';
    ?>








<?php
    //$get_terms_name = get_the_terms( $fetch_get_all_event->ID, 'suddo_event_type' );
    
    
    echo '<td><a href="'.get_permalink($icl_id_of_post).'">'.__($fetch_get_all_event->post_title) .'</a></td>';
    
    echo '</tr>';
    }   }
    
    }
    
    
    $fet_passed_events = get_posts($get_all_events);
    foreach($fet_passed_events as  $fetch_get_passed_event){
        
        $get_eventDateofEvent1 = get_post_meta($fetch_get_passed_event->ID, 'eventDateofEvent', true);
        //$d = date_parse_from_format("m-d-Y", $get_eventDateofEvent1);
        //$getmonthforfilter = $d["month"];
        $get_eventCountryofEvent1 = get_post_meta($fetch_get_passed_event->ID, 'eventCountry', true);
        
        $getmonthforfilter1 = date("m", strtotime($get_eventDateofEvent1));
        $getyearforfilter1 = date("Y", strtotime($get_eventDateofEvent1));
        $getdateforfilter1 = date("d", strtotime($get_eventDateofEvent1)).',';
        
        // $getmdYforfilter = date("m/d/Y", strtotime($get_eventDateofEvent1));
        
        if($getmonthforfilter1 == $getmonths && $getyears == $getyearforfilter1 ){
            if(strtotime(date('m/d/Y')) > strtotime($get_eventDateofEvent1)){
                echo '<tr>';
                echo '<td width="15%">';
                echo $get_current_dates;
                echo $getmdYforfilter;
                
                
                
                
                echo '<span style="text-decoration: line-through;">' .__(date("d/m/Y", strtotime($get_eventDateofEvent1))) .'</span>';
                
                
                echo '</td>';
                echo '<td width="20%">';
                ?>



<?php  //echo $get_eventCountryofEvent1;    ?>


<?php
    $get_terms_name1 = get_the_terms( $fetch_get_passed_event->ID, 'suddo_event_type' );
    //print_r($get_terms_name1);
    foreach ( $get_terms_name1 as $get_terms_names1 ) {
        $get_terms_names1->name;
        //print_r($get_terms_names);
    }
    // Check if event type is birthday
    if($get_terms_names1->slug == 'birthday-nl' || $get_terms_names1->slug == 'birthday-fr' || $get_terms_names1->slug == 'birthday-en'){
        $get_riderskey = get_post_meta($fetch_get_passed_event->ID, 'riderskey', true);
        $un_get_riderskey = unserialize($get_riderskey);
        $getposttypes = get_post_type( $un_get_riderskey[0]);
        if($getposttypes == 'riders'){
            $icl_id_of_post = icl_object_id($un_get_riderskey[0], 'riders', FALSE, ICL_LANGUAGE_CODE);
        }elseif($getposttypes == 'suddo_staff'){
            $icl_id_of_post = icl_object_id($un_get_riderskey[0], 'suddo_staff', FALSE, ICL_LANGUAGE_CODE);
        }
        
        $get_eventCountryofEvent1=get_post_meta($icl_id_of_post, 'rdNationality', true);
        
    }else{ // Else send default id
        $icl_id_of_post = $fetch_get_passed_event->ID;
    }
				
    ?>
<img src="http://upload.wikimedia.org/wikipedia/commons/d/d5/Blank_-_Spacer.png" class="flag flag-<?php echo strtolower($get_eventCountryofEvent1); ?>" alt="<?php echo $countries[$get_eventCountryofEvent1];  ?>" title="<?php echo $countries[$get_eventCountryofEvent1];  ?>" style="margin-top: 5px;margin-left: 1%;" />
<?php
    echo $get_terms_names1->name;
    //print_r(get_post_meta($icl_id_of_post));
    echo '</td>';
    echo '<td><a href="'.get_permalink($icl_id_of_post).'">'.__($fetch_get_passed_event->post_title) .'</a></td>';
    
    echo '</tr>';
    }
    }
    }
    ?>
</table>
</div>
<hr/>
<div>
<?php
    $get_prev_month = $current_url;
    if (strpos($current_url, '?') !== false) {
        $get_prev_month .= '&month=';
    }else{
        $get_prev_month .= '?month=';
    }
    $get_prev_month .= $prevmonth;
    
    
    $get_next_month = $current_url;
    if (strpos($current_url, '?') !== false) {
        $get_next_month .= '&month=';
    }else{
        $get_next_month .= '?month=';
    }
    $get_next_month .= $nextmonth;
    
    ?>
<?php if($months[$prevmonth] != ''): ?>
<a href="//<?php echo $get_prev_month; ?>" style="font-weight: normal; color:#ee2e24;"> &#60; <?php echo __($months[$prevmonth]); ?></a>
<?php endif; ?>
<?php if($months[$nextmonth] != ''): ?>
<a href="//<?php echo $get_next_month; ?>" style="float: right;font-weight: normal; color:#ee2e24;"> <?php echo __($months[$nextmonth]); ?> &#62; </a>
<?php endif; ?>
</div>
</div>
</div>

<div id="sidebar" class="sidebar">
<?php
    dynamic_sidebar('Calender Events Sidebar');
    
    ?>
</div>

<?php get_footer(); ?>
