<?php

/**
 * The file that defines the rider
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the dashboard.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Plugin_Name
 * @subpackage Plugin_Name/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, dashboard-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    Plugin_Name
 * @subpackage Plugin_Name/includes
 * @author     Your Name <email@example.com>
 */
/**
 * Calls the class on the post edit screen.
 */
get_header();
global $post;
global $smof_data;


/* =====Get Events Without Birthday===== Start*/   
$getevent_nobirthday = cwebco_get_posts_term('events', '', '', 'suddo_event_type', 'slug', 'birthday', 'NOT IN');
foreach ($getevent_nobirthday as $genb){                      
    $get_post_m = get_post_meta($genb->ID, 'eventDateofEvent', true); 
    $get_event_date[] = date('Y', strtotime($get_post_m));
}
/* =====Get Events Without Birthday===== Ends*/


$cweb_gallery_arguments_temp = array(
                                    'posts_per_page'   => 200,
                                    'offset'           => 0,
                                    'category'         => '',
                                    'category_name'    => '',
                                    'orderby'          => 'post_date',
                                    'order'            => 'DESC',
                                    'include'          => '',
                                    'exclude'          => '',
                                    'meta_key'         => 'key_gallery_events_name',
                                    'meta_value'       => $post->ID,
                                    'post_type'        => 'suddo_gallery',
                                    'post_mime_type'   => '',
                                    'post_parent'      => '',
                                    'post_status'      => 'publish',
                                    'suppress_filters' => 0 );



//Get Results
$get_results_for_eventn = array(
                                    'posts_per_page'   => 200,
                                    'offset'           => 0,
                                    'category'         => '',
                                    'category_name'    => '',
                                    'orderby'          => 'post_date',
                                    'order'            => 'DESC',
                                    'include'          => '',
                                    'exclude'          => '',
                                    'meta_key'         => 'key_events_result_name',
                                    'meta_value'       => $post->ID,
                                    'post_type'        => 'event_results',
                                    'post_mime_type'   => '',
                                    'post_parent'      => '',
                                    'post_status'      => 'publish',
                                    'suppress_filters' => 0 );

$months = array(
                1 => 'January',
                2 => 'February',
                3 => 'March',
                4 => 'April',
                5 => 'May',
                6 => 'June',
                7 => 'July',
                8 => 'August',
                9 => 'September',
                10 => 'October',
                11 => 'November',
                12 => 'December');

$getpostmonth = get_post_meta( $post->ID, 'eventDateofEvent', true );


$getmonths = date("m", strtotime($getpostmonth));



$cal_pages_men = get_pages(array(
    'meta_key' => '_wp_page_template',
    'meta_value' => 'events-template-mens.php'
));

$cal_pages_women = get_pages(array(
    'meta_key' => '_wp_page_template',
    'meta_value' => 'events-template-ladies.php'
));

$cal_pages_u23 = get_pages(array(
    'meta_key' => '_wp_page_template',
    'meta_value' => 'events-template-u23.php'
));
$get_cal_page_men=$cal_pages_men['0']->ID;  // 2930
$get_cal_page_women=$cal_pages_women['0']->ID;   // 2940
$get_cal_page_u23=$cal_pages_u23['0']->ID;   //2946

/* assign static values for live site */
$get_cal_page_men = '2930';
$get_cal_page_women = '2940';
$get_cal_page_u23 = '2946';
    
    
$team_name= get_post_meta($post->ID, 'key_event_team_name', true);
$team_object_for_left=get_term_by('id', $team_name, 'teams');
$team_object_type=$team_object_for_left->slug;

if(strpos($team_object_type, 'men')!==false){
	 $current_url = get_permalink($get_cal_page_men);
}elseif(strpos($team_object_type, 'ladi')!==false){
	$current_url = get_permalink($get_cal_page_women);
}
else{
	$current_url = get_permalink($get_cal_page_u23);
}

$current_url1=$current_url;
$year =get_post_meta( $post->ID, 'eventDateofEvent', true );
$getyear = date("Y", strtotime($year));
?>

<style type="text/css">
    .rider-black-bar{position: absolute; bottom: 0; background: #000; color: #fff; width: 100%; padding: 2% 5%;opacity: 0.8;}
    
    .rider-main-div:hover > .rider-black-bar{background:<?php echo $smof_data['primary_color']; ?>}
    #wpsimplegallery li img{ margin-left: 0}
</style>

<!-- Google Map API Scripst -->
<script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
<script type="text/javascript" src='http://maps.google.com/maps/api/js?sensor=false&libraries=places'></script>
<script type="text/javascript" src='<?php echo ADV_PLUGIN_WS_PATH1.'/public/js/dist/locationpicker.jquery.min.js' ?>'></script>


<div id="content" style="float: left;width:71% !important; margin:0 !important">
   
<div class="fusion-one-third one_third fusion-layout-column fusion-column spacing-yes">
        <ul class="fusion-checklist">
             <li class="fusion-li-item size-small" style="background: #F6F6F6;">
            <h3  style="font-size: 20px; padding: 9px;"><?php echo __("Calendar");?></h3>
             </li>
            <?php 
            $getcurrentyears = date("Y", strtotime("now"));
            //echo $getyear;
            
               if(isset($_GET['get_year']) && $_GET['get_year'] != $getcurrentyears){ 
                   
              //  $current_url1 = get_permalink( $get_cal_page );
                            
                            
                            if (strpos($current_url1, '&get_year=') !== false ) {
                                //echo 'adasd';
                                
                                $current_url1 = str_replace('&get_year='.$_GET['get_year'], '', $current_url1);
                                
                            }    
                            elseif (strpos($current_url1, '?get_year=') !== false) {
                                //echo 'adasd';
                                
                                $current_url1 = str_replace('?get_year='.$_GET['get_year'], '', $current_url1);
                                
                            } 
            ?>
                   
            <li class="fusion-li-item size-small" style="background: #F6F6F6; padding: 5px 2px 5px 8px;">
                <span  class="icon-wrapper circle-yes" style="display:none;"><i class="fusion-li-icon fa fa-star-o" style="color:#ffffff;"></i></span>
                <?php 
                    $getstrttimeate = date('Y', strtotime('+'.$dt.' years'));  
                    if(in_array($getstrttimeate, $get_event_date)){ 
                ?>
                    <a href="//<?php echo $current_url1; ?><?php if (strpos($current_url, '?') !== false) {
                                        echo '&';                                
                                    }else{
                                        echo '?';
                                    } ?>get_year=<?php echo date("Y", strtotime("now")); ?>"><?php echo date("Y", strtotime("now")); ;?></a>
            </li>
               <?php } 
               
            } ?>
            <li class="fusion-li-item size-small" style="background: #F6F6F6;">
            <!--<h3  style="font-size: 20px; padding: 9px;"><?php //echo __("Calendar");?></h3>-->
                <span  class="icon-wrapper circle-yes" style="display:none;"><i class="fusion-li-icon fa fa-star-o" style="color:#ffffff;"></i></span>
               
                <b style="font-weight: bold; padding-left: 9px;font-size: 13px;color: #333333;">
                <?php 
                    if(isset($_GET['get_year'])){
                        echo $getyear = $_GET['get_year'];
                    }else{
                        //echo date("Y", strtotime("now"));
                        echo $getyear = date("Y", strtotime($year));
                    }
                ?>
        </b>
                <ul style="padding: 2px 5px 5px 14px;">
                    
                        <?php
                         // $current_url = get_permalink( $get_cal_page );
                            

                           // echo $post->ID;

                       // $terms = wp_get_post_terms( $post->ID, 'suddo_event_type' ); 
        	   /*    $team_name= get_post_meta($post->ID, 'key_event_team_name', true);
			$team_object_for_left=get_term_by('id', $team_name, 'teams');
			$team_object_type=$team_object_for_left->slug;

			if(strpos($team_object_type, 'men')!==false){
				 $current_url = get_permalink($get_cal_page_men);
			}elseif(strpos($team_object_type, 'ladi')!==false){
				$current_url = get_permalink($get_cal_page_women);
			}
			else{
				$current_url = get_permalink($get_cal_page_u23);
			}
	
*/

                            if (strpos($current_url, '&month=') !== false || strpos($current_url, '?month=') !== false) {
                                //echo 'adasd';
                                
                                $current_url = str_replace('&month='.$_GET['month'], '', $current_url);
                                
                            }
                            
                           
                        
                            for ($i = 1; $i <= 12; $i++)
                            {
                                   echo '<li style="list-style-type: none;"><a';
                                   if($getmonths == $i){echo ' style="font-weight:bold; color:'.$smof_data['primary_color'].' "';}
                                   //echo ' href="'.get_permalink( $post->ID );
                                   echo ' href="'.$current_url;
                                   if (strpos($current_url, '?') !== false) {
                                        echo '&get_year=';                                
                                    }else{
                                        echo '?get_year=';
                                    } 
                                   echo $getyear.'&month='.$i.'"> <b style="font-size: 18px;color: #747474;float: left;text-align: left;margin-left: -3px;font-weight: normal;margin-right: 3px;">&GT;</b> '. $months[$i].'</a></li>';
                            }
                        ?>
                    
                </ul>
            </li>
             <?php 
               
             //  $current_url2 = get_permalink( $get_cal_page );
                 $current_url2=$current_url;
              
                           if(isset($_GET['month'])){ 
                           if (strpos($current_url2, '?month='.$_GET['month'].'&get_year='.$_GET['get_year']) !== false) {
                                //echo 'adasd';
                                
                                $current_url2 = str_replace('?month='.$_GET['month'].'&get_year='.$_GET['get_year'], '?', $current_url2);
                                
                            } 
                           }
                            if (strpos($current_url2, '&get_year=') !== false ) {
                                //echo 'adasd';
                                
                                $current_url2 = str_replace('&get_year='.$_GET['get_year'], '', $current_url2);
                                
                            } 
                            elseif (strpos($current_url2, '?get_year=') !== false) {
                                //echo 'adasd';
                                
                                $current_url2 = str_replace('?get_year='.$_GET['get_year'].'&month='.$_GET['month'], '?', $current_url2);
                                
                            } 
                            if (strpos($current_url2, '&get_year='.$_GET['get_year'].'&month='.$_GET['month']) !== false ) {
                                //echo 'adasd';
                                
                                $current_url2 = str_replace('&get_year='.$_GET['get_year'].'&month='.$_GET['month'], '', $current_url2);
                                
                            }
                    
                        
                        ?>
             
                <?php if(isset($_GET['get_year'])){
                    for ($dt=2;$dt<=5;$dt++){
                        
                        ?>
                    <li class="fusion-li-item size-small" style="background: #F6F6F6; padding: 5px 2px 5px 12px;">
                    <span  class="icon-wrapper circle-yes" style="display:none;"><i class="fusion-li-icon fa fa-star-o" style="color:#ffffff;"></i></span>
                   <?php $newEndingDate = date("Y", strtotime(date("Y", strtotime($_GET['get_year'])) . " + $dt years")); 
                   if(in_array($newEndingDate, $get_event_date)){ // Check Array Start
                   ?>
                   <a href="<?php echo $current_url2; ?><?php if (strpos($current_url2, '?') !== false) {
                                        echo '&';                                
                                    }else{
                                        echo '?';
                                    } ?>get_year=<?php echo $newEndingDate; ?>"><?php echo $newEndingDate ;?></a>
                   </li>
                   <?php
                    }}
                }else{
                    for ($dt=-1;$dt>=-5;$dt--){
                       
                  
                    $getstrttimeate = date("Y", strtotime($dt." years", strtotime($year)));
                    if(in_array($getstrttimeate, $get_event_date)){ // Check Array Start
                    
                    ?>
                    <li class="fusion-li-item size-small" style="background: #F6F6F6; padding: 5px 2px 5px 12px;">
                    <span class="icon-wrapper circle-yes" style="display:none;"><i class="fusion-li-icon fa fa-star-o" style="color:#ffffff;"></i></span>
                   <?php //$newEndingDate = date("Y", strtotime(date("Y", strtotime($_GET['get_year'])) . " + $dt years")); ?>
                   <a href="<?php echo $current_url2; ?><?php if (strpos($current_url2, '?') !== false) {
                                        echo '&';                                
                                    }else{
                                        echo '?';
                                    } ?>get_year=<?php echo $getstrttimeate; ?>&month=<?php echo date('m'); ?>" style="font-weight: bold;"><?php echo $getstrttimeate ;?></a>
                   </li>
                <?php } } } ?>    
             
            <?php
                    
                ?>
        </ul>
    </div>

   
    
    <div class="fusion-two-third two_third fusion-layout-column fusion-column last spacing-yes">
        <div class="fusion-title title"></div>
        <div><?php echo date("d/m/Y", strtotime(get_post_meta( $post->ID, 'eventDateofEvent', true )));  ?></div>
        <div><?php if(get_post_meta( $post->ID, 'eventCity', true )!=''): echo get_post_meta( $post->ID, 'eventCity', true ).', '; endif;?> 
	     <?php if(get_post_meta( $post->ID, 'eventState', true )!=''): echo get_post_meta( $post->ID, 'eventState', true ).','; endif;?>
	     <?php $geteventcountid =  get_post_meta( $post->ID, 'eventCountry', true ); echo $countries[$geteventcountid];  ?><img src="http://upload.wikimedia.org/wikipedia/commons/d/d5/Blank_-_Spacer.png" class="flag flag-<?php echo strtolower($geteventcountid); ?>" alt="<?php echo $countries[$geteventcountid];  ?>" title="<?php echo $countries[$geteventcountid];  ?>" style="margin-top: 5px;margin-left: 1%;" /></div>
        <div style="margin-top: 10px;"><b><?php if(get_post_meta( $post->ID, 'eventDescription', true )!=''): 
							echo __('Abstract');?>: </b>
							<?php echo get_post_meta( $post->ID, 'eventDescription', true );  
						endif;
					   ?></div>
     <?php $text = get_post_meta( $post->ID, 'eventWebsite', true ); 
     
     if($text ==''){
         
     }
     else{ ?>
         <div><?php echo __('Website');?>:  
         <?php
$text = get_post_meta( $post->ID, 'eventWebsite', true );
//echo $text;
 ///exit;
$word = "http://";

$pos = strpos($text, $word);

if ($pos === false) {
   
    
    ?>
            
            <a style="color: <?php echo $smof_data['primary_color']; ?>" href="http://<?php echo get_post_meta( $post->ID, 'eventWebsite', true );  ?>" target="_blank">http://<?php echo get_post_meta( $post->ID, 'eventWebsite', true );  ?></a>
<?php } else { 
    
   
    ?>
            
 <a style="color: <?php echo $smof_data['primary_color']; ?>" href="<?php echo get_post_meta( $post->ID, 'eventWebsite', true );  ?>" target="_blank"><?php echo get_post_meta( $post->ID, 'eventWebsite', true );  ?></a>
<?php }
?>   
            
        
        </div>
   <?php  }
     ?>
        
        <?php

  $getriderskey = get_post_meta( $post->ID, 'riderskey', true );  
                $unriderskey = unserialize($getriderskey);
		$unriderskey_test=$unriderskey;
		$count=0;
                if($unriderskey_test != ''):
                    foreach($unriderskey_test as $k){
                        if ( FALSE === get_post_status($k)) {
                        }
                        else{
                            ++$count;
                        }
                    }
                endif;
		//echo $count;

		?>
        
        <div><br/><br/><h3><?php if($count): echo __('Riders'); endif; ?> </h3></div>
        <div>            
            <?php 
            
            if($unriderskey != ''):
                $r=1;
                foreach ($unriderskey as $unriderskeys){  
                    
                            
                if ( 'publish' == get_post_status( $unriderskeys ) ) {
                    // The post does not exist                    
               
                $simage = wp_get_attachment_image_src( get_post_thumbnail_id( $unriderskeys ), 'rider-event-thumb' ); 
                    if($r % 3  ){ 
              
            ?>
            <div class="fusion-one-third one_third fusion-layout-column fusion-column spacing-yes" style="overflow: hidden">
                        <div style="position: relative" class="rider-main-div">
                            <a href="<?php echo  get_permalink($unriderskeys); ?>" style="color: #fff"> 
                                <img class="person-img img-responsive" src="<?php echo $simage[0]; ?>" alt="John Doe" style="min-height: 200px;" alt="<?php the_title(); ?>" />                                                                                        
                            </a>
                            <div class="rider-black-bar">
                                        <a href="<?php echo  get_permalink($unriderskeys); ?>" style="color: #fff">                                        
                                            <span class="person-name" style="color: #fff; width: 100%; display: block"><?php echo get_post_meta( $unriderskeys, 'rdLastName', true );  ?></span>
                                            <span class="person-title" style="color: #fff; width: 100%; display: block"><?php echo get_post_meta( $unriderskeys, 'rdFirstName', true );  ?></span>                                        
                                        </a>
                            </div>
                        </div>
                    </div>
            <?php   }  else { ?>
                    <div class="fusion-one-third one_third fusion-layout-column fusion-column spacing-yes last">
                        <div style="position: relative" class="rider-main-div">
                            <a href="<?php echo  get_permalink($unriderskeys); ?>" style="color: #fff"> 
                                <img class="person-img img-responsive" src="<?php echo $simage[0]; ?>" alt="John Doe" style="min-height: 200px;" alt="<?php the_title(); ?>" />                                                                                        
                            </a>
                            <div class="rider-black-bar">
                                        <a href="<?php echo  get_permalink($unriderskeys); ?>" style="color: #fff">                                        
                                            <span class="person-name" style="color: #fff; width: 100%; display: block"><?php echo get_post_meta( $unriderskeys, 'rdLastName', true );  ?></span>
                                            <span class="person-title" style="color: #fff; width: 100%; display: block"><?php echo get_post_meta( $unriderskeys, 'rdFirstName', true );  ?></span>                                        
                                        </a>
                            </div>
                        </div>
                    </div>
                  
                   <?php }
                   $r++;
                   
                   }
                
                
                }  
            endif;    
            ?>                
        </div>
        <div class="clear"></div>
        
        
        <div>            
            <ul class="fusion-checklist">
                <?php          
                    $cwebc_get_gallery_query = "SELECT *  FROM `wp_postmeta` WHERE `meta_key` = 'key_gallery_events_name'";  
                    $cwebc_query_result=$wpdb->get_results($cwebc_get_gallery_query, OBJECT);
                    if(!empty($cwebc_query_result)):
                        foreach($cwebc_query_result as $cwebc_query_results):
                        
                            $getmetvaldd =  $cwebc_query_results->meta_value;
                            $ungetmetval = unserialize($getmetvaldd);
                             
                        if($ungetmetval != ''){
                            
                            if(in_array($post->ID, $ungetmetval)): 
                                
                                //echo $cwebc_query_results->post_id;
                           // echo get_post_status( $cwebc_query_results->post_id );
                                if ( 'publish' == get_post_status( $cwebc_query_results->post_id ) ){ ?>
                                    
                               <div><br/><h3><?php echo __('Media Gallery');?></h3></div>
                                   <?php 
                                $get_event_img_gall = do_shortcode('[wpsgallery id='.$cwebc_query_results->post_id.']');
                                $geteventvideogallery = get_post_meta($cwebc_query_results->post_id,'key_video_box', true);
                                $geteventvideogallery1 = unserialize($geteventvideogallery);
                                
                     
                                echo
                                '<li class="fusion-li-item size-small ">'
                                    .'<div class="fusion-title title" style="display:none"><h4 style="display:none" class="title-heading-left">'.  get_the_title($cwebc_query_results->post_id).'</h4><div class="title-sep-container" style="display:none"><div class="title-sep sep-single"></div></div></div>';                                    
                                    if($get_event_img_gall == '' && $geteventvideogallery == ''){
                                        echo 'No media in the gallery';
                                    }
                                    if($get_event_img_gall != ''){
                                        echo '<span>Image Gallery</span>';
                                        echo do_shortcode('[wpsgallery id='.$cwebc_query_results->post_id.']');
                                    }
                                    if(count($geteventvideogallery1) > 0 ){
                                        echo  '<br/><span>Video Gallery</span><div class="clear"></div>'  ;                                     
                                        
                                        //Starts Video Gallery Loop                                       
                                        $vid = 1;
                                        foreach ($geteventvideogallery1 as $geteventvideogallery1ssss){
                                        ?>          
                                            <div class="fusion-one-half one_half fusion-layout-column fusion-column <?php if($vid%2 == 0) echo 'last'; ?> spacing-yes">
                                                <a rel="prettyphoto" onclick="_gaq.push(['_trackEvent', 'outbound-article', 'https://www.youtube.com/watch?v=<?php echo $geteventvideogallery1ssss; ?>', '']);" href="https://www.youtube.com/watch?v=<?php echo $geteventvideogallery1ssss; ?>"><img style="width: 148px;height: 95px;" alt="RT with Thunder180" src="https://i.ytimg.com/vi/<?php echo $geteventvideogallery1ssss; ?>/mqdefault.jpg" class="alignnone size-full wp-image-11815"></a>                                                           
                                            </div>    
                                        <?php
                                        $vid++;
                                        }                                                                               
                                    }
                                echo '<div class="clear"></div></li>';
                         }else{
                             echo '';
                         }
                            endif;
                            
                        }
                        endforeach;
                        
                    endif;                
                ?>
            </ul>
        </div>
        <div class="clear"></div>
        <div> <!---Google Map Section --Starts-->
                <div id="us1" style="width: 500px; height: 400px;"></div>
                <script>jQuery('#us1').locationpicker({
                    location: {latitude: <?php echo get_post_meta( $post->ID, 'eventEndPoint', true ); ?>, longitude: <?php echo get_post_meta( $post->ID, 'eventStartPoint', true ); ?>},
                    radius: 300
                });</script>
        </div> <!---Google Map Section --Ends-->
    </div>
</div>
<div class="sidebar">
    <?php         
        //Get Result Of Events
        $post_results_for_eventn = get_posts( $get_results_for_eventn );
        foreach ($post_results_for_eventn as $post_results_for_eventn_g){
            echo '<div style="margin-top:10%"><strong>' .ucfirst($post_results_for_eventn_g->post_title).'</strong></div><hr/>';
            $getridersdatanow = get_post_meta( $post_results_for_eventn_g->ID, 'key_rider_id', true );
            $unridersdatanow = unserialize($getridersdatanow);
            
            $get_key_rider_marks = get_post_meta( $post_results_for_eventn_g->ID, 'key_rider_marks', true );
            $un_get_key_rider_marks = unserialize($get_key_rider_marks);
            //print_r($un_get_key_rider_marks);
            echo '<ul style="padding: 0px 0px 0px 22px;">';
            foreach($unridersdatanow as $keyasval => $unridersdatanows){                
                echo '<li><a href="'.get_permalink($unridersdatanows).'">';                
                echo $un_get_key_rider_marks[$keyasval] .' > '. get_post_meta( $unridersdatanows, 'rdFirstName', true ) .'&nbsp', get_post_meta( $unridersdatanows, 'rdLastName', true );                
                echo '</a></li>';
            }
            echo '</ul>';
        }
    ?>
    <br/>
<?php if(get_post_meta($post->ID,'key_event_update', true)!=''):?>
    <strong><?php echo __('Updates on');?> <?php echo $post->post_title; ?></strong><hr/>
        <?php echo get_post_meta($post->ID,'key_event_update', true); ?>
    
    <br/><br/>
<?php endif;?>
    <strong>#lottosoudal</strong><hr/>
    <a class="twitter-timeline" href="https://twitter.com/search?q=lottosoudal" data-widget-id="564778691775832064"><?php echo __('Tweets about lottosoudal');?></a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>         
</div>


<style type="text/css">
    .cycle_gallery{background-color: #F5F5F5;
border-top: 1px solid #EBEBEB;
border-left: 1px solid #d8d8d8;
border-right: 1px solid #d8d8d8;
border-bottom: 1px solid #d8d8d8;
padding: 4%;}

.cycle_gallery li{background-color: #F5F5F5;
border-top: 1px solid #EBEBEB;
border-left: 1px solid #d8d8d8;
border-right: 1px solid #d8d8d8;
border-bottom: 1px solid #d8d8d8;
padding: 4%;}

.cycle_gallery li ul li{border: 0px !important}
</style>
<?php get_footer(); ?>

