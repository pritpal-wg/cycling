<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the dashboard.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Plugin_Name
 * @subpackage Plugin_Name/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, dashboard-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    Plugin_Name
 * @subpackage Plugin_Name/includes
 * @author     Your Name <email@example.com>
 */
/**
 * Calls the class on the post edit screen.
 */

function call_eventCustomFeilds() {
    new eventCustomFeilds();
}

if ( is_admin() ) {
    add_action( 'load-post.php', 'call_eventCustomFeilds' );
    add_action( 'load-post-new.php', 'call_eventCustomFeilds' );
}



$prefix = 'event';
$c_events_meta_boxes[] =  array(                          // list of meta fields

                    array(
                        'name' => __('Country','wp-cycle-manager'),                 // field name
                        'desc' => __('','wp-cyle-manager'), // field description, optional
                        'id' => $prefix . 'Country',              // field id, i.e. the meta key
                        'type' => 'selected',                       // text box
                        'std' => ''                    // default value, optional
                    ),
                    array(
                        'name' => __('State / county','wp-team-manager'),                  // field name
                        'desc' => __('','wp-team-manager'), // field description, optional
                        'id' => $prefix . 'State',              // field id, i.e. the meta key
                        'type' => 'text',                       // text box
                        'std' => ''                    // default value, optional
                    ),
                    array(
                        'name' => __('City','wp-team-manager'),                  // field name
                        'desc' => __('','wp-team-manager'), // field description, optional
                        'id' => $prefix . 'City',              // field id, i.e. the meta key
                        'type' => 'text',                       // text box
                        'std' => ''                    // default value, optional
                    ),                   
                    array(
                        'name' => __('Address 1','wp-team-manager'),                  // field name
                        'desc' => __('','wp-team-manager'), // field description, optional
                        'id' => $prefix . 'Address1',              // field id, i.e. the meta key
                        'type' => 'text',                       // text box
                        'std' => ''                    // default value, optional
                    ),        
                    array(
                        'name' => __('Address 2','wp-team-manager'),                 // field name
                        'desc' => __('','wp-team-manager'), // field description, optional
                        'id' => $prefix . 'Address2',              // field id, i.e. the meta key
                        'type' => 'text',                       // text box
                        'std' => ''                    // default value, optional
                    ),
                    array(
                        'name' => __('Zip Code / Post Code','wp-team-manager'),                  // field name
                        'desc' => __('','wp-team-manager'), // field description, optional
                        'id' => $prefix . 'ZipCode',              // field id, i.e. the meta key
                        'type' => 'text',                       // text box
                        'std' => ''                    // default value, optional
                    ),
                    array(
                        'name' => __('Date of Event','wp-team-manager'),                  // field name
                        'desc' => __('','wp-team-manager'), // field description, optional
                        'id' => $prefix . 'DateofEvent',              // field id, i.e. the meta key
                        'type' => 'text',                       // text box
                        'std' => ''                    // default value, optional
                    ),
                    array(
                        'name' => __('Description / Abstract','wp-team-manager'),                  // field name
                        'desc' => __('','wp-team-manager'), // field description, optional
                        'id' => $prefix . 'Description',              // field id, i.e. the meta key
                        'type' => 'textarea',                       // text box
                        'std' => ''                    // default value, optional
                    ),
                    
                    array(
                        'name' => __('Website','wp-team-manager'),                  // field name
                        'desc' => __('','wp-team-manager'), // field description, optional
                        'id' => $prefix . 'Website',              // field id, i.e. the meta key
                        'type' => 'text',                       // text box
                        'std' => ''                    // default value, optional
                    ),
                    array(
                        'name' => __('Google Map Start Point','wp-team-manager'),                 // field name
                        'desc' => __('Enter Google Map Start Point (Longitudinal)','wp-team-manager'), // field description, optional
                        'id' => $prefix . 'StartPoint',              // field id, i.e. the meta key
                        'type' => 'text',                       // text box
                        'std' => ''                    // default value, optional
                    ),
                    array(
                        'name' => __('Google Map End Point','wp-team-manager'),                 // field name
                        'desc' => __('Enter Google Map End Point (Latitude)','wp-team-manager'), // field description, optional
                        'id' => $prefix . 'EndPoint',              // field id, i.e. the meta key
                        'type' => 'text',                       // text box
                        'std' => ''                    // default value, optional
                    ),
                    
);


$cweb_rider_args = array(
                                    'posts_per_page'   => 200,
                                    'offset'           => 0,
                                    
                                    'category_name'    => '',
                                    'orderby'          => 'post_date',
                                    'order'            => 'DESC',
                                    'include'          => '',
                                    'exclude'          => '',
                                    'meta_key'         => '',
                                    'meta_value'       => '',
                                    'post_type'        => 'riders',
                                    'post_mime_type'   => '',
                                    'post_parent'      => '',
                                    'post_status'      => 'publish',
                                    'suppress_filters' => 0 );
if(isset($_POST['event_team_name'])){
    
    $cweb_rider_args['category']  = $_POST['event_team_name'];
}

$cweb_results_args = array(
                                    'posts_per_page'   => 200,
                                    'offset'           => 0,
                                    'category'         => '',
                                    'category_name'    => '',
                                    'orderby'          => 'post_date',
                                    'order'            => 'DESC',
                                    'include'          => '',
                                    'exclude'          => '',
                                    'meta_key'         => 'key_events_result_name',
                                    
                                    'post_type'        => 'event_results',
                                    'post_mime_type'   => '',
                                    'post_parent'      => '',
                                    'post_status'      => 'publish',
                                    'suppress_filters' => 0 );
if(isset($_GET['post'])){
    $cweb_results_args['meta_value'] = $_GET['post'];
}

$cweb_gallery_arguments = array(
                                    'posts_per_page'   => 200,
                                    'offset'           => 0,
                                    'category'         => '',
                                    'category_name'    => '',
                                    'orderby'          => 'post_date',
                                    'order'            => 'DESC',
                                    'include'          => '',
                                    'exclude'          => '',
                                    'meta_key'         => 'key_gallery_events_name',                                    
                                    'post_type'        => 'suddo_gallery',
                                    'post_mime_type'   => '',
                                    'post_parent'      => '',
                                    'post_status'      => 'publish',
                                    'suppress_filters' => 0 );

if(isset($_GET['post'])){
    $cweb_gallery_arguments['meta_value'] = $_GET['post'];
}

/** milliejanescorner
 * The Class.
 */
class eventCustomFeilds {
        
	/**
	 * Hook into the appropriate actions when the class is constructed.
	 */
	public function __construct() {
            
		add_action( 'add_meta_boxes', array( $this, 'add_meta_box' ) );
		add_action( 'save_post', array( $this, 'save' ) );
                
                add_action( 'admin_footer', array( $this,'my_action_javascript') ); // Write our JS below here
                add_action( 'wp_ajax_my_action', array( $this,'my_action_callback') );
                
               
	}

	/**
	 * Adds the meta box container.
	 */
	public function add_meta_box( $post_type ) {
            $post_types = array('events');     //limit meta box to certain post types
            if ( in_array( $post_type, $post_types )) {
		add_meta_box(
			'some_meta_box_name'
			,__( 'Details', 'myplugin_textdomain' )
			,array( $this, 'render_meta_box_content' )
			,$post_type
			,'advanced'
			,'high'
		);                                                
            }     
            add_meta_box( 'cwebc-meta-box', __( 'Results', 'textdomain' ), array( $this,'cwebc_meta_box_output'), 'events', 'side', '' );
            add_meta_box( 'cwebc-meta-gallery', __( 'Gallery', 'textdomain' ), array( $this,'cwebc_meta_box_gallery'), 'events', 'side', '' );
            add_meta_box( 'cwebc-meta-event_update', __( 'Events Updates', 'textdomain' ), array( $this,'cwebc_meta_box_event_update'), 'events', 'side', '' );
	}
        



	/**
	 * Save the meta when the post is saved.
	 *
	 * @param int $post_id The ID of the post being saved.
	 */
	public function save( $post_id ) {
                global $c_events_meta_boxes;
		/*
		 * We need to verify this came from the our screen and with proper authorization,
		 * because save_post can be triggered at other times.
		 */

		// Check if our nonce is set.
		if ( ! isset( $_POST['myplugin_inner_custom_box_nonce'] ) )
			return $post_id;

		$nonce = $_POST['myplugin_inner_custom_box_nonce'];

		// Verify that the nonce is valid.
		if ( ! wp_verify_nonce( $nonce, 'myplugin_inner_custom_box' ) )
			return $post_id;

		// If this is an autosave, our form has not been submitted,
                //     so we don't want to do anything.
		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) 
			return $post_id;

		// Check the user's permissions.
		if ( 'page' == $_POST['post_type'] ) {

			if ( ! current_user_can( 'edit_page', $post_id ) )
				return $post_id;
	
		} else {

			if ( ! current_user_can( 'edit_post', $post_id ) )
				return $post_id;
		}

		/* OK, its safe for us to save the data now. */
                
                global $cweb_rider_args;
                $rider_posts1 = get_posts( $cweb_rider_args );
                foreach ( $rider_posts1 as $rider_post1 ){
                    update_post_meta( $post_id, $rider_post1->ID, $_POST['rd_'.$rider_post1->ID] );
                } 
                                                                
		// Sanitize the user input.
                
                //Save Riders
                $mydata1 = serialize( $_POST['rd_rdr'] );  
                update_post_meta( $post_id, 'riderskey', $mydata1 );
                
                //Save Array
                foreach ($c_events_meta_boxes['0'] as $field) {
                    update_post_meta( $post_id, $field['id'], $_POST[$field['id']] );
                }
                
                update_post_meta( $post_id, 'key_event_result_nm', $_POST['event_result_nm'] );
                update_post_meta( $post_id, 'key_event_gallery_nm', $_POST['event_gallery_nm'] );
                
                // Save Event Team                
                update_post_meta( $post_id, 'key_event_team_name', $_POST['event_team_name'] );
                
                //Save Riders
                $gallery_event = serialize( $_POST['gallery_event'] );  
                update_post_meta( $post_id, 'key_gallery_event', $gallery_event );
                
                
                // Events Update
                update_post_meta( $post_id, 'key_event_update', $_POST['event_update'] );
               
                
	}


        function my_action_javascript() { ?>
	<script type="text/javascript" >
	jQuery(document).ready(function($) {

		var data = {
			'action': 'my_action',
			'whatever': 1234
		};
                
		// since 2.8 ajaxurl is always defined in the admin header and points to admin-ajax.php
		$.post(ajaxurl, data, function(response) {
			//alert('Got this from the server: ' + response);
		});
	});
	</script> <?php 
}
        
        
        function my_action_callback() {
            
            global $wpdb; // this is how you get access to the database

            $whatever = intval( $_REQUEST['whatever'] );

            $whatever += 10;
            
             ob_clean();
            echo $whatever;

            wp_die(); // this is required to terminate immediately and return a proper response
        }
        
        
	/**
	 * Render Meta Box content.
	 *
	 * @param WP_Post $post The post object.
	 */
	public function render_meta_box_content( $post ) {
	
		// Add an nonce field so we can check for it later.
		wp_nonce_field( 'myplugin_inner_custom_box', 'myplugin_inner_custom_box_nonce' );
		
                global $c_events_meta_boxes;
                global $cweb_results_args;
                global $cweb_rider_args;
                global $cweb_gallery_arguments;
                global $countries;
                global $s_countries;
                
                
                echo '<link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
  <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>';
               
                
		echo '<table class="form-table">';

		foreach ($c_events_meta_boxes['0'] as $field) {
			$meta = get_post_meta($post->ID, $field['id'], true);
			//$meta = !empty($meta) ? $meta : $field['std'];

			echo '<tr>';
			// call separated methods for displaying each type of field
			//call_user_func(array(&$this, 'show_field_' . $field['type']), $field, $meta);
                        echo '<th><label for="myplugin_new_field">';
                            _e( ''.$field['name'].'', 'myplugin_textdomain' );
                            if($field['id']=='eventWebsite'){
                                echo ' ( http:// ) ';
                            }
                        echo '</label></th><td> ';
                        if($field['type'] == 'textarea'){
                            echo '<textarea id="'.$field['id'].'" name="'.$field['id'].'"';
                            echo ' style="width:95%" >'.esc_attr( $meta ).'</textarea>';
                        }elseif($field['type'] == 'selected'){                            
                            echo '<select id="'.$field['id'].'" name="'.$field['id'].'" style="width:95%">';
                            foreach ($s_countries as $countid=>$contval){
                            ?>
                            <option value="<?php echo $contval; ?>" <?php if($meta == $contval ){ echo 'selected=selected'; }?>><?php echo __ ($countries[$contval]); ?></option>                           
                            <?php                            
                            }
                            echo '</select>';
                        }else{
                            echo '<input type="'.$field['type'].'" id="'.$field['id'].'" name="'.$field['id'].'"';
                            echo ' value="' . esc_attr( $meta ) . '" style="width:95%" />';
                            echo '<i>'.$field['desc'].'</i>';
                            echo '</td></tr>';                                                      
                        }
                }   
		
                
                wp_reset_postdata();
                
                //For Team Meta
                $teams_args = array(
                        'type'                     => 'riders',              
                        'orderby'                  => 'name',
                        'order'                    => 'ASC',                       
                        'taxonomy'                 => 'teams',                      
                ); 
                $teams_categories = get_categories( $teams_args );
                //echo '<pre>';
                //print_r($teams_categories);
                echo '<tr><th><label for="myplugin_new_field">';
                            _e( 'Select Team', 'myplugin_textdomain' );
                echo '</label></th><td>';
                echo '<select style="width:95%" name="event_team_name" id="main_team_list" onchange="showUser(this.value)">'
                . '<option value="">Select Team</option>';
                foreach ($teams_categories as $teams_category) {
                    $cwebc_get_event_team_meta = get_post_meta($post->ID, 'key_event_team_name', true); 
                ?>
                    <option value="<?php echo $teams_category->cat_ID ?>" <?php if($teams_category->cat_ID == $cwebc_get_event_team_meta){ echo "selected=selected";} ?>><?php echo $teams_category->cat_name; ?> </option>
                <?php        
                }
                echo '</select>';                                
                echo '</td></tr>';    
                
                // For Riders Meta
                $cwebc_get_event_team_for_rider = get_post_meta($post->ID, 'key_event_team_name', true);
                if(!isset($cwebc_get_event_team_for_rider) || $cwebc_get_event_team_for_rider == '' || $cwebc_get_event_team_for_rider == '0'){
                    //echo '<input name="save" type="submit" class="button button-primary button-large" id="publish" accesskey="p" value="Update">';
                    echo '<tr><th>';                
                    echo '</th><td>';  
                    echo '<p>Please update team first to select riders</p>';                    
                    echo '</td></tr>';
                }else{
                echo '<tr><th>';                
                echo '</th><td>';    
                $getteamslug = get_term_by('id', $cwebc_get_event_team_for_rider, 'teams');
                $cweb_rider_args1 = array(
                                    'posts_per_page'   => 200,
                                    'offset'           => 0,
                                    'category'         => '',
                                    'category_name'    => '',
                                    'orderby'          => 'post_date',
                                    'order'            => 'DESC',
                                    'include'          => '',
                                    'exclude'          => '',
                                    'meta_key'         => '',
                                    'meta_value'       => '',
                                    'tax_query'        => array(
                                                            array(
                                                        'taxonomy' => 'teams',
                                                        'field' => 'slug',
                                                        'terms' => $getteamslug)),
                                    'post_type'        => 'riders',
                                    'post_mime_type'   => '',
                                    'post_parent'      => '',
                                    'post_status'      => 'publish',
                                    'suppress_filters' => 0
                    
                                    );
                
                
                    $rider_posts = get_posts( $cweb_rider_args1 );
                    //echo '<pre>';
                    //print_r($rider_posts);
                    $rider_metas = get_post_meta($post->ID, 'riderskey', true);
                    if( ! empty( $rider_metas ) ) {
                        $getunserary = unserialize($rider_metas);
                    }                     
                    echo '<ul class="ridersclass">';
                    foreach ( $rider_posts as $rider_post ) : setup_postdata( $rider_post );                     
                    ?>                    
                        <li>
                            <label><input type="checkbox" value="<?php echo $rider_post->ID; ?>" name="rd_rdr[]" id="rd_<?php echo $rider_post->ID; ?>" <?php if(isset($getunserary) || $getunserary != ''){echo in_array($rider_post->ID, $getunserary)? 'checked=checked': '';} ?> /><?php echo $rider_post->post_title; ?></label>
                        </li>
                    <?php endforeach; 
                    echo '</ul>';
                    wp_reset_postdata();
                echo '</td></tr>';
                }
                echo '<tr><td></td><td><p><input name="save" type="submit" class="button button-primary button-large" id="publish" accesskey="p" value="Update"></p></td></tr>';
                
                //$get_results_args = get_posts( $cweb_results_args );
                //echo '<pre>';
                //print_r($get_results_args);
                 
                 /*
                echo '<tr><th><label for="myplugin_new_field">';
                            _e( 'Select Result', 'myplugin_textdomain' );
                echo '</label></th><td>';
                echo '<select style="width:95%" name="event_result_nm" id="main_team_list">'
                . '<option value="">Select Result</option>';
                foreach ($get_results_args as $get_results_arg) {
                    $get_rest_sel_val = get_post_meta($post->ID, 'key_event_result_nm', true);
                    ?>
                    <option value="<?php echo $get_results_arg->ID ?>" <?php if($get_results_arg->ID ==$get_rest_sel_val){ echo "selected=selected";} ?> > <?php echo $get_results_arg->post_title ?> </option>
                <?php    
                }
                echo '</select>';
                wp_reset_postdata();$rider_metas = get_post_meta($post->ID, 'riderskey', true);
                    if( ! empty( $rider_metas ) ) {
                        $getunserary = unserialize($rider_metas);
                    }
                
                echo '</td></tr>';
                */
               
                echo '</table>';
              
                              
	}
        
        
        /* Add Metabox on righjt side bar */   
        // For result
        function cwebc_meta_box_output( $post ) {
                // create a nonce field
                wp_nonce_field( 'my_cwebc_meta_box_nonce', 'cwebc_meta_box_nonce' );
                global $cweb_results_args;
                
                if(isset($_GET['post'])){
                $get_gallery_args = get_posts( $cweb_results_args );
                //echo '<pre>';
                //print_r($get_results_args);
                if(count($get_gallery_args) == '0' || count($get_gallery_args) == ''){
                    echo 'No Result Yet';
                }else{
                    $result_no = 1;
                    echo '<ul class="categorychecklist form-no-clear">';
                    foreach ($get_gallery_args as $get_gallery_arg) {
                    $result_url = admin_url( 'post.php?post='.$get_gallery_arg->ID.'&action=edit', 'http' );
                    
                    $get_rest_sel_val = get_post_meta($post->ID, 'key_event_result_nm', true);
                    ?>
                        <li><?php echo $result_no; ?>. <a href="<?php echo $result_url; ?>"><?php echo $get_gallery_arg->post_title; ?></a></li>
                    <?php 
                    $result_no++;
                    }
                    echo '</ul>';
                }
                }
        }
        
        // For result
        function cwebc_meta_box_gallery( $post ) {
                // create a nonce field
                wp_nonce_field( 'my_cwebc_meta_box_nonce', 'cwebc_meta_box_nonce' );
                
                    global $wpdb;
                    $cwebc_get_gallery_query = "SELECT *  FROM `wp_postmeta` WHERE `meta_key` = 'key_gallery_events_name'";  
                    $cwebc_query_result=$wpdb->get_results($cwebc_get_gallery_query, OBJECT);
                    if(!empty($cwebc_query_result)):
                        
                        echo '<ul class="categorychecklist form-no-clear">';
                        foreach($cwebc_query_result as $cwebc_query_results):
                            $getmetvaldd =  $cwebc_query_results->meta_value;
                            $ungetmetval = unserialize($getmetvaldd);
                            if(in_array($post->ID, $ungetmetval)):  
                                $result_url_gallery = admin_url( 'post.php?post='.$cwebc_query_results->post_id.'&action=edit', 'http' );
                                echo '<li style="list-style-type: decimal;list-style-position: inside;"> <a href="'.$result_url_gallery.'">'.get_the_title($cwebc_query_results->post_id).'</a></li>';
                            endif;                            
                        endforeach;  
                        echo '</ul>';
                    endif;    
                
                /*
                global $cweb_gallery_arguments;
                
                $get_results_args = get_posts( $cweb_gallery_arguments );
                //echo '<pre>';
                //print_r($get_results_args);
                if(count($get_results_args) == '0' || count($get_results_args) == ''){
                    echo 'No Gallery Yet';
                }else{
                    $get_gallery_metas = get_post_meta($post->ID, 'key_gallery_event', true);
                    if( ! empty( $get_gallery_metas ) ) {
                        $un_get_gallery_metas = unserialize($get_gallery_metas);
                    }
                    
                    $gallery_no = 1;
                    echo '<ul class="categorychecklist form-no-clear">';
                    foreach ($get_results_args as $get_results_arg) {
                    
                    
                    $get_rest_sel_val = get_post_meta($post->ID, 'key_event_result_nm', true);
                    
                    $result_url_gallery = admin_url( 'post.php?post='.$get_results_arg->ID.'&action=edit', 'http' );
                    ?>
                    <li>
                        <?php echo $gallery_no; ?>. <a href="<?php echo $result_url_gallery; ?>"><?php echo $get_results_arg->post_title; ?></a>
                    </li>
                    <?php 
                    $gallery_no++;
                    }
                    echo '</ul>';
                }
                */
        }
        
        //For Event Updates
        function cwebc_meta_box_event_update( $post ) {
                // create a nonce field
                wp_nonce_field( 'my_cwebc_meta_box_nonce', 'cwebc_meta_box_nonce' );
                $_key_event_update = get_post_meta($post->ID,'key_event_update', true);
                echo '<textarea style="width:100%" name="event_update">'.$_key_event_update.'</textarea>';
        }
}

function events_widgets_init() {
    register_sidebar( array(
        'name' => __( 'Calender Events Sidebar', 'cal_events' ),
        'id' => 'cal_sidebar',
        'description' => __( 'Widgets in this area will be shown on calender page.', 'theme-slug' ),        
	'before_widget' => '<div id="%1$s" class="widget %2$s">',
	'after_widget' => '</div>',
	'before_title' => '<div class="heading"><h3>',
	'after_title' => '</h3></div>',
    ) );
}
add_action( 'widgets_init', 'events_widgets_init' );

