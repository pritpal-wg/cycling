<?php
// Template Name: Events Template
get_header(); 
global $post;
global $smof_data;

$term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) ); 

$gettermslug =  $term->slug; // will show the slug


 $get_all_riders_arc = array(
                                    'posts_per_page'   => 200,
                                    'offset'           => 0,
                                    'category'         => '',
                                    'category_name'    => '',
                                    'orderby'          => 'meta_value',
                                    'order'            => 'ASC',
                                    'include'          => '',
                                    'exclude'          => '',
                                    'meta_key'         => 'rdLastName',
                                    'meta_value'       => '',
                                    'tax_query'        => array(
                                                            array(
                                                            'taxonomy' => 'teams',
                                                            'field' => 'slug',
                                                            'key' => 'rdLastName',
                                                            'terms' => $gettermslug)),
                                    
                                    'post_mime_type'   => '',
                                    'post_parent'      => '',
                                    'post_status'      => 'publish',
                                    'suppress_filters' => true ); 
/*$get_all_riders_arc = array(
       'posts_per_page'   => 200,        
       'order' => 'ASC',
       'orderby' => 'meta_value',
       'meta_key' => 'rdLastName',
       'post_status'      => 'publish',
       'paged'            => $paged,
       'meta_query' => array(
               array(
                       'key' => 'rdLastName',
                       'compare' => '!=' ,
                           'type' => 'NUMERIC'                        
               )
       
       
       )
);*/





if(isset($_GET['getstaff']) && $_GET['getstaff'] == '1'){
    $get_all_riders_arc['post_type'] = 'suddo_staff';
}else{
    $get_all_riders_arc['post_type'] = 'riders';
}
//echo '<pre>';
//print_r($get_all_riders_arc);
?>


<style type="text/css">
    .full-width{display: none}
    .rider-black-bar{position: absolute; bottom: 0; background: #000; color: #fff; width: 100%; padding: 2% 5%;opacity: 0.8;}
    .rider-main-div{height: 305px}
    .rider-main-div:hover > .rider-black-bar{background:<?php echo $smof_data['primary_color']; ?>}
</style>
<div id="content" class="rider_container" style="float: left;width:71% !important; margin:0 !important">

    
                 <?php
                    
                    $get_all_riders_posts = get_posts( $get_all_riders_arc );
                    
                    $i = 1;
                    foreach($get_all_riders_posts as $get_all_riders_post):   
                        
                ?>
    <div class="fusion-one-fourth one_fourth fusion-layout-column fusion-column <?php if($i%4 == 0){ echo 'last';} ?> spacing-yes" >                                
                                <?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $get_all_riders_post->ID ), 'medium' ); ?>
        <div style="position: relative" class="rider-main-div">
            <a href="<?php echo get_permalink($get_all_riders_post->ID); ?>" style="color: #fff"> 
                                    <?php if($image[0] != ''):?>
                                        <img class="person-img img-responsive" src="<?php echo $image[0]; ?>" style="width: 100%" alt="<?php the_title(); ?>" />
                                    <?php else: ?>
                                    <img class="person-img img-responsive" src="<?php echo plugin_dir_url( __FILE__ ); ?>/admin/img/no-image.jpg" alt="<?php the_title(); ?>" style="width: 100%" />
                                    <?php endif; ?>
                                    </a>
                                    <div class="rider-black-bar">
                                        <a href="<?php echo get_permalink($get_all_riders_post->ID); ?>" style="color: #fff">                                        
                                            <span class="person-name" style="color: #fff; width: 100%; display: block"><?php echo get_post_meta( $get_all_riders_post->ID, 'rdLastName', true );  ?></span>
                                            <span class="person-title" style="color: #fff; width: 100%; display: block"><?php echo get_post_meta( $get_all_riders_post->ID, 'rdFirstName', true );  ?></span>                                        
                                        </a>
                                    </div>
                                </div>
                                
                                
                            </div>
                <?php
                        $i++;
			// End the loop.
			endforeach;
?>
</div>

   <div id="sidebar" class="sidebar rider_sidebar">
<?php 

    dynamic_sidebar('Rider List Sidebar');
?>    
</div> 
<div class="clear"></div>
<?php get_footer(); ?>

<style>
    #sidebar{
        
        display:none;
    }    
    
    .rider_sidebar {
        
        display:block !important;
    }
    
#content {
display:none;
}
.rider_container {

display:block !important;

}
</style>


