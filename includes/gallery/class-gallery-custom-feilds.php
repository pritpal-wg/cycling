<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the dashboard.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Plugin_Name
 * @subpackage Plugin_Name/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, dashboard-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    Plugin_Name
 * @subpackage Plugin_Name/includes
 * @author     Your Name <email@example.com>
 */
/**
 * Calls the class on the post edit screen.
 */
function call_galleryCustomFeilds() {
    new galleryCustomFeilds();
}

if ( is_admin() ) {
    add_action( 'load-post.php', 'call_galleryCustomFeilds' );
    add_action( 'load-post-new.php', 'call_galleryCustomFeilds' );
}

$get_events_data = array();

$gallery_events_args = array(
                                    'posts_per_page'   => 1000,
                                    'offset'           => 0,
                                    'category'         => '',
                                    'category_name'    => '',
                                    'orderby'          => 'post_date',
                                    'order'            => 'DESC',
                                    'include'          => '',
                                    'exclude'          => '',
                                    'meta_key'         => '',
                                    'meta_value'       => '',
                                    'post_type'        => 'events',
                                    'post_mime_type'   => '',
                                    'post_parent'      => '',
                                    'post_status'      => 'publish',
                                    'suppress_filters' => 0 );


/** 
 * The Class.
 */
class galleryCustomFeilds {
        
	/**
	 * Hook into the appropriate actions when the class is constructed.
	 */
	public function __construct() {
            
		add_action( 'add_meta_boxes', array( $this, 'add_meta_box' ) );
		add_action( 'save_post', array( $this, 'save' ) );    
                
               // add_meta_box( 'cwebc-meta-video_gallery', __( 'Video Gallery', 'textdomain' ), array( $this,'cwebc_meta_box_video_gallery'), 'suddo_gallery', 'side', '' );
	}

	/**
	 * Adds the meta box container.
	 */
	public function add_meta_box( $post_type ) {
            $post_types = array('suddo_gallery');     //limit meta box to certain post types
            if ( in_array( $post_type, $post_types )) {
		add_meta_box(
			'some_meta_box_name'
			,__( 'Select Gallery Event', 'myplugin_textdomain' )
			,array( $this, 'render_meta_box_content' )
			,$post_type
			,'advanced'
			,'high'
		);
                
                
                add_meta_box(
			'some_meta_box_name1'
			,__( 'Select Youtube Gallery', 'myplugin_textdomain1' )
			,array( $this, 'render_meta_box_content1' )
			,$post_type
			,'advanced'
			,'high'
		);
            }
	}

	/**
	 * Save the meta when the post is saved.
	 *
	 * @param int $post_id The ID of the post being saved.
	 */
	public function save( $post_id ) {
                global $rider_meta_boxes;
                global $pal_totals;
		/*
		 * We need to verify this came from the our screen and with proper authorization,
		 * because save_post can be triggered at other times.
		 */

		// Check if our nonce is set.
		if ( ! isset( $_POST['myplugin_inner_custom_box_nonce'] ) )
			return $post_id;

		$nonce = $_POST['myplugin_inner_custom_box_nonce'];

		// Verify that the nonce is valid.
		if ( ! wp_verify_nonce( $nonce, 'myplugin_inner_custom_box' ) )
			return $post_id;

		// If this is an autosave, our form has not been submitted,
                //     so we don't want to do anything.
		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) 
			return $post_id;

		// Check the user's permissions.
		if ( 'page' == $_POST['post_type'] ) {

			if ( ! current_user_can( 'edit_page', $post_id ) )
				return $post_id;
	
		} else {

			if ( ! current_user_can( 'edit_post', $post_id ) )
				return $post_id;
		}

		/* OK, its safe for us to save the data now. */
                
                $get_events_data = get_post_meta($post_id, 'key_gallery_events_name', true);
               
                if($get_events_data == '' || !is_array($get_events_data)){
                    $get_events_data = array();
                }
                //print_r($get_events_data);
                
                
                
		// Sanitize the user input.
		$gallery_events_name = $_POST['gallery_events_name'] ;
                if($gallery_events_name != ''){
                    array_push($get_events_data, $gallery_events_name);                
                }
                
		// Update the meta field.
		update_post_meta( $post_id, 'key_gallery_events_name', $get_events_data );
                
                
                $get_events_data1 = get_post_meta($post_id, 'key_gallery_events_name', true);
                //print_r($get_events_data1);
                //exit;
                if(isset($_POST['delete_gallery_events'])){
                    $delete_gallery_events = $_POST['delete_gallery_events'];
                    foreach ($delete_gallery_events as $eventtodel) {     
                        if(($keyss = array_search($eventtodel, $get_events_data1)) !== false) {
                            unset($get_events_data1[$keyss]); 
                        }                                               
                    }
                    
                    
                    update_post_meta( $post_id, 'key_gallery_events_name', $get_events_data1 );
                }
                
                
                
                // Update the meta field.
		update_post_meta( $post_id, 'key_video_gallery', $_POST['video_gallery'] );
                
                
                $get_video_box1 = get_post_meta($post_id, 'key_video_box', true);
                $un_get_video_box1 = unserialize($get_video_box1);
               // echo '<pre>';
               // print_r($un_get_video_box1);
                
                //exit;
                if(isset($_POST['del_video_box'])){
                  
                    $getdel_video_box = $_POST['del_video_box'];
                    //print_r($getdel_video_box);
                    
                    foreach ($getdel_video_box as $valuedel) {
                        
                        unset($un_get_video_box1[$valuedel]);
                        
                    }
                    $serun_get_video_box1 = serialize($un_get_video_box1);               
                    update_post_meta( $post_id, 'key_video_box', $serun_get_video_box1 );
                }else{
                    $getdel_video_box = $_POST['del_video_box'];   
                                        
                    if(!empty($_POST['video_box'])){
                        $ser_video_box = serialize($_POST['video_box']);                    
                        update_post_meta( $post_id, 'key_video_box', $ser_video_box );
                    }
                }
	}


	/**
	 * Render Meta Box content.
	 *
	 * @param WP_Post $post The post object.
	 */
	public function render_meta_box_content( $post ) {
	
		// Add an nonce field so we can check for it later.
		wp_nonce_field( 'myplugin_inner_custom_box', 'myplugin_inner_custom_box_nonce' );
		
                global $gallery_events_args;
                
                $get_gallery_events_args = get_posts( $gallery_events_args );                                
                
                ?>

                <script type="text/javascript">
                        var vidbox = 1;
                        function add_events() {
                            evebox++;
                            var objTo1 = document.getElementById('room_fileds');
                            var divtest1 = document.createElement("div");
                            divtest1.innerHTML = '<input id="text_tag_input'+evebox+'" type="text" name="video_box[]" />';
                            objTo1.appendChild(divtest1)
                                return false;
                        }
                        </script>
                <?php
                
                
                echo '<table class="form-table">';                
                        echo '<tr><th><label for="myplugin_new_field">';
                            _e( 'Select Event', 'myplugin_textdomain' );
                echo '</label></th><td>';
                
                
                //echo '<select style="width:95%" name="gallery_events_name" id="main_team_list">'
                //. '<option value="">Select Event</option>';
                 $get_gallery_events_name = get_post_meta($post->ID, 'key_gallery_events_name', true);
                 //$get_ungallery_events_name = unserialize($get_gallery_events_name);
                // print_r($get_gallery_events_name);
                 
                echo '<ul>';    
                foreach ($get_gallery_events_name as $get_gallery_events_arg) {                 
                    ;
                    ?>
                    <li style="float: left; width: 33%"><label>
                            <input type="checkbox" name="delete_gallery_events[]" value="<?php echo $get_gallery_events_arg ?>" >
                            <span style="width:90%"><?php echo get_the_title($get_gallery_events_arg); ?></span>
                        </label>
                    </li>
                    <?php
echo '</ul>';
                echo '<div class="clear"></div><div style="margin: 1% 0 0 1px;"><input name="save" type="submit" style="background: transparent;border: 0;color: #a00;cursor: pointer;text-decoration: underline;padding: 0;margin: 0;" id="publish" accesskey="p" value="Delete Selected"></div>';
                }
                  
                
                
                echo '<div class="clear"></div><div style="font-weight: bold;margin: 5% 0 1% 3px;">Add More :-</div>';
              echo '<select style="width:95%" name="gallery_events_name" id="main_team_list">'
                . '<option value="">Select Event</option>';
              foreach ($get_gallery_events_args as $get_gallery_events_arg) { 
                  if(in_array($get_gallery_events_arg->ID, $get_gallery_events_name)){
                      
                  }else{
                    echo '<option value="'.$get_gallery_events_arg->ID.'">'.$get_gallery_events_arg->post_title.'</option>';
                  }
              }
              echo '</select>';
              echo '<div style="margin: 2% 0 0 1px;"><input name="save" type="submit" class="button button-primary button-large" id="publish" accesskey="p" value="Add"></div>';  
                echo '</td></tr>'; 
                echo '</table>';
	}          
        public function render_meta_box_content1( $post ) {
            ?>
                    <script type="text/javascript">
                        var vidbox = 1;
                        function add_fields() {
                            vidbox++;
                            var objTo = document.getElementById('room_fileds');
                            var divtest = document.createElement("div");
                            divtest.innerHTML = '<input id="text_tag_input'+vidbox+'" type="text" name="video_box[]" />';
                            objTo.appendChild(divtest)
                                return false;
                        }
                        
                       

jQuery(document).ready(function(){
    jQuery("#show_you_vid").click(function(){
        jQuery("#youtube_pop_div").show();
    });
    jQuery("#hide_you_vid").click(function(){
        jQuery("#youtube_pop_div").hide();
    });
});

                    </script>
                    <div>
                        <a href="javascript:void(0)"  id="show_you_vid" class="button" title="Add Media" onclick="add_fields()" ><span class="wp-media-buttons-icon"></span> Add Media</a>
                    </div>
                    <hr/>
                    <div>
                        <?php 
                            $get_video_box = get_post_meta($post->ID, 'key_video_box', true);
                            $un_get_video_box = unserialize($get_video_box);
                            //print_r($un_get_video_box);
                            echo '<table style="width: 100%;text-align: center;">';
                            echo '<tr style="background: #E5E5E5;"><th></th><th>Youtube ID</th><th>Image</th><th>Delete</th></tr>';
                            foreach ($un_get_video_box as $getkey => $un_get_video_boxs){
                               echo '<tr>'; 
                                echo '<td>';                                
                                echo '</td>';
                                echo '<td>';
                                echo '<input type="text" value="'.$un_get_video_boxs.'" name="video_box[]"  />';
                                echo '</td>';
                                echo '<td>';
                                echo '<img src="https://i.ytimg.com/vi/'.$un_get_video_boxs.'/mqdefault.jpg" style="width: 150px;" />';
                                echo '</td>';
                                echo '<td>';
                                echo '<input type="checkbox" value="'.$getkey.'" name="del_video_box[]"  />';
                                echo '</td>';
                               echo '</tr>';
                            }
                            echo '</table>';
                        ?>
                    </div>
                    <div id="youtube_pop_div" style="display: none">
                    <div style="position: fixed; z-index: 12; width: 100%; height: 100%; background: #000;top: 0;left: 0;opacity: 0.5;">h</div>
                    <div style="width: 80%; background: #fff; position: fixed; height: 60%; top: 20%; z-index: 99999999999">
                        <h3 class="hndle ui-sortable-handle"><span>Add Youtube Video id</span></h3>
                        <a class="media-modal-close" href="javascript:void(0)" id="hide_you_vid"><span class="media-modal-icon"><span class="screen-reader-text">Close media panel</span></span></a>
                        <div style="float: left;margin: 2% 2%;">
                        <div id="room_fileds" style="padding: 1%">
                            
                        </div>                         
                        <div style="padding: 1%">
                            <a href="javascript:void(0)" title="Add Media" onclick="add_fields()">Add More</a>
                        </div>
                        <div style="padding: 1%">
                            <input name="save" type="submit" class="button button-primary button-large" id="publish" accesskey="p" value="Update">
                        </div>
                        </div>
                        <div style="float: left;margin: 2% 2%;"><img src="<?php echo ADV_PLUGIN_WS_PATH1; ?>/admin/img/youtube.png" /></div>
                    </div>
                    </div>
                    
            <?php
        }
        
        
        //For Event Updates
        /*function cwebc_meta_box_video_gallery( $post ) {
                // create a nonce field
                wp_nonce_field( 'my_cwebc_meta_box_nonce', 'cwebc_meta_box_nonce' );
                $_key_video_gallery = get_post_meta($post->ID,'key_video_gallery', true);
                echo '<textarea style="width:100%" name="video_gallery">'.$_key_video_gallery.'</textarea>';
        }*/
       
}





