<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the dashboard.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Plugin_Name
 * @subpackage Plugin_Name/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, dashboard-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    Plugin_Name
 * @subpackage Plugin_Name/includes
 * @author     Your Name <email@example.com>
 */
/**
 * Calls the class on the post edit screen.
 */
global $post;
function call_riderCustomFeilds() {
    new riderCustomFeilds();
}

if ( is_admin() ) {
    add_action( 'load-post.php', 'call_riderCustomFeilds' );
    add_action( 'load-post-new.php', 'call_riderCustomFeilds' );
}


$prefix = 'rd';
$rider_meta_boxes[] =  array(                          // list of meta fields
                    
                    array(
                        'name' => __('First Name','wp-team-manager'),                 // field name
                        'desc' => __('First Name','wp-team-manager'), // field description, optional
                        'id' => $prefix . 'FirstName',              // field id, i.e. the meta key
                        'type' => 'text',                       // text box
                        'std' => ''                    // default value, optional
                    ),
                    array(
                        'name' => __('Last Name','wp-team-manager'),                 // field name
                        'desc' => __('Last Name','wp-team-manager'), // field description, optional
                        'id' => $prefix . 'LastName',              // field id, i.e. the meta key
                        'type' => 'text',                       // text box
                        'std' => ''                    // default value, optional
                    ),
                    array(
                        'name' => __('Gender','wp-team-manager'),                 // field name
                        'desc' => __('Male or Female','wp-team-manager'), // field description, optional
                        'id' => $prefix . 'Gender',              // field id, i.e. the meta key
                        'type' => 'selected',                       // text box
                        'std' => ''                    // default value, optional
                    ),
                    array(
                        'name' => __('Birthday','wp-team-manager'),                  // field name
                        'desc' => __('Birth Date','wp-team-manager'), // field description, optional
                        'id' => $prefix . 'datepicker',              // field id, i.e. the meta key
                        'type' => 'text',                       // text box
                        'std' => ''                    // default value, optional
                    ),
                    array(
                        'name' => __('Nationality','wp-team-manager'),                  // field name
                        'desc' => __('Nationality','wp-team-manager'), // field description, optional
                        'id' => $prefix . 'Nationality',              // field id, i.e. the meta key
                        'type' => 'natselected',                       // text box
                        'std' => ''                    // default value, optional
                    ),                   
                    array(
                        'name' => __('Weight','wp-team-manager'),                  // field name
                        'desc' => __('Weight','wp-team-manager'), // field description, optional
                        'id' => $prefix . 'Weight',              // field id, i.e. the meta key
                        'type' => 'text',                       // text box
                        'std' => ''                    // default value, optional
                    ),        
                    array(
                        'name' => __('Height','wp-team-manager'),                 // field name
                        'desc' => __('Height','wp-team-manager'), // field description, optional
                        'id' => $prefix . 'Height',              // field id, i.e. the meta key
                        'type' => 'text',                       // text box
                        'std' => ''                    // default value, optional
                    ),
                    array(
                        'name' => __('Professional Since','wp-team-manager'),                  // field name
                        'desc' => __('Professional Since','wp-team-manager'), // field description, optional
                        'id' => $prefix . 'Professional',              // field id, i.e. the meta key
                        'type' => 'text',                       // text box
                        'std' => ''                    // default value, optional
                    ),
                    array(
                        'name' => __('Personal Twitter Handle','wp-team-manager'),                  // field name
                        'desc' => __('Personal Twitter Handle','wp-team-manager'), // field description, optional
                        'id' => $prefix . 'Twitter',              // field id, i.e. the meta key
                        'type' => 'text',                       // text box
                        'std' => ''                    // default value, optional
                    ),
                    array(
                        'name' => __('Twitter Data Widget ID','wp-team-manager'),                  // field name
                        'desc' => __('Twitter Data Widget ID','wp-team-manager'), // field description, optional
                        'id' => $prefix . 'TwitterWidgetID',              // field id, i.e. the meta key
                        'type' => 'text',                       // text box
                        'std' => ''                    // default value, optional
                    ),
                    array(
                        'name' => __('Personal Website','wp-team-manager'),                  // field name
                        'desc' => __('Personal Website','wp-team-manager'), // field description, optional
                        'id' => $prefix . 'Website',              // field id, i.e. the meta key
                        'type' => 'text',                       // text box
                        'std' => ''                    // default value, optional
                    ),                    
                    array(
                        'name' => __('Description / Abstract','wp-team-manager'),                  // field name
                        'desc' => __('Description / Abstract','wp-team-manager'), // field description, optional
                        'id' => $prefix . 'Description',              // field id, i.e. the meta key
                        'type' => 'textarea',                       // text box
                        'std' => ''                    // default value, optional
                    ),
                    
                   
);
if(isset($_GET['post'])){
$add_more_palmers = get_post_meta($_GET['post'], 'add-more-pal', true);
$pal_totals = $add_more_palmers;
}


$countries = array();


/** 
 * The Class.
 */
class riderCustomFeilds {
        
	/**
	 * Hook into the appropriate actions when the class is constructed.
	 */
	public function __construct() {
            
		add_action( 'add_meta_boxes', array( $this, 'add_meta_box' ) );
                
                add_action( 'admin_enqueue_scripts', array( $this, 'jquery_ui_scripts') );
                
		add_action( 'save_post', array( $this, 'save' ) );
                
                add_filter( 'page_template', array( $this,'wpa3396_page_template') );
                
                
                
	}

	/**
	 * Adds the meta box container.
	 */
	public function add_meta_box( $post_type ) {
            $post_types = array('riders');     //limit meta box to certain post types
            if ( in_array( $post_type, $post_types )) {
		add_meta_box(
			'some_meta_box_name'
			,__( 'Rider Information', 'myplugin_textdomain' )
			,array( $this, 'render_meta_box_content' )
			,$post_type
			,'advanced'
			,'high'
		);
            }
	}

        
        
        
	/**
	 * Save the meta when the post is saved.
	 *
	 * @param int $post_id The ID of the post being saved.
	 */
	public function save( $post_id ) {
                global $rider_meta_boxes;
                global $pal_totals;
		/*
		 * We need to verify this came from the our screen and with proper authorization,
		 * because save_post can be triggered at other times.
		 */

		// Check if our nonce is set.
		if ( ! isset( $_POST['myplugin_inner_custom_box_nonce'] ) )
			return $post_id;

		$nonce = $_POST['myplugin_inner_custom_box_nonce'];

		// Verify that the nonce is valid.
		if ( ! wp_verify_nonce( $nonce, 'myplugin_inner_custom_box' ) )
			return $post_id;

		// If this is an autosave, our form has not been submitted,
                //     so we don't want to do anything.
		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) 
			return $post_id;

		// Check the user's permissions.
		if ( 'page' == $_POST['post_type'] ) {

			if ( ! current_user_can( 'edit_page', $post_id ) )
				return $post_id;
	
		} else {

			if ( ! current_user_can( 'edit_post', $post_id ) )
				return $post_id;
		}

		/* OK, its safe for us to save the data now. */
                
               
                
                
		// Sanitize the user input.
		$mydata = sanitize_text_field( $_POST['rider_gender'] );
                $mydata1 = sanitize_text_field( $_POST['rider_birthday'] );
                
		// Update the meta field.
		update_post_meta( $post_id, 'key1', $mydata );
                
                update_post_meta( $post_id, 'key2', $mydata1 );
                foreach ($rider_meta_boxes['0'] as $field) {
                    update_post_meta( $post_id, $field['id'], $_POST[$field['id']] );
                }
                
                //Save Riders
                $pl_name = serialize( $_POST['pl_name'] );  
                update_post_meta( $post_id, 'key_$countriespl_name', $pl_name );
                
                $getriderpals = get_post_meta($post_id, 'add-more-pal', true);
                
                
                
                //Save Palmers
                for($pal_no = 1; $pal_no <= $getriderpals; $pal_no++){ 
                    
                    //echo htmlentities(stripslashes($_REQUEST['PalmersDetails1']));
                    $getpaldet = htmlentities(stripslashes($_REQUEST['PalmersDetails'.$pal_no])) ;
                    //exit;
                    update_post_meta( $post_id, 'PalmersName'.$pal_no, $_POST['PalmersName'.$pal_no] );
                    //update_post_meta( $post_id, 'PalmersVictories'.$pal_no, $_POST['PalmersVictories'.$pal_no] );
                    update_post_meta( $post_id, 'PalmersYear'.$pal_no, $_POST['PalmersYear'.$pal_no] );
                    update_post_meta( $post_id, 'PalmersDetails'.$pal_no, $getpaldet);
                }               
                update_post_meta( $post_id, 'add-more-pal', sanitize_text_field($_POST['add-more-pal']) );
                
                
                
                
                
                
	}

        function jquery_ui_scripts() {            
            wp_enqueue_script( 'jquery_ui', '//code.jquery.com/ui/1.11.2/jquery-ui.js', array(), '1.0.0', true );
            wp_enqueue_style('jqueryui-style', '//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css');
            wp_enqueue_script( 'script-name', ADV_PLUGIN_WS_PATH1 . 'admin/js/jquery_ui_script.js', array(), '1.0.0', true );
            
        }

	/**
	 * Render Meta Box content.
	 *
	 * @param WP_Post $post The post object.
	 */
	public function render_meta_box_content( $post ) {
	
		// Add an nonce field so we can check for it later.
		wp_nonce_field( 'myplugin_inner_custom_box', 'myplugin_inner_custom_box_nonce' );
		
                global $rider_meta_boxes;
                global $cweb_palmars_args;
                global $rider_palemer_custom;
                global $pal_totals;
                global $countries;
                global $s_countries;


                
                echo '<script>
                        $(function() {
                          $( "#rddatepicker" ).datepicker({
                              changeYear: true,
                              changeMonth: true,
                              dateFormat: "dd-mm-yy"
                      });
                        });
                </script>';
                
		echo '<table class="form-table">';

		foreach ($rider_meta_boxes['0'] as $field) {
			$meta = get_post_meta($post->ID, $field['id'], true);
			//$meta = !empty($meta) ? $meta : $field['std'];

			echo '<tr>';
			// call separated methods for displaying each type of field
			//call_user_func(array(&$this, 'show_field_' . $field['type']), $field, $meta);
                        echo '<th><label for="myplugin_new_field">';
                            _e( ''.$field['name'].'', 'myplugin_textdomain' );
                        echo '</label></th><td> ';
                        
                         if($field['type'] == 'selected'){
                            echo '<select id="'.$field['id'].'" name="'.$field['id'].'" style="width:95%">';
                            ?>
                            <option value="Male" <?php if($meta == 'Male' ){ echo 'selected=selected'; }?>>Male</option>
                            <option value="Female" <?php if($meta == 'Female' ){ echo 'selected=selected'; }?>>Female</option>
                            <?php
                            echo '</select>';
                        }elseif($field['type'] == 'natselected'){                            
                            
                           
                            echo '<select id="'.$field['id'].'" name="'.$field['id'].'" style="width:95%">';
                            foreach ($s_countries as $countid=>$contval){
                            ?>
                            <option value="<?php echo $contval; ?>" <?php if($meta == $contval ){ echo 'selected=selected'; }?>><?php echo __($countries[$contval]); ?></option>                           
                            <?php                            
                            }
                            echo '</select>';
                        }elseif($field['type'] == 'textarea'){
                            echo '<textarea class="wp-editor-area" style="width:95%" id="'.$field['id'].'" name="'.$field['id'].'">' . esc_attr( $meta ) . '</textarea>';
                        }else{
                        echo '<input type="'.$field['type'].'" id="'.$field['id'].'" name="'.$field['id'].'"';
                        echo ' value="' . esc_attr( $meta ) . '" style="width:95%" />';
                            echo '</td></tr>';    
                            wp_reset_postdata();
                        }
                        }
                        
                        
                        
                        
                     
		echo '</table>';  
                
                echo '<table class="form-table">';
                echo '<tr><th>Palmares</th><td><input type="text" name="add-more-pal" value="'.$pal_totals.'" style="width:95%" />'
                        . '<i>Enter Number of Palmers to Show and update</i></td></tr>'; 
                echo '<tr><td></td><td><p class="submit"><input type="submit" name="submit" id="submit" class="button button-primary" value="Submit"></p></td></tr>';
                echo '</table>';  

                echo '<table class="form-table">';
               
                echo '<tr>';
                        echo '<th style="text-align: left;padding-left: 11px;padding-bottom: 0px;"></th>';
                        echo '<th style="text-align: left;padding-left: 11px;padding-bottom: 0px;">Team</th>';
                       
                        echo '<th style="text-align: left;padding-left: 11px;padding-bottom: 0px;">Year</th>';
                        echo '<th style="text-align: left;padding-left: 11px;padding-bottom: 0px;">Details</th>';
                        
                        echo '</tr>';
                        for($pal_no = 1; $pal_no <= $pal_totals; $pal_no++){ 
                            $get_pal_meta_name = get_post_meta($post->ID, 'PalmersName'.$pal_no, true);
                            $get_pal_meta_victories = get_post_meta($post->ID, 'PalmersVictories'.$pal_no, true);
                            $get_pal_meta_year = get_post_meta($post->ID, 'PalmersYear'.$pal_no, true);
                            $get_pal_meta_details = get_post_meta($post->ID, 'PalmersDetails'.$pal_no, true);
                        echo '<th><label for="myplugin_new_field">';
                            _e( 'Palmers '.$pal_no, 'myplugin_textdomain' );
                        echo '</label><td> ';                        
                        echo '<input type="Text" id="PalmersName'.$pal_no.'" name="PalmersName'.$pal_no.'"';
                        echo ' value="'.$get_pal_meta_name.'" style="width:95%" />';                      
                        echo '</td>';
                        //echo '<td> ';                        
                        //echo '<input type="Text" id="PalmersVictories'.$pal_no.'" name="PalmersVictories'.$pal_no.'"';
                        //echo 'value="'.$get_pal_meta_victories.'" style="width:95%" />';                      
                        //echo '</td>';
                        echo '<td>';
                        echo '<input type="Text" id="PalmersYear'.$pal_no.'" name="PalmersYear'.$pal_no.'"';
                        echo 'value="'.$get_pal_meta_year.'" style="width:95%" />';                      
                        echo '</td><td>';
                        echo '<textarea id="PalmersDetails'.$pal_no.'" name="PalmersDetails'.$pal_no.'"';
                        echo ' style="width:95%" >'.$get_pal_meta_details.'</textarea>';                      
                        echo '</td></tr>';
                        }
                       
                echo '</table>';
	}          
        
        
        function wpa3396_page_template( $page_template )
        {
            if ( is_page( 'my-custom-page-slug' ) ) {
                $page_template = dirname( __FILE__ ) . '/includes/rider/class-rider-custom-template.php.php';
            }
            return $page_template;
        }
}

function theme_slug_widgets_init() {
    register_sidebar( array(
        'name' => __( 'Rider Sidebar', 'theme-slug213' ),
        'id' => 'sidebar-1',
        'description' => __( 'Widgets in this area will be shown on rider page.', 'theme-slug' ),        
	'before_widget' => '<div id="%1$s" class="widget %2$s">',
	'after_widget' => '</div>',
	'before_title' => '<div class="heading"><h3>',
	'after_title' => '</h3></div>',
    ) );
}
add_action( 'widgets_init', 'theme_slug_widgets_init' );


function rider_list_widgets_init() {
    register_sidebar( array(
        'name' => __( 'Rider List Sidebar', 'rider_list' ),
        'id' => 'rider-list',
        'description' => __( 'Widgets in this area will be shown on riders list.', 'rider_list' ),        
	'before_widget' => '<div id="%1$s" class="widget %2$s">',
	'after_widget' => '</div>',
	'before_title' => '<div class="heading"><h3>',
	'after_title' => '</h3></div>',
    ) );
}
add_action( 'widgets_init', 'rider_list_widgets_init' );







