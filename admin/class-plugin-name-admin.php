<?php

/**
 * The dashboard-specific functionality of the plugin.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Plugin_Name
 * @subpackage Plugin_Name/admin
 */

/**
 * The dashboard-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the dashboard-specific stylesheet and JavaScript.
 *
 * @package    Plugin_Name
 * @subpackage Plugin_Name/admin
 * @author     Your Name <email@example.com>
 */
class Plugin_Name_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @var      string    $plugin_name       The name of this plugin.
	 * @var      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;
		add_action('admin_menu', array(&$this, 'register_my_custom_menu_page'));
		
                //Create Custom Riders                
                add_action('init', array(&$this, 'custom_cwebco_riders'));                                                
               
                // Hook into the 'init' action Teams
                add_action( 'init', array(&$this,'custom_cwebco_teams') );
                
                // Hook into the 'init' action Staff
                add_action( 'init', array(&$this,'custom_cwebco_suddo_staff') );
                                
                //Hide default Riders Post Type Menu Item from Admin Menu                
                add_action( 'admin_menu', array(&$this, 'cwebc_remove_cycling_menu_items') );
                
                // Hook into the 'init' action Current Menu
                add_action( 'admin_head', array(&$this,'cwebc_active_current_menu') );
                
                // Add Extra Feilds in Events Type
                add_action( 'suddo_event_type_edit_form_fields', array(&$this,'pippin_taxonomy_edit_meta_field') );
                add_action( 'edited_suddo_event_type',  array(&$this,'save_taxonomy_custom_meta'));  
                add_action( 'create_suddo_event_type',  array(&$this,'save_taxonomy_custom_meta'));
                                
                //Show Custom Links on Team Type
                add_action( 'teams_edit_form_fields', array(&$this,'teams_extra_feilds') );
                
                add_action( 'init', array(&$this,'cwebc_custom_event') );
                 
                 
                // Hook into the 'init' action
                add_action( 'init', array(&$this,'suddo_event_type'));
                
                // Hook into the 'init' action
                add_action( 'init', array(&$this,'custom_results') );
                //add_action( 'init', array(&$this,'custom_results1') );
                                
                // Hook into the 'init' action
                add_action( 'init', array(&$this,'suddo_gallery') );
                
                
                // Hook into the 'init' action
                //add_action( 'init', array(&$this,'cwebc_palmares') );
               
	}

	/**
	 * Register the stylesheets for the Dashboard.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Plugin_Name_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Plugin_Name_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/plugin-name-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the dashboard.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Plugin_Name_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Plugin_Name_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/plugin-name-admin.js', array( 'jquery' ), $this->version, false );

	}


        function register_my_custom_menu_page() {
            global $submenu;
            add_menu_page('Cycling', 'Cycling', 'read', 'cycling', array(&$this, 'cycling_dashboard_page'), plugins_url('cycling/admin/img/icon.png'));
            add_submenu_page('cycling', 'Dashboard', 'Dashboard', 'read', 'cycling', array(&$this, 'cycling_dashboard_page'));
            add_submenu_page('cycling', 'Manage Teams', 'Manage Teams', 'read', 'teams', array(&$this, 'manage_teams'));
            add_submenu_page('cycling', 'Manage Riders', 'Manage Riders', 'read', 'riders', array(&$this, 'manage_riders'));
            //add_submenu_page('cycling', 'Manage Palmares', 'Manage Palmares', 'read', 'palmares', array(&$this, 'manage_palmares'));
            
            add_submenu_page('cycling', 'Manage Staff', 'Manage Staff', 'read', 'suddo_staff', array(&$this, 'manage_suddo_staff'));
            add_submenu_page('cycling', 'Event Type', 'Event Type', 'read', 'event_type', array(&$this, 'manage_events_type'));
            add_submenu_page('cycling', 'Manage Events', 'Manage Events', 'read', 'events', array(&$this, 'manage_events'));
            add_submenu_page('cycling', 'Manage Results', 'Manage Results', 'read', 'results', array(&$this, 'manage_results'));
            add_submenu_page('cycling', 'Manage Gallery', 'Manage Gallery', 'read', 'gallery', array(&$this, 'manage_gallery'));
            add_submenu_page('cycling', 'Settings', 'Settings', 'read', 'settings', array(&$this, 'manage_settings'));
            
            //$submenu['daily-diary'][0][0] = 'Daily Diary';
        }
        
        /*
         * Dashboard Fucntion
         */
        function cycling_dashboard_page() {
            if (!current_user_can('read')) {
                wp_die(__('You do not have sufficient permissions to access this page.'));
            } else {
                //include(plugins_url('cycling/admin/partials/plugin-name-admin-display.php'));
                //echo '<h1>Welcome to Cycling Dashboard</h1>';
                include(ADV_PLUGIN_FS_PATH1 . 'includes/dashboard/dashboard.php');
            }
        }
        
        /*
         * Teams Fucntion
         */
        function manage_teams() {
            if (!current_user_can('read')) {
                wp_die(__('You do not have sufficient permissions to access this page.'));
            } else {
                //include(plugins_url('cycling/admin/partials/plugin-name-admin-display.php'));
              //  echo wpflex_setup();
                echo '<div align="center"><div class="spinner-myimg"></div></div>';
                $url = admin_url().'edit-tags.php?taxonomy=teams&post_type=riders';
                ?>
                 <script>location.href='<?php echo $url;?>';</script>
                <?php
            }
            
        }
        
        /*
         * Riders Fucntion
         */
        function manage_riders() {
            if (!current_user_can('read')) {
                wp_die(__('You do not have sufficient permissions to access this page.'));
            } else {
                echo '<div align="center"><div class="spinner-myimg"></div></div>';
                $url = admin_url().'edit.php?post_type=riders';
                ?>
                 <script>location.href='<?php echo $url;?>';</script>
                <?php
            }
            
        }
        
        /*
         * Staff Fucntion
         */
        function manage_suddo_staff() {
            if (!current_user_can('read')) {
                wp_die(__('You do not have sufficient permissions to access this page.'));
            } else {
                echo '<div align="center"><div class="spinner-myimg"></div></div>';
                $url = admin_url().'edit.php?post_type=suddo_staff';
                ?>
                 <script>location.href='<?php echo $url;?>';</script>
                <?php
            }
            
        }
        
        /*
         * Events Fucntion Type
         */
        function manage_events_type() {
            if (!current_user_can('read')) {
                wp_die(__('You do not have sufficient permissions to access this page.'));
            } else {
                echo '<div align="center"><div class="spinner-myimg"></div></div>';
                $url = admin_url().'edit-tags.php?taxonomy=suddo_event_type&post_type=events';
                ?>
                 <script>location.href='<?php echo $url;?>';</script>
                <?php
            }
            
        }
        
        
        /*
         * Events Fucntion
         */
        function manage_events() {
            if (!current_user_can('read')) {
                wp_die(__('You do not have sufficient permissions to access this page.'));
            } else {                
                echo '<div align="center"><div class="spinner-myimg"></div></div>';
                $url = admin_url().'edit.php?post_type=events';
                ?>
                 <script type="text/javascript">location.href='<?php echo $url;?>';</script>
                <?php
            }
            
        }
        
        /*
         * Result Fucntion
         */
        function manage_results() {
            if (!current_user_can('read')) {
                wp_die(__('You do not have sufficient permissions to access this page.'));
            } else {
                echo '<div align="center"><div class="spinner-myimg"></div></div>';
                $url = admin_url().'edit.php?post_type=event_results';
                ?>
                 <script>location.href='<?php echo $url;?>';</script>
                <?php
            }
            
        }
        
        /*
         * Gallery Fucntion
         */
        function manage_gallery() {
            if (!current_user_can('read')) {
                wp_die(__('You do not have sufficient permissions to access this page.'));
            } else {
                echo '<div align="center"><div class="spinner-myimg"></div></div>';
                $url = admin_url().'edit.php?post_type=suddo_gallery';
                ?>
                 <script>location.href='<?php echo $url;?>';</script>
                <?php
            }
            
        }
        /*
         * Setting Fucntion
         */
        function manage_settings() {
            if (!current_user_can('read')) {
                wp_die(__('You do not have sufficient permissions to access this page.'));
            } else {
                include(ADV_PLUGIN_FS_PATH1 . 'includes/settings/class-settings.php');
            }
            
        }
        
        
        /*
         * Gallery Fucntion
         */
        function manage_palmares() {
            if (!current_user_can('read')) {
                wp_die(__('You do not have sufficient permissions to access this page.'));
            } else {
                $url = admin_url().'edit.php?post_type=cwebc_palmares';
                ?>
                 <script>location.href='<?php echo $url;?>';</script>
                <?php
            }
            
        }
        
        // Register Custom Riders
        function custom_cwebco_riders() {

                $labels = array(
                        'name'                => _x( 'Riders', 'Post Type General Name', 'riders' ),
                        'singular_name'       => _x( 'Riders', 'Post Type Singular Name', 'riders' ),
                        'menu_name'           => __( 'Rider Type', 'riders' ),
                        'parent_item_colon'   => __( 'Parent Rider:', 'riders' ),
                        'all_items'           => __( 'All Riders', 'riders' ),
                        'view_item'           => __( 'View Rider', 'riders' ),
                        'add_new_item'        => __( 'Add New Rider', 'riders' ),
                        'add_new'             => __( 'Add New Rider', 'riders' ),
                        'edit_item'           => __( 'Edit Rider', 'riders' ),
                        'update_item'         => __( 'Update Rider', 'riders' ),
                        'search_items'        => __( 'Search Rider', 'riders' ),
                        'not_found'           => __( 'Not found', 'riders' ),
                        'not_found_in_trash'  => __( 'Not found in Trash', 'riders' ),
                );
                $args = array(
                        'label'               => __( 'riders', 'riders' ),
                        'description'         => __( 'Rider Description', 'riders' ),
                        'labels'              => $labels,
                        'supports'            => array( 'title', 'thumbnail', 'editor'),
                        //'taxonomies'          => array( 'category', 'post_tag' ),
                        'hierarchical'        => false,
                        'public'              => true,
                        'show_ui'             => true,
                        'show_in_menu'        => true,
                        'show_in_nav_menus'   => true,
                        'show_in_admin_bar'   => true,
                        'menu_position'       => 5,
                        'can_export'          => true,
                        'has_archive'         => true,
                        'exclude_from_sgalleryearch' => false,
                        'publicly_queryable'  => true,
                        'capability_type'     => 'page',
                );
                register_post_type( 'riders', $args );

        }

          
          
        
        /**
        * Hide default Custom Post Type Menu Item from Admin Menu
        */
  
        function nerfherder_remove_cpt_menu_items() {
            remove_menu_page( 'edit.php?post_type=post_type' );
        }  
        
        
        // Register Custom Teams
        function custom_cwebco_teams() {

	$labels = array(
		'name'                       => _x( 'Teams', 'Taxonomy General Name', 'teams' ),
		'singular_name'              => _x( 'Teams', 'Taxonomy Singular Name', 'teams' ),
		'menu_name'                  => __( 'Teams', 'teams' ),
		'all_items'                  => __( 'All Teams', 'teams' ),
		'parent_item'                => __( 'Parent Team', 'teams' ),
		'parent_item_colon'          => __( 'Parent Team:', 'teams' ),
		'new_item_name'              => __( 'New Team Name', 'teams' ),
		'add_new_item'               => __( 'Add New Team', 'teams' ),
		'edit_item'                  => __( 'Edit Team', 'teams' ),
		'update_item'                => __( 'Update Team', 'teams' ),
		'separate_items_with_commas' => __( 'Separate Team with commas', 'teams' ),
		'search_items'               => __( 'Search Teams', 'teams' ),
		'add_or_remove_items'        => __( 'Add or remove Team', 'teams' ),
		'choose_from_most_used'      => __( 'Choose from the most used items', 'teams' ),
		'not_found'                  => __( 'Not Found', 'teams' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
	);
	register_taxonomy( 'teams', array( 'riders','suddo_staff' ), $args );

        }
    
        
        // Register Custom Post Type
        function custom_cwebco_suddo_staff() {
            $labels = array(
                    'name'                => _x( 'Staff', 'Post Type General Name', 'suddo_staff' ),
                    'singular_name'       => _x( 'Staff', 'Post Type Singular Name', 'suddo_staff' ),
                    'menu_name'           => __( 'Staff Type', 'suddo_staff' ),
                    'parent_item_colon'   => __( 'Parent Rider:', 'suddo_staff' ),
                    'all_items'           => __( 'All Staff', 'suddo_staff' ),
                    'view_item'           => __( 'View Staff', 'suddo_staff' ),
                    'add_new_item'        => __( 'Add New Staff', 'suddo_staff' ),
                    'add_new'             => __( 'Add New Staff', 'suddo_staff' ),
                    'edit_item'           => __( 'Edit Staff', 'suddo_staff' ),
                    'update_item'         => __( 'Update Staff', 'suddo_staff' ),
                    'search_items'        => __( 'Search Staff', 'suddo_staff' ),
                    'not_found'           => __( 'Not found', 'suddo_staff' ),
                    'not_found_in_trash'  => __( 'Not found in Trash', 'suddo_staff' ),
            );
            $args = array(
                    'label'               => __( 'suddo_staff', 'suddo_staff' ),
                    'description'         => __( 'Staff Description', 'suddo_staff' ),
                    'labels'              => $labels,
                    'supports'            => array( 'title', 'thumbnail', ),
                    //'taxonomies'          => array( 'category', 'post_tag' ),
                    'hierarchical'        => false,
                    'public'              => true,
                    'show_ui'             => true,
                    'show_in_menu'        => true,
                    'show_in_nav_menus'   => true,
                    'show_in_admin_bar'   => true,
                    'menu_position'       => 5,
                    'can_export'          => true,
                    'has_archive'         => true,
                    'exclude_from_search' => false,
                    'publicly_queryable'  => true,
                    'capability_type'     => 'page',
            );
            register_post_type( 'suddo_staff', $args );
        }
        
        /*
         * Custom Post for Events
         */
        
        function cwebc_custom_event() {

                $labels = array(
                        'name'                => _x( 'Events', 'Post Type General Name', 'events' ),
                        'singular_name'       => _x( 'Event', 'Post Type Singular Name', 'events' ),
                        'menu_name'           => __( 'Event Type', 'events' ),
                        'parent_item_colon'   => __( 'Parent Event:', 'events' ),
                        'all_items'           => __( 'All Events', 'events' ),
                        'view_item'           => __( 'View Event', 'events' ),
                        'add_new_item'        => __( 'Add New Event', 'events' ),
                        'add_new'             => __( 'Add New Event', 'events' ),
                        'edit_item'           => __( 'Edit Event', 'events' ),
                        'update_item'         => __( 'Update Event', 'events' ),
                        'search_items'        => __( 'Search Event', 'events' ),
                        'not_found'           => __( 'Not found', 'events' ),
                        'not_found_in_trash'  => __( 'Not found in Trash', 'events' ),
                );
                $args = array(
                        'label'               => __( 'events', 'events' ),
                        'description'         => __( 'Event Description', 'events' ),
                        'labels'              => $labels,
                        'supports'            => array( 'title', 'editor', 'thumbnail' ),
                        'taxonomies'          => array( 'post_tag' ),
                        'hierarchical'        => false,
                        'public'              => true,
                        'show_ui'             => true,
                        'show_in_menu'        => true,
                        'show_in_nav_menus'   => true,
                        'show_in_admin_bar'   => true,
                        'menu_position'       => 5,
                        'can_export'          => true,
                        'has_archive'         => true,
                        'exclude_from_search' => false,
                        'publicly_queryable'  => true,
                        'capability_type'     => 'page',
                );
                register_post_type( 'events', $args );

        }
        
        
        // Register Custom Taxonomy
function suddo_event_type() {

	$labels = array(
		'name'                       => _x( 'Events Type', 'Taxonomy General Name', 'suddo_event_type' ),
		'singular_name'              => _x( 'Event Type', 'Taxonomy Singular Name', 'suddo_event_type' ),
		'menu_name'                  => __( 'Event Type', 'suddo_event_type' ),
		'all_items'                  => __( 'All Event Type', 'suddo_event_type' ),
		'parent_item'                => __( 'Parent Event Type', 'suddo_event_type' ),
		'parent_item_colon'          => __( 'Parent Event Type:', 'suddo_event_type' ),
		'new_item_name'              => __( 'New Event Type', 'suddo_event_type' ),
		'add_new_item'               => __( 'Add New Event Type', 'suddo_event_type' ),
		'edit_item'                  => __( 'Edit Event Type', 'suddo_event_type' ),
		'update_item'                => __( 'Update Event Type', 'suddo_event_type' ),
		'separate_items_with_commas' => __( 'Separate Event Type with commas', 'suddo_event_type' ),
		'search_items'               => __( 'Search Event Type', 'suddo_event_type' ),
		'add_or_remove_items'        => __( 'Add or remove Event Type', 'suddo_event_type' ),
		'choose_from_most_used'      => __( 'Choose from the most used items', 'suddo_event_type' ),
		'not_found'                  => __( 'Not Found', 'suddo_event_type' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
                'show_in_menu'               => true,
	);
	register_taxonomy( 'suddo_event_type', array( 'events' ), $args );

}


// Register Custom Post Type
function suddo_gallery() {

	$labels = array(
		'name'                => _x( 'Gallery', 'Post Type General Name', 'suddo_gallery' ),
		'singular_name'       => _x( 'Gallery', 'Post Type Singular Name', 'suddo_gallery' ),
		'menu_name'           => __( 'Gallery', 'suddo_gallery' ),
		'parent_item_colon'   => __( 'Parent Gallery:', 'suddo_gallery' ),
		'all_items'           => __( 'All Gallery', 'suddo_gallery' ),
		'view_item'           => __( 'View Gallery', 'suddo_gallery' ),
		'add_new_item'        => __( 'Add New Gallery', 'suddo_gallery' ),
		'add_new'             => __( 'Add New Gallery', 'suddo_gallery' ),
		'edit_item'           => __( 'Edit Gallery', 'suddo_gallery' ),
		'update_item'         => __( 'Update Gallery', 'suddo_gallery' ),
		'search_items'        => __( 'Search Gallery', 'suddo_gallery' ),
		'not_found'           => __( 'Not found', 'suddo_gallery' ),
		'not_found_in_trash'  => __( 'Not found in Trash', 'suddo_gallery' ),
	);
	$args = array(
		'label'               => __( 'suddo_gallery', 'suddo_gallery' ),
		'description'         => __( 'Gallery Description', 'suddo_gallery' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'thumbnail', ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
	);
	register_post_type( 'suddo_gallery', $args );

}



// Register Custom Post Type
function custom_results() {

	$labels = array(
		'name'                => _x( 'Results', 'Post Type General Name', 'event_results' ),
		'singular_name'       => _x( 'Results', 'Post Type Singular Name', 'event_results' ),
		'menu_name'           => __( 'Results', 'event_results' ),
		'parent_item_colon'   => __( 'Parent Result:', 'event_results' ),
		'all_items'           => __( 'All Results', 'event_results' ),
		'view_item'           => __( 'View Result', 'event_results' ),
		'add_new_item'        => __( 'Add New Result', 'event_results' ),
		'add_new'             => __( 'Add New Result', 'event_results' ),
		'edit_item'           => __( 'Edit Result', 'event_results' ),
		'update_item'         => __( 'Update Result', 'event_results' ),
		'search_items'        => __( 'Search Result', 'event_results' ),
		'not_found'           => __( 'Not found', 'event_results' ),
		'not_found_in_trash'  => __( 'Not found in Trash', 'event_results' ),
	);
	$args = array(
		'label'               => __( 'event_results', 'event_results' ),
		'description'         => __( 'Results Description', 'event_results' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'thumbnail', ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
	);
	register_post_type( 'event_results', $args );

}






// Register Custom Post Type
/*
function cwebc_palmares() {

	$labels = array(
		'name'                => _x( 'Palmares', 'Post Type General Name', 'cwebc_palmares' ),
		'singular_name'       => _x( 'Palmares', 'Post Type Singular Name', 'cwebc_palmares' ),
		'menu_name'           => __( 'Palmares', 'cwebc_palmares' ),
		'parent_item_colon'   => __( 'Parent Palmares:', 'cwebc_palmares' ),
		'all_items'           => __( 'All ItePalmaress', 'cwebc_palmares' ),
		'view_item'           => __( 'View Palmares', 'cwebc_palmares' ),
		'add_new_item'        => __( 'Add New Palmares', 'cwebc_palmares' ),
		'add_new'             => __( 'Add New', 'cwebc_palmares' ),
		'edit_item'           => __( 'Edit Palmares', 'cwebc_palmares' ),
		'update_item'         => __( 'Update Palmares', 'cwebc_palmares' ),
		'search_items'        => __( 'Search Palmares', 'cwebc_palmares' ),
		'not_found'           => __( 'Not found', 'cwebc_palmares' ),
		'not_found_in_trash'  => __( 'Not found in Trash', 'cwebc_palmares' ),
	);
	$args = array(
		'label'               => __( 'cwebc_palmares', 'cwebc_palmares' ),
		'description'         => __( 'Palmares Description', 'cwebc_palmares' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'thumbnail', ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
	);
	register_post_type( 'cwebc_palmares', $args );

}

*/

        /*
         * Hide Menus
         */
        function cwebc_remove_cycling_menu_items(){
            remove_menu_page( 'edit.php?post_type=riders' );
            remove_menu_page( 'edit.php?post_type=suddo_staff' );
            remove_menu_page( 'edit.php?post_type=events' );
            remove_menu_page( 'edit.php?post_type=event_results' );
            remove_menu_page( 'edit.php?post_type=suddo_gallery' );
        }


        /*
        * Active Class to My Menu
        */
        function cwebc_active_current_menu(){
            
            //exit;
            $screen = get_current_screen();           
            if ( $screen->id == 'riders' || $screen->id == 'edit-riders' || $screen->id == 'suddo_staff' || $screen->id == 'edit-suddo_staff' || $screen->id == 'edit-teams' || $screen->id == 'edit-suddo_event_type' || $screen->id == 'events' || $screen->id == 'edit-events' || $screen->id == 'event_results' || $screen->id == 'edit-event_results' || $screen->id == 'suddo_gallery' || $screen->id == 'edit-suddo_gallery' || $screen->id == 'cwebc_palmares' || $screen->id == 'edit-cwebc_palmares' )  {
            ?>
            <script type="text/javascript">
            jQuery(document).ready(function($) {
                    $('#toplevel_page_cycling').addClass('wp-has-current-submenu wp-menu-open menu-top menu-top-first').removeClass('wp-not-current-submenu');
                    $('#toplevel_page_cycling > a').addClass('wp-has-current-submenu').removeClass('wp-not-current-submenu');
            });
            </script>
            <?php }
            
            if ( $screen->id == 'suddo_staff' ) {
            ?>
            <script type="text/javascript">
            jQuery(document).ready(function($) {
                    jQuery('a[href$="suddo_staff"]').parent().addClass('current');
                    jQuery('a[href$="suddo_staff"]').addClass('current');
            });
            </script>
            <?php
            }
            
            if ( $screen->id == 'riders' ) {
            ?>
            <script type="text/javascript">
            jQuery(document).ready(function($) {
                    $('a[href$="riders"]').parent().addClass('current');
                    $('a[href$="riders"]').addClass('current');
            });
            </script>
            <?php
            }
            
            
            
            if ( $screen->id == 'events' ) {
            ?>
            <script type="text/javascript">
            jQuery(document).ready(function($) {
                    $('a[href$="events"]').parent().addClass('current');
                    $('a[href$="events"]').addClass('current');
                    $('a[href$="events_add_post"]').parent().addClass('current');
                    $('a[href$="events_add_post"]').addClass('current');
            });
            </script>
            <?php
            }
            
            if ( $screen->id == 'edit-events' ) {
	?>
	<script type="text/javascript">
	jQuery(document).ready(function($) {
                $('a[href$="events"]').parent().addClass('current');
                $('a[href$="events"]').addClass('current');		
	});
	</script>
	<?php
	}
            
            
            
            if ( $screen->id == 'event_results' ) {
            ?>
            <script type="text/javascript">
            jQuery(document).ready(function($) {
                    $('a[href$="results"]').parent().addClass('current');
                    $('a[href$="results"]').addClass('current');
            });
            </script>
            <?php
            }
            
             if ( $screen->id == 'suddo_gallery' ) {
            ?>
            <script type="text/javascript">
            jQuery(document).ready(function($) {
                    $('a[href$="gallery"]').parent().addClass('current');
                    $('a[href$="gallery"]').addClass('current');
            });
            </script>
            <?php
            }
            
            if ( $screen->id == 'cwebc_palmares' ) {
            ?>
            <script type="text/javascript">
            jQuery(document).ready(function($) {
                    $('a[href$="palmares"]').parent().addClass('current');
                    $('a[href$="palmares"]').addClass('current');
            });
            </script>
            <?php
            }
            
        }
        
        
        
        

        
        
        // Edit term page
function pippin_taxonomy_edit_meta_field($term) {
 
	// put the term ID into a variable
	$t_id = $term->term_id;
 
	// retrieve the existing value(s) for this meta field. This returns an array
	$term_meta = get_option( "taxonomy_$t_id" ); ?>
	<tr class="form-field">
	<th scope="row" valign="top"><label for="term_meta[custom_term_meta]"><?php _e( 'Color', 'pippin' ); ?></label></th>
		<td>
			<input type="text" name="term_meta[custom_term_meta]" id="term_meta[custom_term_meta]" value="<?php echo esc_attr( $term_meta['custom_term_meta'] ) ? esc_attr( $term_meta['custom_term_meta'] ) : ''; ?>">
			<p class="description"><?php _e( 'Enter a value for this field','pippin' ); ?></p>
		</td>
	</tr>
        
        <tr class="form-field">
	<th scope="row" valign="top"><label for="term_meta[assign_result]"><?php _e( 'Assign Result', 'pippin' ); ?></label></th>
		<td>
                    <input type="checkbox" name="term_meta[assign_result]" id="term_meta[assign_result]" value="result_enabl"  <?php if($term_meta['assign_result'] == 'result_enabl'){echo 'checked=checked';} ?> >
                    
			<p class="description"><?php _e( 'Enable For Race / Recon','pippin' ); ?></p>
		</td>
	</tr>
<?php
}

// Save extra taxonomy fields callback function.
function save_taxonomy_custom_meta( $term_id ) {
	if ( isset( $_POST['term_meta'] ) ) {
		$t_id = $term_id;
		$term_meta = get_option( "taxonomy_$t_id" );
		$cat_keys = array_keys( $_POST['term_meta'] );
		foreach ( $cat_keys as $key ) {
			if ( isset ( $_POST['term_meta'][$key] ) ) {
				$term_meta[$key] = $_POST['term_meta'][$key];
			}
		}
		// Save the option array.
		update_option( "taxonomy_$t_id", $term_meta );
	}
}  




//SHow Extra Feilds on Events Type
function teams_extra_feilds($term){
    $gettermlink = get_term_link($term);
    if (strpos($gettermlink, '?') !== false) {
        $andq = '&';                                
    }else{
        $andq = '?';
    } ?>
           
        <a href="<?php echo get_term_link($term); ?>" target="_blank">View Rider</a>        
        | <a href="<?php echo get_term_link($term); ?><?php if (strpos(get_term_link($term), '?') !== false) { echo '&'; }else{echo '?';} ?>getstaff=1" target="_blank">View Staff</a>
    <?php    
    $cal_pages = get_pages(array(
        'meta_key' => '_wp_page_template',
        'meta_value' => 'events-template.php'
    ));
    foreach($cal_pages as $cal_page){
        echo ' | <a href="'.get_permalink($get_cal_page = $cal_page->ID); if (strpos(get_permalink($get_cal_page = $cal_page->ID), '?') !== false) { echo '&'; }else{echo '?';} echo 'team='.$term->slug.'" target="_blank" >Calender</a>';
    }
    
    $cal_pages1 = get_pages(array(
        'meta_key' => '_wp_page_template',
        'meta_value' => 'calender-events-template.php'
    ));
    foreach($cal_pages1 as $cal_page1){
        echo ' | <a href="'.get_permalink($cal_page1->ID);if (strpos(get_permalink($cal_page1->ID), '?') !== false) { echo '&'; }else{echo '?';} echo 'team='.$term->slug.'" target="_blank" >Calender View</a>';
    }
    $cal_pages2 = get_pages(array(
        'meta_key' => '_wp_page_template',
        'meta_value' => 'team-gallery-template.php'
    ));
    foreach($cal_pages2 as $cal_page2){
        echo ' | <a href="'.get_permalink($cal_page2->ID);if (strpos(get_permalink($cal_page2->ID), '?') !== false) { echo '&'; }else{echo '?';} echo 'team='.$term->slug.'" target="_blank" >View Gallery</a>';
    }
        
}


}



