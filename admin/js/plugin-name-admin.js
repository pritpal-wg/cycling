(function ($) {
    'use strict';

    /**
     * All of the code for your Dashboard-specific JavaScript source
     * should reside in this file.
     *
     * Note that this assume you're going to use jQuery, so it prepares
     * the $ function reference to be used within the scope of this
     * function.
     *
     * From here, you're able to define handlers for when the DOM is
     * ready:
     *
     * $(function() {
     *
     * });
     *
     * Or when the window is loaded:
     *
     * $( window ).load(function() {
     *
     * });
     *
     * ...and so on.
     *
     * Remember that ideally, we should not attach any more than a single DOM-ready or window-load handler
     * for any particular page. Though other scripts in WordPress core, other plugins, and other themes may
     * be doing this, we should try to minimize doing that in our own work.
     */

// Selec All Checkbox Jquery
    jQuery(document).ready(function () {
        jQuery('#selecctall').click(function (event) {  //on click 
            if (this.checked) { // check select status
                //alert(jQuery('#unselecctall').checked);
                jQuery('.checkbox1').each(function () { //loop through each checkbox
                    this.checked = true;  //select all checkboxes with class "checkbox1"                   
                });
            } else {
                jQuery('.checkbox1').each(function () { //loop through each checkbox
                    this.checked = false; //deselect all checkboxes with class "checkbox1"                       
                });
            }
        });

        jQuery('#unselecctall').click(function (event) {  //on click 
            if (this.checked) { // check select status
                jQuery('.checkbox1').each(function () { //loop through each checkbox
                    this.checked = false;  //select all checkboxes with class "checkbox1"               
                });
            }
        });





    });



})(jQuery);


