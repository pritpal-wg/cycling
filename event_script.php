<?php
require_once( dirname(dirname(__FILE__)) . '/../../wp-load.php' );
require_once( dirname(dirname(__FILE__)) . '/../../wp-includes/wp-db.php' );
require_once( dirname(dirname(__FILE__)) . '/../../wp-includes/pluggable.php' );
require_once( dirname(dirname(__FILE__)) . '/../../wp-includes/general-template.php' );
require_once( dirname(dirname(__FILE__)) . '/../../wp-admin/includes/template.php' );
global $wpdb;



$cal_pages_men = get_pages(array(
    'meta_key' => '_wp_page_template',
    'meta_value' => 'events-template-mens.php'
));

$cal_pages_women = get_pages(array(
    'meta_key' => '_wp_page_template',
    'meta_value' => 'events-template-ladies.php'
));

$cal_pages_u23 = get_pages(array(
    'meta_key' => '_wp_page_template',
    'meta_value' => 'events-template-u23.php'
));

print_r($cal_pages_men);exit;


$current_date = date("m/d/Y");

// Add 15 Days to current date
$i = 1;
$date = date("m/d/Y", strtotime(date('m/d/Y') . " +" . $i . " years"));
$update = strtotime($date);

$currentdate = strtotime($current_date);


// Get All Riders Posts
$args = array(
    'posts_per_page' => 200,
    'offset' => 0,
    'category' => '',
    'category_name' => '',
    'orderby' => 'post_date',
    'order' => 'DESC',
    'include' => '',
    'exclude' => '',
    'meta_key' => '',
    'meta_value' => '',
    'post_type' => array( 'riders', 'suddo_staff' ),
    'post_mime_type' => '',
    'post_parent' => '',
    'post_status' => 'publish',
    'suppress_filters' => true
);
$get_all_riders_left_posts = get_posts($args);




foreach ($get_all_riders_left_posts as $post) {

    /* do not know */
    $post_categories = get_the_terms($post->ID, 'teams');
    foreach ($post_categories as $term) {
        $team_link = $term->name;
        $team_slug_nm = $term->slug;
        $team_id = $term->term_id;
    }

   $team_id_fr = icl_object_id($team_id, 'teams', true, 'fr' );
   $team_id_nl = icl_object_id($team_id, 'teams', true, 'nl' );
   $team_id_en = icl_object_id($team_id, 'teams', true, 'en' );
   $get_post_type = get_post_type($post->ID);

    // Get Rider Date And Month
    $rider_brth = get_post_meta($post->ID, 'rddatepicker', true);
    
    /* convert birth day to current year  */
    $time=strtotime($rider_brth);
    $riderdate = date("m/d",$time);
    $finalriderdate = $riderdate.'/'.date('Y');   // current year processing
    $serriderdate = strtotime($finalriderdate);
    
    /* get next year & current year */
    $nextyear1 = date("Y", strtotime(date('Y') . " +1 years"));
    $finaleventdatess = $riderdate.'/'.date("Y");
    $finaleventdate_next = $riderdate.'/'.$nextyear1;

    
    // Check if rider birthday matched with after 365 Days
    if (($serriderdate >= $currentdate) && ($serriderdate <= $update)) {    
                
        
        $checkquery = "SELECT count(post_title) FROM `".$wpdb->prefix."posts` as ps INNER JOIN `".$wpdb->prefix."postmeta` as pm ON pm.post_id = ps.ID WHERE post_title like '".$post->post_title."' AND `post_status` = 'publish' AND `post_type` = 'events' AND `meta_value` = '".$finaleventdatess."'";
        $post_if = $wpdb->get_var($checkquery);
        
        
        if($post_if < 1){                            
        // Get Birthday Category ID by slug 'birthday'
        $category = get_term_by('slug', 'birthday-nl', 'suddo_event_type');
        $term_id = $category->term_id;    
                    
        $nextyear = date("Y", strtotime(date('Y') . " +1 years"));
    
        //Generate Event Date
        $finaleventdate = $riderdate.'/'.date("Y");
        
        // Array for wp_insert_post
        $my_posts = array(
            'post_title' => $post->post_title,
            'post_content' => 'Birthday Event ' . $post->post_title,
            'post_type' => 'events',
            'tax_input' => array(
                                'suddo_event_type' => array($term_id)
                            ),
            'post_status' => 'publish',            
        );

        
		
		$postid_nl = icl_object_id($post->ID, $get_post_type , true, 'nl' );
		$riderpostid = array($postid_nl);
        $serriderpostid_nl = serialize($riderpostid);
        echo $serriderpostid_nl;
        
        $post_id = wp_insert_post($my_posts);    
        //Custom Feilds of created posts
        update_post_meta($post_id, 'eventDateofEvent', $finaleventdate);
        update_post_meta($post_id, 'key_event_team_name', $team_id_nl);
        update_post_meta($post_id, 'riderskey', $serriderpostid_nl);
        
        
        $myrows = $wpdb->get_results( "SELECT * FROM ".$wpdb->prefix."icl_translations WHERE element_type = 'post_events' AND element_id = '$post_id'" );
    
        $trid = $myrows[0]->trid;
        
        
    
        // Array for wp_insert_post for nl
        // Get Birthday Category ID by slug 'birthday'
        $category_nl = get_term_by('slug', 'birthday-en', 'suddo_event_type');
        $term_id_nl = $category_nl->term_id;  
        
        $my_posts_nl = array(
            'post_title' => $post->post_title,
            'post_content' => 'Birthday Event ' . $post->post_title,
            'post_type' => 'events',
            'tax_input' => array(
                                'suddo_event_type' => array($term_id_nl)
                            ),
            'post_status' => 'publish',            
        );
        $post_id_nl = wp_insert_post($my_posts_nl);
		
		$postid_en = icl_object_id($post->ID, $get_post_type, true, 'en' );
		$riderpostid_en = array($postid_en);
        $serriderpostid_en = serialize($riderpostid_en);
		
        //Custom Feilds of created posts
        update_post_meta($post_id_nl, 'eventDateofEvent', $finaleventdate);
        update_post_meta($post_id_nl, 'key_event_team_name', $team_id_en);
        update_post_meta($post_id_nl, 'riderskey', $serriderpostid_en);
        $wpdb->query(
	"
            UPDATE " .$wpdb->prefix."icl_translations
            SET trid = $trid,
            language_code = 'en',    
            source_language_code = 'nl'
            WHERE element_id = $post_id_nl 
	"
        );
        
        // Array for wp_insert_post for fr
        $category_fr = get_term_by('slug', 'birthday-fr', 'suddo_event_type');
        $term_id_fr = $category_fr->term_id;  
        
        $my_posts_fr = array(
            'post_title' => $post->post_title,
            'post_content' => 'Birthday Event ' . $post->post_title,
            'post_type' => 'events',
            'tax_input' => array(
                                'suddo_event_type' => array($term_id_fr)
                            ),
            'post_status' => 'publish',            
        );
        $post_id_fr = wp_insert_post($my_posts_fr);
		
		$postid_fr = icl_object_id($post->ID, $get_post_type, true, 'fr' );
		$riderpostid_fr = array($postid_fr);
        $serriderpostid_fr = serialize($riderpostid_fr);
		
        update_post_meta($post_id_fr, 'eventDateofEvent', $finaleventdate);
        update_post_meta($post_id_fr, 'key_event_team_name', $team_id_fr);
        update_post_meta($post_id_fr, 'riderskey', $serriderpostid_fr);
        $wpdb->query(
	"
            UPDATE " .$wpdb->prefix."icl_translations
            SET trid = $trid,
            language_code = 'fr',    
            source_language_code = 'nl'
            WHERE element_id = $post_id_fr 
	"
        );
        
      
        
        // Error Handing
        if (is_wp_error($post_id)) {
            $errors = $post_id->get_error_messages();
            foreach ($errors as $error) {
                echo $error; 
                exit;
            }
        }
        }
        
        
       
        
        $checkquerynext = "SELECT count(post_title) FROM `".$wpdb->prefix."posts` as ps INNER JOIN `".$wpdb->prefix."postmeta` as pm ON pm.post_id = ps.ID WHERE post_title like '".$post->post_title."' AND `post_status` = 'publish' AND `post_type` = 'events' AND `meta_value` = '".$finaleventdate_next."'";
        $post_if_next = $wpdb->get_var($checkquerynext);
        
        if($post_if_next < 1){
                        // Get Birthday Category ID by slug 'birthday'
                        $category1 = get_term_by('slug', 'birthday-nl', 'suddo_event_type');
                        $term_id1 = $category1->term_id;    

                        $nextyear_n = date("Y", strtotime(date('Y') . " +1 years"));

                        //Generate Event Date
                        $finaleventdate1 = $riderdate.'/'.$nextyear_n;

                        // Array for wp_insert_post
                        $my_posts1 = array(
                            'post_title' => $post->post_title,
                            'post_content' => 'Birthday Event ' . $post->post_title,
                            'post_type' => 'events',
                            'tax_input' => array(
                                                'suddo_event_type' => array($term_id1)
                                            ),
                            'post_status' => 'publish',            
                        );

                        $riderpostid1 = array($post->ID);
                        $serriderpostid1 = serialize($riderpostid1);

                        $post_id1 = wp_insert_post($my_posts1); 
						
						$postid_nl1 = icl_object_id($post->ID, $get_post_type, true, 'nl' );
						$riderpostid1 = array($postid_nl1);
						$serriderpostid_nl1 = serialize($riderpostid1);
						   
                        //Custom Feilds of created posts
                        update_post_meta($post_id1, 'eventDateofEvent', $finaleventdate1);
                        update_post_meta($post_id1, 'key_event_team_name', $team_id_nl);
                        update_post_meta($post_id1, 'riderskey', $serriderpostid_nl1);


                        $myrows1 = $wpdb->get_results( "SELECT * FROM ".$wpdb->prefix."icl_translations WHERE element_type = 'post_events' AND element_id = '$post_id1'" );

                        $trid1 = $myrows1[0]->trid;

                        // Array for wp_insert_post for nl
                        // Get Birthday Category ID by slug 'birthday'
                        $category_nl1 = get_term_by('slug', 'birthday-en', 'suddo_event_type');
                        $term_id_nl1 = $category_nl1->term_id;  

                        $my_posts_nl1 = array(
                            'post_title' => $post->post_title,
                            'post_content' => 'Birthday Event ' . $post->post_title,
                            'post_type' => 'events',
                            'tax_input' => array(
                                                'suddo_event_type' => array($term_id_nl1)
                                            ),
                            'post_status' => 'publish',            
                        );
                        $post_id_nl1 = wp_insert_post($my_posts_nl1);
						
						$postid_en1 = icl_object_id($post->ID, $get_post_type, true, 'en' );
						$riderpostid_en1 = array($postid_en1);
						$serriderpostid_en1 = serialize($riderpostid_en1);
						
                        //Custom Feilds of created posts
                        update_post_meta($post_id_nl1, 'eventDateofEvent', $finaleventdate1);
                        update_post_meta($post_id_nl1, 'key_event_team_name', $team_id_en);
                        update_post_meta($post_id_nl1, 'riderskey', $serriderpostid_en1);
                        $wpdb->query(
                        "
                            UPDATE " .$wpdb->prefix."icl_translations
                            SET trid = $trid1,
                            language_code = 'en',    
                            source_language_code = 'nl'
                            WHERE element_id = $post_id_nl1 
                        "
                        );

                        // Array for wp_insert_post for fr
                        $category_fr1 = get_term_by('slug', 'birthday-fr', 'suddo_event_type');
                        $term_id_fr1 = $category_fr1->term_id;  

                        $my_posts_fr1 = array(
                            'post_title' => $post->post_title,
                            'post_content' => 'Birthday Event ' . $post->post_title,
                            'post_type' => 'events',
                            'tax_input' => array(
                                                'suddo_event_type' => array($term_id_fr1)
                                            ),
                            'post_status' => 'publish',            
                        );
                        $post_id_fr1 = wp_insert_post($my_posts_fr1);
						
						$postid_fr1 = icl_object_id($post->ID, $get_post_type, true, 'fr' );
						$riderpostid_fr1 = array($postid_fr1);
						$serriderpostid_fr1 = serialize($riderpostid_fr1);
						
                        update_post_meta($post_id_fr1, 'eventDateofEvent', $finaleventdate1);
                        update_post_meta($post_id_fr1, 'key_event_team_name', $team_id_fr);
                        update_post_meta($post_id_fr1, 'riderskey', $serriderpostid_fr1);
                        $wpdb->query(
                        "
                            UPDATE " .$wpdb->prefix."icl_translations
                            SET trid = $trid1,
                            language_code = 'fr',    
                            source_language_code = 'nl'
                            WHERE element_id = $post_id_fr1 
                        "
                        );

                        //Custom Feilds of created posts
                        /*update_post_meta($post_id, 'eventDateofEvent', $finaleventdate);
                        update_post_meta($post_id, 'key_event_team_name', $team_id);
                        update_post_meta($post_id, 'riderskey', $serriderpostid1);
                        */
                        // Error Handing
                        if (is_wp_error($post_id)) {
                            $errors = $post_id->get_error_messages();
                            foreach ($errors as $error) {
                                echo $error; 
                                exit;
                            }
                        }
        }

       
        
    }
}

echo 'Events updated';        
?>